package id.padangsambian.retribusiapppetugas.view.company

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.shared.model.Company
import id.padangsambian.shared.setValue
import id.padangsambian.shared.toCurrency
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.onTextChanged
import id.widianapw.android_utils.extensions.validate
import id.widianapw.android_utils.extensions.value
import id.widianapw.android_utils.validator.ValidatorType
import kotlinx.android.synthetic.main.fragment_transaction_company_b_s.*

data class TransactionCompanyRequest(
    var company_id: Int? = null,
    var money_pay: Int? = null,
    var money_changes: Int? = null
);

class TransactionCompanyBSFragment(
    val company: Company?,
    val onSave: (TransactionCompanyRequest) -> Unit
) : BottomSheetDialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_BottomSheetDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaction_company_b_s, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val value = company?.unpaid_transaction?.sumOf { it.amount ?: 0 }
        til_store_amount?.setValue(value.toString())
        tv_total_payment?.text = "Total tagihan periode ini ${value?.toCurrency()}"

        til_money_pay?.editText?.onTextChanged {
            val changes = (it.toIntOrNull() ?: 0) - (value ?: 0)
            tv_total_money_changes?.text = "Total kembalian ${changes.toCurrency()}"
        }

        btn_payment_detail?.onClick {
            val dialog = TransactionDetailDialogFragment(company)
            dialog.show(childFragmentManager, "")
        }

        btn_save?.onClick {
            company?.let { com ->
                if (til_money_pay?.validate(
                        ValidatorType.MinimumValue(value ?: 0)
                    ) == true
                ) {
                    val changes = (til_money_pay?.value()?.toIntOrNull() ?: 0) - (value ?: 0)
                    val dataSend = TransactionCompanyRequest().apply {
                        company_id = com.id
                        money_pay = til_money_pay?.value()?.toIntOrNull()
                        money_changes = changes
                    }
                    val dialog = TransactionConfirmationDialogFragment(
                        company.unpaid_transaction,
                        dataSend.money_pay,
                        dataSend.money_changes
                    ) {
                        it?.let { it2 ->
                            this.dismiss()
                            this.onSave(dataSend)
                        }
                    }
                    dialog.show(childFragmentManager, "")
                }
            }
        }
    }


}