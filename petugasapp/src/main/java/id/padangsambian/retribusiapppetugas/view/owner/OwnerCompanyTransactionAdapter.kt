package id.padangsambian.retribusiapppetugas.view.owner

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.shared.model.Company
import id.padangsambian.shared.toCurrency
import id.widianapw.android_utils.extensions.onClick
import kotlinx.android.synthetic.main.layout_rv_owner_company_payment.view.*

class OwnerCompanyTransactionAdapter(val onClickDetail: (Company) -> Unit) :
    RecyclerView.Adapter<OwnerCompanyTransactionAdapter.ViewHolder>() {
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    val list = mutableListOf<Company>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_rv_owner_company_payment, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.itemView.apply {
            tv_rv_owner_company_name?.text = item.name
            tv_rv_owner_company_amount?.text =
                item.unpaid_transaction?.sumOf { it.amount ?: 0 }?.toCurrency()
            btn_rv_see_detail_company_owner?.onClick {
                onClickDetail(item)
            }
        }
    }

    override fun getItemCount(): Int = list.count()

    fun setData(data: List<Company>?) {
        list.apply {
            clear()
            data?.let { addAll(it) }
        }
        notifyDataSetChanged()
    }
}