package id.padangsambian.retribusiapppetugas.view.company

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.model.CompanyResponse
import id.padangsambian.retribusiapppetugas.presenter.company.CompanyPresenter
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.padangsambian.retribusiapppetugas.view.map.MapsActivity
import id.padangsambian.shared.Utils
import id.padangsambian.shared.getColor
import id.padangsambian.shared.input.BaseDialogFragment
import id.padangsambian.shared.model.Company
import id.padangsambian.shared.model.SuccessResponse
import id.padangsambian.shared.toCurrency
import id.widianapw.android_utils.common.Const
import id.widianapw.android_utils.extensions.*
import kotlinx.android.synthetic.main.activity_company_detail.*

class CompanyDetailActivity : BaseActivity(), CompanyPresenter.Delegate {
    companion object {
        const val ID = "id"
    }

    private val companyOperationalAdapter = CompanyOperationalAdapter()
    private val presenter = CompanyPresenter(this)
    private var id: Int? = null
    private var companyData: Company? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company_detail)
        setupToolbar("Usaha Detail") {
            onBackPressed()
        }

        id = intent.getIntExtra(ID, 0)

        initView()
        loadData(true)

    }

    private fun initView() {
        rv_company_operational_read?.adapter = companyOperationalAdapter
        btn_company_store_transaction?.onClick {
            companyData?.let { it1 ->
                doRetributionPayment(it1, {
                    isLoading(true)
                    doRequest {
                        presenter.postTransaction(it)
                    }
                }, {
                    isLoading(true)
                    doRequest {
                        presenter.postInstalmentTransaction(it)
                    }
                })
            }
        }

        btn_company_transaction_history?.onClick {
            with(Intent(this, TransactionActivity::class.java)) {
                putExtra(TransactionActivity.ID, companyData?.id)
                putExtra(
                    TransactionActivity.SUBSCRIPTION_TYPE,
                    companyData?.subscription?.subscription_type?.category?.lowercase()
                )
                startActivity(this)
            }
        }

        btn_company_document_read?.onClick {
            companyData?.documents?.let {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(it))
                startActivity(browserIntent)
            }
        }

        btn_company_pin_read?.onClick {
            with(Intent(this, MapsActivity::class.java)) {
                putExtra(MapsActivity.STATE, MapsActivity.State.DETAIL)
                putExtra(MapsActivity.ID, companyData?.id)
                startActivity(this)
            }
        }
    }

    private fun loadData(isFirst: Boolean = false) {
        isLoading(true, isFirst)
        doRequest {
            id?.let { presenter.getCompanyDetail(it) }
        }
    }

    private fun isLoading(izLoading: Boolean, isFirst: Boolean = false) {
        if (izLoading) {
            if (isFirst) loader_company?.visible()
            else Utils.showProgress(this)
        } else {
            loader_company?.animateGone()
            Utils.hideProgress()
        }
    }

    override fun getCompanyDetailResponse(res: CompanyResponse) {
        super.getCompanyDetailResponse(res)
        isLoading(false)
        updateViewFromResponse(res)
    }

    private fun updateViewFromResponse(res: CompanyResponse) {
        companyData = res.data
        companyData?.run {
            chip_open_status_company?.text =
                if (company_close?.is_close == true) "Tutup" else "Buka"

//            Read
            val backgroundColor = when (getPaymentStatus()) {
                is Company.PaymentStatus.Paid -> R.color.color_lightest_primary
                is Company.PaymentStatus.Unpaid -> R.color.color_light_error

            }

            val textColor = when (getPaymentStatus()) {
                is Company.PaymentStatus.Paid -> R.color.colorPrimary
                is Company.PaymentStatus.Unpaid -> R.color.error
            }

            chip_company_transaction_status_read?.apply {
                setChipBackgroundColorResource(backgroundColor)
                setTextColor(textColor.getColor(this@CompanyDetailActivity))
                text = getPaymentStatus().status
            }


            tv_company_name_read?.text = name ?: "-"
            tv_company_address_read?.text = address ?: "-"
            tv_company_tempekan_read?.text = banjar?.name ?: "-"
            tv_company_type_read?.text = company_type?.type ?: "-"

            tv_company_payment_type_read?.text = subscription?.subscription_type?.category ?: "-"
            tv_company_payment_amount_read?.text = subscription_amount?.toCurrency()


            tv_company_next_payment_read?.text =
                "Tanggal pembayaran selanjutnya adalah ${
                    next_transaction?.date?.toDateString(
                        Const.READ_DATE_FORMAT,
                        isConvertToLocal = false
                    )
                }"

            layout_cuti_company_detail?.setVisibility(company_close?.is_cuti == true)
            company_close?.schedule?.let {
                val READ_DATE = "dd MMMM yyyy"
                val dateStart = it.date_start?.toDateString(
                    fromFormat = "yyyy-MM-dd",
                    toFormat = READ_DATE
                )
                val dateEnd = it.date_end?.toDateString(
                    fromFormat = "yyyy-MM-dd",
                    toFormat = READ_DATE
                )
                if (dateStart == dateEnd) {
                    tv_company_close_detail?.text = dateStart
                } else {
                    tv_company_close_detail?.text = "$dateStart - $dateEnd"
                }

            }
            company_operational_schedule?.let { companyOperationalAdapter.setData(it) }


            iv_company_read?.loadImage(photos)

            if (documents == null) {
                btn_company_document_read?.text = "-"
            }

            btn_company_store_transaction?.setVisibility((getPaymentStatus() is Company.PaymentStatus.Unpaid) && (company_close?.is_close != true))

        }
    }

    override fun showError(title: String?, message: String?) {
        isLoading(false)
        super<BaseActivity>.showError(title, message)
    }

    override fun postTransactionResponse(res: SuccessResponse) {
        super.postTransactionResponse(res)
        isLoading(false)
        showSuccessDialog(
            "Transaksi berhasil dilakukan",
            listener = object : BaseDialogFragment.Listener {})
        loadData()
    }
}