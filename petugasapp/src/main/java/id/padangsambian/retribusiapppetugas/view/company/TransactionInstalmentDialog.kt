package id.padangsambian.retribusiapppetugas.view.company

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputLayout
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.shared.model.Company
import id.padangsambian.shared.toCurrency
import id.widianapw.android_utils.extensions.onClick

class TransactionInstalmentDialog(val company: Company?) : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaction_instalment_dialog, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = layoutInflater.inflate(R.layout.fragment_transaction_instalment_dialog, null)
        val rvInstalment = view.findViewById<RecyclerView>(R.id.rv_instalment_transaction)
        val tilMoneyPay = view.findViewById<TextInputLayout>(R.id.til_money_pay)

        val tvPaymentTotal = view.findViewById<TextView>(R.id.tv_total_instalment)
        val tvMoneyChanges = view.findViewById<TextView>(R.id.tv_total_money_changes)
        val btnPay = view.findViewById<MaterialButton>(R.id.btn_pay_instalment)
        val btnBack = view.findViewById<MaterialButton>(R.id.btn_cancel_instalment)

        val builder = context?.let {
            MaterialAlertDialogBuilder(it).run {
                setView(view)
                create()

            }
        }

        builder?.let {
//            tvNominalLabel?.text =
//                "Nominal Bayar (${company?.subscription?.subscription_type?.category})"
//
//            val payText =
//                "${company?.next_transaction?.subscription_qty} x ${company?.next_transaction?.subscription_amount}"
//            tvPayment?.text = payText

        }
        return builder!!
    }

}