package id.padangsambian.retribusiapppetugas.view.profile

import android.os.Bundle
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.model.profile.ProfileRequest
import id.padangsambian.retribusiapppetugas.model.profile.ProfileResponse
import id.padangsambian.retribusiapppetugas.presenter.profile.ProfilePresenter
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.padangsambian.shared.Utils
import id.padangsambian.shared.input.BaseDialogFragment
import id.padangsambian.shared.setValue
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.validate
import id.widianapw.android_utils.extensions.value
import id.widianapw.android_utils.validator.ValidatorType
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : BaseActivity(), ProfilePresenter.Delegate {
    private val presenter = ProfilePresenter(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        setupToolbar("Profil") {
            onBackPressed()
        }
        loadData()
        initView()
    }

    private fun initView() {
        switch_change_password?.setOnCheckedChangeListener { buttonView, isChecked ->
            expanded_password_profile?.isExpanded = isChecked
        }

        btn_update_profile?.onClick {
            if (isAllValid()) {
                isLoading(true)
                val req = ProfileRequest().apply {
                    name = til_name?.value()
                    phone = til_phone?.value()
                    if (til_email?.value()?.isNotBlank() == true) {
                        email = til_email?.value()
                    }
                    if (switch_change_password?.isChecked == true) {
                        password = til_password_profile?.value()
                    }
                }
                doRequest {
                    presenter.updateProfile(req)
                }
            }
        }
    }

    private fun isAllValid(): Boolean {
        val validity = mutableListOf<Boolean>()
        if (til_email?.value()?.isNotBlank() == true)
            validity.add(
                til_email?.validate(
                    ValidatorType.Email
                ) == true
            )

        if (switch_change_password?.isChecked == true) {
            validity.add(
                til_password_profile?.validate(
                    ValidatorType.Required,
                    ValidatorType.MinimumLength(8)
                ) == true
            )
        }

        validity.add(
            til_phone?.validate(
                ValidatorType.Required,
                ValidatorType.MinimumLength(9),
                ValidatorType.MaximumLength(13)
            ) == true
        )

        validity.add(
            til_name?.validate(
                ValidatorType.Required
            ) == true
        )

        return !validity.contains(false)
    }

    private fun loadData() {
        isLoading(true)
        doRequest {
            presenter.getProfile()
        }
    }

    private fun isLoading(izLoading: Boolean) {
        if (izLoading) Utils.showProgress(this)
        else Utils.hideProgress()
    }

    override fun showError(title: String?, message: String?) {
        isLoading(false)
        super<BaseActivity>.showError(title, message)
    }

    override fun getProfileResponse(res: ProfileResponse) {
        super.getProfileResponse(res)
        isLoading(false)
        res.data?.apply {
            til_name?.setValue(name)
            til_phone?.setValue(phone)
            til_email?.setValue(email)
        }
    }

    override fun updateProfileResponse(res: ProfileResponse) {
        super.updateProfileResponse(res)
        isLoading(false)
        showSuccessSavedDialog(object : BaseDialogFragment.Listener {})
    }

}