package id.padangsambian.retribusiapppetugas.view

import InitialResponse
import android.os.Bundle
import android.os.Handler
import id.padangsambian.retribusiapp.presenter.SplashPresenter
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapppetugas.MainActivity
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.presenter.Auth
import id.padangsambian.retribusiapppetugas.view.auth.LoginActivity
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.padangsambian.retribusiapppetugas.view.executive.ExecutiveMainActivity
import id.widianapw.android_utils.extensions.goToActivity

class SplashActivity : BaseActivity(), SplashPresenter.Delegate {
    val presenter = SplashPresenter(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        doRequest {
            presenter.getInitial()
        }
    }

    override fun initialResponse(response: InitialResponse) {
        Handler().postDelayed({
            finish()
            if (preference.isLoggedIn) {
                if (Auth.user()?.role == "staff") {
                    goToActivity(MainActivity::class.java)
                } else {
                    goToActivity(ExecutiveMainActivity::class.java)
                }

            } else
                goToActivity(LoginActivity::class.java)
        }, 1000)

    }
}