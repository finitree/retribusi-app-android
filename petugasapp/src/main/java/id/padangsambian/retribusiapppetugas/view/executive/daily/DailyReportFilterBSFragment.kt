package id.padangsambian.retribusiapppetugas.view.executive.daily

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.shared.openDatePicker
import id.widianapw.android_utils.common.Const
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.toDateString
import id.widianapw.android_utils.extensions.validate
import id.widianapw.android_utils.extensions.value
import id.widianapw.android_utils.validator.ValidatorType
import kotlinx.android.synthetic.main.fragment_daily_report_filter_b_s.*

class DailyReportFilterBSFragment(
    val initial: String? = null,
    val onSave: (String) -> Unit,
    val onReset: () -> Unit

) : BottomSheetDialogFragment() {
    val DATE_FORMAT_API = "yyyy-MM-dd"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.AppTheme_BottomSheetDialog)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_daily_report_filter_b_s, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        til_date_filter?.editText?.apply {
            initial?.let {
                val formattedDate = it.toDateString(
                    fromFormat = DATE_FORMAT_API,
                    toFormat = Const.READ_DATE_FORMAT
                )
                setText(formattedDate)
            }
            onClick {
                openDatePicker(childFragmentManager, isValidateBackward = true)
            }
        }


        btn_cancel_filter?.onClick {
            this.dismiss()
        }

        btn_reset_filter?.onClick {
            this.dismiss()
            onReset()
        }

        btn_save_filter?.onClick {
            val valid = til_date_filter.validate(ValidatorType.Required)
            if (valid) {
                this.dismiss()
                til_date_filter?.value()
                    ?.toDateString(
                        fromFormat = Const.READ_DATE_FORMAT,
                        toFormat = DATE_FORMAT_API
                    )?.let { it1 ->
                        onSave(
                            it1
                        )
                    }
            }

        }

    }

}