package id.padangsambian.retribusiapppetugas.view.company

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.shared.days
import id.padangsambian.shared.model.CompanyOperationalItem
import kotlinx.android.synthetic.main.layout_rv_company_schedule.view.*

class CompanyOperationalAdapter() :
    RecyclerView.Adapter<CompanyOperationalAdapter.ViewHolder>() {
    private val list = mutableListOf<CompanyOperationalItem>()
    private var isEdit = false

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_rv_company_schedule, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.itemView.apply {
            val dayName = days.find { it.id == item.day }?.name
            tv_rv_company_schedule?.text =
                "${dayName}, ${item.opening_hours} - ${item.closing_hours}"
            chip_rv_company_is_open?.text = if (item.isOpenBool()) "Buka" else "Tutup"
            if (item.isOpenBool())
                chip_rv_company_is_open?.setChipBackgroundColorResource(R.color.color_lightest_primary)
            else
                chip_rv_company_is_open?.setChipBackgroundColorResource(R.color.color_light_error)

        }
    }

    override fun getItemCount(): Int = list.count()

    fun setData(data: List<CompanyOperationalItem>) {
        this.list.apply {
            clear()
            addAll(data)
        }
        notifyDataSetChanged()
    }

    fun setEditMode(izEdit: Boolean) {
        isEdit = izEdit
        notifyDataSetChanged()
    }

    fun updateItem(operational: CompanyOperationalItem) {
        val days = this.list.map { it.day }
        val index = days.indexOf(operational.day)
        this.list[index].apply {
            is_open = operational.is_open
            opening_hours = operational.opening_hours
            closing_hours = operational.closing_hours
        }
        notifyItemChanged(index)
    }

    fun getData(): List<CompanyOperationalItem> {
        return this.list
    }
}