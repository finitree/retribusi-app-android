package id.padangsambian.retribusiapppetugas.view.executive.staff

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.shared.model.user.Staff
import id.padangsambian.shared.toCurrency
import id.widianapw.android_utils.extensions.onClick
import kotlinx.android.synthetic.main.layout_rv_daily_report.view.tv_staff_name
import kotlinx.android.synthetic.main.layout_rv_staff_report.view.*

class StaffReportAdapter(val onClick: (Int) -> Unit) :
    RecyclerView.Adapter<StaffReportAdapter.ViewHolder>() {
    val list = mutableListOf<Staff>()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_rv_staff_report, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.itemView.apply {
            tv_staff_name?.text = item.user?.name
            tv_staff_balance?.text = item.balance?.toCurrency()
            tv_staff_companies?.text = item.companies?.map { it.name }?.joinToString()
            cv_staff?.onClick {
                item.id?.let { it1 -> onClick(it1) }
            }
        }
    }

    override fun getItemCount(): Int = list.count()

    fun setData(data: List<Staff>?) {
        this.apply {
            list.clear()
            data?.let { list.addAll(it) }
        }
        notifyDataSetChanged()
    }
}