package id.padangsambian.retribusiapppetugas.view.balance

import android.app.Dialog
import android.os.Bundle
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.model.BalanceRequest
import id.padangsambian.retribusiapppetugas.model.BalanceResponse
import id.padangsambian.retribusiapppetugas.model.profile.ProfileResponse
import id.padangsambian.retribusiapppetugas.presenter.balance.BalancePresenter
import id.padangsambian.retribusiapppetugas.presenter.profile.ProfilePresenter
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.padangsambian.shared.Utils
import id.padangsambian.shared.input.BaseDialogFragment
import id.padangsambian.shared.model.SuccessResponse
import id.padangsambian.shared.model.user.User
import id.padangsambian.shared.toCurrency
import id.widianapw.android_utils.common.Const
import id.widianapw.android_utils.extensions.*
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_store_balance.*
import kotlin.math.roundToInt

class StoreBalanceActivity : BaseActivity(), ProfilePresenter.Delegate, BalancePresenter.Delegate {
    private val profilePresenter = ProfilePresenter(this)
    private val balancePresenter = BalancePresenter(this)
    private var staffValue: Int? = null
    private var setorValue: Int? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_balance)
        setupToolbar("Setor ke Admin") {
            onBackPressed()
        }
        initView()
        loadData()
    }

    private fun loadData() {
//        isLoading(true)
        val realm = Realm.getDefaultInstance()
        val user = realm.where<User>().findFirst()
        val balance = user?.staff?.balance

        btn_store_balance?.isEnabled = balance.isNotNoll()

        tv_balance?.text = balance?.toCurrency()
        staffValue = balance?.times(0.2)?.roundToInt()
        setorValue = staffValue?.let { balance?.minus(it) }
        tv_setor_value?.text = setorValue?.toCurrency()
        tv_fee_petugas_value?.text = staffValue?.toCurrency()


        doRequest {
            profilePresenter.getProfile()
            balancePresenter.getBalance()
        }
    }

    override fun getBalanceResponse(res: BalanceResponse) {
        super.getBalanceResponse(res)
        val lastTrx = res.data?.find { it.type == "out" }
        if (lastTrx != null) {
            val words =
                "Penyetoran terakhir: ${lastTrx.created_at?.toDateString(toFormat = Const.READ_DATE_FORMAT)}"
            tv_prev_date_balance_store?.text = words
        }

    }

    private fun isLoading(izLoading: Boolean) {
        if (izLoading) Utils.showProgress(this)
        else Utils.hideProgress()
    }

    private fun initView() {

        btn_store_balance?.onClick {
//            val isValid =
//                til_store_amount?.validate(
//                    ValidatorType.Required,
//                    ValidatorType.MinimumValue(1000)
//                ) == true
//            if (isValid) {
            showConfirmationDialog(
                title = "Apakah anda yakin ingin melakukan setor uang ke admin?",
                listener = object : BaseDialogFragment.Listener {
                    override fun primaryClick(dialog: Dialog) {
                        super.primaryClick(dialog)
                        isLoading(true)
                        val req = BalanceRequest().apply {
                            setor_amount = setorValue
                            staff_amount = staffValue
                            notes = til_store_note?.value()
                        }
                        doRequest {
                            balancePresenter.storeBalance(req)
                        }
                    }
                }
            )
//            }
        }
    }

    override fun getProfileResponse(res: ProfileResponse) {
        super.getProfileResponse(res)
//        isLoading(false)
        tv_balance?.text = res.data?.staff?.balance?.toCurrency()
    }

    override fun showError(title: String?, message: String?) {
        isLoading(false)
        super<BaseActivity>.showError(title, message)
    }

    override fun storeBalanceResponse(res: SuccessResponse) {
        super.storeBalanceResponse(res)
        isLoading(false)
        finish()
        goToActivity(SuccessStoreBalanceActivity::class.java)
    }

}