package id.padangsambian.retribusiapppetugas.view.company

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

/**
 * Created by Widiana Putra on 09/12/21
 * Copyright (c) 2021 - Made with love
 */
class TransactionPagerAdapter(fm: FragmentManager, val companyId: Int?, val subscriptionType: String?) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val tabTitle = listOf("Periode Terbayar", "Periode Tidak Terbayar")
    private val types = listOf("paid", "unpaid")
    val activePages = hashMapOf<Int, Fragment>()

    override fun getCount(): Int {
        return types.count()
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return tabTitle[position]
    }

    override fun getItem(position: Int): Fragment {
        if (activePages[position] != null) {
            return activePages[position]!!
        }
        val fragment = TransactionFragment.newInstance(
            types[position], companyId, subscriptionType
        )
        activePages[position] = fragment
        return fragment
    }

    fun fetchFilterTransaction(req: FilterReq? = null) {
        activePages.values.forEach {
            val fragment = it as? TransactionFragment?
            fragment?.fetchData(req?.date_from, req?.date_to)
        }
    }
}