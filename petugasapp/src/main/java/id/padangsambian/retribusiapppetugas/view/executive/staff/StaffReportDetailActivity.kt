package id.padangsambian.retribusiapppetugas.view.executive.staff

import android.os.Bundle
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.model.DailyReportResponse
import id.padangsambian.retribusiapppetugas.presenter.report.ReportPresenter
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.padangsambian.retribusiapppetugas.view.executive.daily.DailyReportAdapter
import id.padangsambian.shared.openDatePicker
import id.widianapw.android_utils.common.Const
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.onTextChanged
import id.widianapw.android_utils.extensions.toDateString
import id.widianapw.android_utils.extensions.visible
import kotlinx.android.synthetic.main.activity_staff_report_detail.*

class StaffReportDetailActivity : BaseActivity(), ReportPresenter.Delegate {
    companion object {
        const val ID = "id"
    }

    private val presenter = ReportPresenter(this)
    private val adapter = DailyReportAdapter()
    private var staffId: Int? = null
    val DATE_FORMAT_API = "yyyy-MM-dd"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_staff_report_detail)
        setupToolbar("Laporan Detail Petugas") {
            onBackPressed()
        }
        staffId = intent.getIntExtra(ID, 0)
        initView()
        loadData()
    }

    private fun initView() {
        rv_daily_report?.adapter = adapter
        et_date_daily_report?.apply {
            onClick {
                this.openDatePicker(supportFragmentManager, isValidateBackward = true)
            }
            onTextChanged {
                tv_date_daily_report?.visible()
                tv_date_daily_report?.text = it
                loadData(it)
            }
        }

        sr_daily_report?.setOnRefreshListener {
            val text = et_date_daily_report?.text?.toString()
            if (text.isNullOrEmpty()){
                loadData()
            }else{
                loadData(text)
            }
        }
    }

    private fun loadData(date: String? = null) {
        val formatted = date?.toDateString(
            fromFormat = Const.READ_DATE_FORMAT,
            toFormat = DATE_FORMAT_API
        )
        doRequest {
            staffId?.let { presenter.getStaffDetailReport(it, formatted) }
        }
    }

    override fun getStaffDetailResponse(res: DailyReportResponse) {
        super.getStaffDetailResponse(res)
        sr_daily_report?.isRefreshing = false
        adapter.setData(res.data?.transactions)
    }

    override fun showError(title: String?, message: String?) {
        sr_daily_report?.isRefreshing = false
        super<BaseActivity>.showError(title, message)
    }

}