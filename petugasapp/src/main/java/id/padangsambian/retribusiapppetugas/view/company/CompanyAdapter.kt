package id.padangsambian.retribusiapppetugas.view.company

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.shared.getColor
import id.padangsambian.shared.model.Company
import id.widianapw.android_utils.common.Const
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.setVisibility
import id.widianapw.android_utils.extensions.toDateString
import kotlinx.android.synthetic.main.layout_rv_company.view.*

/**
 * Created by Widiana Putra on 27/12/2021
 * Copyright (c) 2021 - Made With Love
 */
class CompanyAdapter(val listener: (Company) -> Unit, val payListener: (Company) -> Unit) :
    RecyclerView.Adapter<CompanyAdapter.ViewHolder>() {
    private val list = mutableListOf<Company>()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.layout_rv_company, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.itemView.apply {
            chip_rv_company_transaction_status?.text = item.getPaymentStatus().status
            when (item.getPaymentStatus()) {
                is Company.PaymentStatus.Paid -> {
                    chip_rv_company_transaction_status?.apply {
                        setChipBackgroundColorResource(R.color.color_lightest_primary)
                        setTextColor(R.color.colorPrimary.getColor(context))
                    }
                }
                else -> {
                    chip_rv_company_transaction_status?.apply {
                        setChipBackgroundColorResource(R.color.color_light_error)
                        setTextColor(R.color.error.getColor(context))
                    }
                }
            }

            tv_rv_company_name?.text = item.name ?: "-"
            tv_rv_company_tempekan?.text = item.banjar?.name ?: "-"
            tv_rv_company_next_payment?.text =
                item.next_transaction?.date?.toDateString(
                    Const.READ_DATE_FORMAT,
                    isConvertToLocal = false
                )

            cv_close_company?.setVisibility(item.company_close?.is_close == true)
            layout_cuti_company?.setVisibility(item.company_close?.is_cuti == true)

            item.company_close?.schedule?.let {
                val READ_DATE = "dd MMMM yyyy"
                val dateStart = it.date_start?.toDateString(
                    fromFormat = "yyyy-MM-dd",
                    toFormat = READ_DATE
                )
                val dateEnd = it.date_end?.toDateString(
                    fromFormat = "yyyy-MM-dd",
                    toFormat = READ_DATE
                )
                if (dateStart == dateEnd) {
                    tv_rv_company_close?.text = dateStart
                } else {
                    tv_rv_company_close?.text = "$dateStart - $dateEnd"
                }
            }

            tv_rv_company_payment_type?.text =
                item.subscription?.subscription_type?.category ?: "-"

            btn_store_transaction?.onClick {
                payListener(item)
            }

            cv_company?.onClick {
                listener(item)
            }

            btn_store_transaction?.setVisibility((item.getPaymentStatus() is Company.PaymentStatus.Unpaid) && (item.company_close?.is_close != true))
        }
    }

    override fun getItemCount(): Int = list.count()

    fun setData(data: List<Company>?) {
        this.list.apply {
            clear()
            data?.let { addAll(it) }
        }
        notifyDataSetChanged()
    }
}