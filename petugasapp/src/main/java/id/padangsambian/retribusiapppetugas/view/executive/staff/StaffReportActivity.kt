package id.padangsambian.retribusiapppetugas.view.executive.staff

import android.content.Intent
import android.os.Bundle
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.model.StaffReportResponse
import id.padangsambian.retribusiapppetugas.presenter.report.ReportPresenter
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.padangsambian.shared.model.user.Staff
import id.widianapw.android_utils.extensions.onTextChanged
import kotlinx.android.synthetic.main.activity_staff_report.*

class StaffReportActivity : BaseActivity(), ReportPresenter.Delegate {
    private val presenter = ReportPresenter(this)
    private val adapter = StaffReportAdapter {
        with(Intent(this, StaffReportDetailActivity::class.java)) {
            putExtra(StaffReportDetailActivity.ID, it)
            startActivity(this)
        }
    }
    private var data: List<Staff>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_staff_report)
        setupToolbar("Laporan Petugas") {
            onBackPressed()
        }

        initView()
        loadData()
    }


    private fun initView() {
        rv_staff_report?.adapter = adapter
        sr_staff_report?.setOnRefreshListener {
            loadData()
        }

        til_search_staff?.editText?.onTextChanged {
            filterList(it)
        }
    }

    private fun filterList(search: String) {
        val filteredList = if (search.isEmpty()) {
            data
        } else {
            data?.filter { it.user?.name?.lowercase()?.contains(search.lowercase()) == true }
                ?.toList()
        }
        adapter.setData(filteredList)
    }

    private fun loadData() {
        doRequest {
            presenter.getStaffReport()
        }
    }

    override fun getStaffResponse(res: StaffReportResponse) {
        super.getStaffResponse(res)
        sr_staff_report?.isRefreshing = false
        data = res.data
        adapter.setData(res.data)
    }

    override fun showError(title: String?, message: String?) {
        super<BaseActivity>.showError(title, message)
        sr_staff_report?.isRefreshing = false
    }
}