package id.padangsambian.retribusiapppetugas.view.auth

import android.os.Bundle
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapppetugas.MainActivity
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.model.auth.LoginRequest
import id.padangsambian.retribusiapppetugas.model.auth.LoginResponse
import id.padangsambian.retribusiapppetugas.presenter.auth.AuthPresenter
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.padangsambian.retribusiapppetugas.view.executive.ExecutiveMainActivity
import id.padangsambian.shared.Utils
import id.widianapw.android_utils.extensions.goToActivity
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.validate
import id.widianapw.android_utils.extensions.value
import id.widianapw.android_utils.validator.ValidatorType
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity(), AuthPresenter.Delegate {
    private val presenter = AuthPresenter(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initListener()
    }

    private fun initListener() {
        btn_forgot_password?.onClick {
            goToActivity(ForgotPasswordActivity::class.java)
        }

        btn_login?.onClick {
            if (isAllValid()) {
                isLoading(true)
                val request = LoginRequest().apply {
                    phone = til_phone_login?.value()
                    password = til_password_login?.value()
                }
                doRequest {
                    presenter.login(request)
                }
            }

        }
    }

    override fun loginResponse(res: LoginResponse) {
        super.loginResponse(res)
        preference.apply {
            accessToken = res.data?.access_token
            isLoggedIn = true
        }
        finishAffinity()
        if (res.data?.user?.role == "staff") {
            goToActivity(MainActivity::class.java)
        } else {
            goToActivity(ExecutiveMainActivity::class.java)
        }
    }

    private fun isAllValid(): Boolean {
        val validity = mutableListOf<Boolean>()
        validity.add(
            til_phone_login?.validate(
                ValidatorType.Required,
                ValidatorType.MinimumLength(9),
                ValidatorType.MaximumLength(14)
            ) == true
        )

        validity.add(
            til_password_login?.validate(
                ValidatorType.Required, ValidatorType.MinimumLength(8)
            ) == true
        )

        return !validity.contains(false)
    }

    private fun isLoading(izLoading: Boolean) {
        if (izLoading) Utils.showProgress(this)
        else Utils.hideProgress()
    }

    override fun showError(title: String?, message: String?) {
        isLoading(false)
        super<BaseActivity>.showError(title, message)
    }
}