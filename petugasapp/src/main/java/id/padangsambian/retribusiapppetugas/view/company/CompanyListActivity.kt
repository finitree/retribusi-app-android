package id.padangsambian.retribusiapppetugas.view.company

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.model.CompanyListResponse
import id.padangsambian.retribusiapppetugas.presenter.company.CompanyPresenter
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.padangsambian.shared.getColor
import id.padangsambian.shared.input.BaseDialogFragment
import id.padangsambian.shared.model.Company
import id.padangsambian.shared.model.SuccessResponse
import id.padangsambian.shared.setValue
import id.widianapw.android_utils.extensions.onTextChanged
import id.widianapw.android_utils.extensions.setVisibility
import kotlinx.android.synthetic.main.activity_company_list.*
import kotlinx.android.synthetic.main.fragment_appbar.*

class CompanyListActivity : BaseActivity(), CompanyPresenter.Delegate {
    private val presenter = CompanyPresenter(this)
    private var adapter: CompanyAdapter? = null
    private var companyList: List<Company>? = null
    var filterIcon: MenuItem? = null
    var filteredType: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company_list)
        setupToolbar("Daftar Usaha") {
            onBackPressed()
        }
        setSupportActionBar(tl_main)
        supportActionBar?.setDisplayHomeAsUpEnabled(true);
        supportActionBar?.setDisplayShowHomeEnabled(true);

        initView()
        loadData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_filter, menu)
        filterIcon = menu?.findItem(R.id.action_filter)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_filter -> {
                openFilterBS()
            }
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return true
    }

    private fun openFilterBS() {
        val bs = CompanyFilterBS(filteredType) {
            filteredType = it
            loadData(it)
            if (it != null) {
                filterIcon?.icon?.setTint(R.color.colorPrimary.getColor(this))
            } else {
                filterIcon?.icon?.setTint(R.color.quantum_grey600.getColor(this))
            }
        }
        bs.show(supportFragmentManager, "")
    }


    private fun initView() {
        adapter = CompanyAdapter({
            with(Intent(this, CompanyDetailActivity::class.java)) {
                putExtra(CompanyDetailActivity.ID, it.id)
                startActivity(this)
            }
        }, {
            doRetributionPayment(it, {
                isLoading(true)
                doRequest {
                    presenter.postTransaction(it)
                }
            }, {
                isLoading(true)
                doRequest {
                    presenter.postInstalmentTransaction(it)
                }
            })
        })
        rv_company_list?.adapter = adapter

        sr_company?.setOnRefreshListener {
            til_search_company?.setValue("")
            loadData()
        }

        til_search_company?.editText?.onTextChanged { value ->
            val list = if (value.isEmpty()) companyList?.toList() else companyList?.filter {
                it.name?.contains(value) == true
            }?.toList()
            adapter?.setData(list)
        }
    }

    private fun loadData(type: String? = null) {
        isLoading(true)
        doRequest {
            presenter.getCompany(type)
        }
    }

    private fun isLoading(izLoading: Boolean) {
        pb_company?.setVisibility(izLoading)
        if (!izLoading) sr_company?.isRefreshing = false
    }

    override fun getCompanyResponse(res: CompanyListResponse) {
        super.getCompanyResponse(res)
        isLoading(false)
        companyList = res.data
        adapter?.setData(res.data)
    }

    override fun showError(title: String?, message: String?) {
        isLoading(false)
        super<BaseActivity>.showError(title, message)

    }

    override fun postTransactionResponse(res: SuccessResponse) {
        super.postTransactionResponse(res)
        isLoading(false)
        showSuccessDialog(
            "Transaksi berhasil dilakukan",
            listener = object : BaseDialogFragment.Listener {})

        til_search_company?.setValue("")
        loadData()
    }
}