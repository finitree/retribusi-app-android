package id.padangsambian.retribusiapppetugas.view.owner

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.model.OwnerCompanyItem
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.padangsambian.retribusiapppetugas.view.company.TransactionDetailDialogFragment
import id.padangsambian.shared.toCurrency
import id.widianapw.android_utils.extensions.isNotNoll
import id.widianapw.android_utils.extensions.loadImage
import id.widianapw.android_utils.extensions.onClick
import kotlinx.android.synthetic.main.layout_rv_owner_company.view.*

class OwnerCompanyAdapter(val activity: BaseActivity, val onPay: (OwnerCompanyItem) -> Unit, val onCallOwner: (OwnerCompanyItem) -> Unit) :
    RecyclerView.Adapter<OwnerCompanyAdapter.ViewHolder>() {
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    val list = mutableListOf<OwnerCompanyItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_rv_owner_company, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        val adapter = OwnerCompanyTransactionAdapter {
            val dialog = TransactionDetailDialogFragment(it)
            dialog.show(activity.supportFragmentManager, "")
        }
        holder.itemView.apply {
            val totalOwner =
                item.company?.sumOf { it.unpaid_transaction?.sumOf { un -> un.amount ?: 0 } ?: 0 }
            rv_owner_company_unpaid?.adapter = adapter
            adapter.setData(item.company)
            tv_rv_owner_company_total?.text = totalOwner?.toCurrency()
            tv_rv_owner_name_company?.text = "${item.owner?.name}"
            btn_rv_payment_owner_company?.onClick {
                onPay(item)
            }
            btn_rv_payment_owner_company?.isEnabled = totalOwner.isNotNoll()

            iv_owner?.loadImage(item.owner?.photo)
            tv_email_owner?.text = item.owner?.email
            tv_phone_owner?.text = item.owner?.phone

            btn_call_owner?.onClick {
                onCallOwner(item)
            }

        }
    }


    override fun getItemCount(): Int = list.count()

    fun setData(data: List<OwnerCompanyItem>?) {
        list.apply {
            clear()
            data?.let { addAll(it) }
        }
        notifyDataSetChanged()
    }
}