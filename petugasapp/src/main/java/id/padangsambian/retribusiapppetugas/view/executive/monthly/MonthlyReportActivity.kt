package id.padangsambian.retribusiapppetugas.view.executive.monthly

import android.os.Bundle
import com.whiteelephant.monthpicker.MonthPickerDialog
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.model.DailyReportResponse
import id.padangsambian.retribusiapppetugas.presenter.report.ReportPresenter
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.padangsambian.retribusiapppetugas.view.executive.daily.DailyReportAdapter
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.onTextChanged
import kotlinx.android.synthetic.main.activity_daily_report.*
import kotlinx.android.synthetic.main.activity_monthly_report.*
import java.util.*

class MonthlyReportActivity : BaseActivity(), ReportPresenter.Delegate {
    private val reportPresenter = ReportPresenter(this)
    private val adapter = DailyReportAdapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monthly_report)
        setupToolbar("Laporan Bulanan") {
            onBackPressed()
        }

        initView()
    }

    private fun getMonthString(month: Int): String {
        val result = when (month) {
            0 -> "Jan"
            1 -> "Feb"
            2 -> "Mar"
            3 -> "Apr"
            4 -> "May"
            5 -> "Jun"
            6 -> "Jul"
            7 -> "Aug"
            8 -> "Sept"
            9 -> "Oct"
            10 -> "Nov"
            11 -> "Dec"
            else -> {
                "Apr"
            }
        }
        return result
    }

    private fun initView() {
        val today = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
        val builder = MonthPickerDialog.Builder(
            this, { selectedMonth, selectedYear ->
                val inputDateOfBirth = "${getMonthString(selectedMonth)} $selectedYear"
                et_month_daily_report.setText(inputDateOfBirth)
            },
            today.get(Calendar.YEAR),
            today.get(Calendar.MONTH)
        )

        builder.setActivatedMonth(today.get(Calendar.MONTH))
            .setMinYear(1980)
            .setActivatedYear(today.get(Calendar.YEAR))
            .setMaxYear(today.get(Calendar.YEAR))
            .setTitle("Pilih Bulan")
            .setMonthRange(Calendar.JANUARY, Calendar.DECEMBER)

        et_month_daily_report?.apply {
            onClick {
                builder.build().show()
            }
            onTextChanged {
                loadData(it)
            }
        }

        sr_monthly_report?.setOnRefreshListener {
            if (et_date_daily_report?.text?.toString().isNullOrEmpty()) {
                sr_monthly_report?.isRefreshing = false
            } else {
                et_date_daily_report?.text?.toString()?.let { loadData(it) }
            }
        }

        rv_monthly_report?.adapter = adapter
    }

    fun loadData(month: String) {
        doRequest {
            reportPresenter.getMonthlyReport(month)
        }
    }

    override fun getMonthReportResponse(res: DailyReportResponse) {
        super.getMonthReportResponse(res)
        sr_monthly_report?.isRefreshing = false
        adapter.setData(res.data?.transactions)
    }

    override fun showError(title: String?, message: String?) {
        sr_daily_report?.isRefreshing = false
        super<BaseActivity>.showError(title, message)
    }
}