package id.padangsambian.retribusiapppetugas.view.balance

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.model.BalanceItem
import id.padangsambian.shared.getColor
import id.padangsambian.shared.toCurrency
import id.widianapw.android_utils.extensions.setVisibility
import kotlinx.android.synthetic.main.layout_rv_balance.view.*

/**
 * Created by Widiana Putra on 29/12/2021
 * Copyright (c) 2021 - Made With Love
 */
class BalanceAdapter : RecyclerView.Adapter<BalanceAdapter.ViewHolder>() {
    private val list = mutableListOf<BalanceItem>()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.layout_rv_balance, parent, false)
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.itemView.apply {
            val isShowDateSeparator =
                if (position == 0 || position == list.lastIndex) true
                else if (position != 0)
                    item.getFormattedDate() != list[position - 1].getFormattedDate()
                else false

            tv_date_separator?.setVisibility(isShowDateSeparator)
            tv_date_separator?.text = item.getFormattedDate()

            val notes = item.notes ?: ""
            if (item.type == "in") {
                tv_title?.text = "Tagihan Retribusi Usaha"
                tv_subtitle?.text = "Uang masuk. " + notes
                tv_nominal?.text = "+" + item.amount?.toCurrency()
                tv_nominal?.setTextColor(R.color.color_primary.getColor(context))

            } else {
                tv_title?.text = "Transfer ke Admin"
                tv_subtitle?.text = "Uang keluar. " + notes
                tv_nominal?.text = "-" + item.amount?.toCurrency()
                tv_nominal?.setTextColor(R.color.color_primary_error.getColor(context))
            }

        }
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    fun setData(data: List<BalanceItem>?) {
        this.list.apply {
            clear()
            data?.let { addAll(it) }
        }
        notifyDataSetChanged()
    }
}