package id.padangsambian.retribusiapppetugas.view.balance

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.model.BalanceResponse
import id.padangsambian.retribusiapppetugas.presenter.balance.BalancePresenter
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.widianapw.android_utils.extensions.gone
import id.widianapw.android_utils.extensions.visible
import kotlinx.android.synthetic.main.activity_balance.*

class BalanceActivity : BaseActivity(), BalancePresenter.Delegate {
    private val presenter = BalancePresenter(this)
    private var adapter: BalanceAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_balance)
        setupToolbar("Riwayat Saldo") {
            onBackPressed()
        }
        initView()
        loadData(true)

    }

    private fun initView() {
        sr_balance?.setOnRefreshListener {
            loadData()
        }
        adapter = BalanceAdapter()
        rv_balance?.adapter = adapter
    }

    private fun loadData(isFirst: Boolean = false) {
        isLoading(true, isFirst)
        doRequest {
            presenter.getBalance()
        }
    }

    private fun isLoading(izLoading: Boolean, isFirst: Boolean = false) {
        if (izLoading) {
            if (isFirst) pb_balance?.visible()
        } else {
            sr_balance?.isRefreshing = false
            pb_balance?.gone()
        }
    }

    override fun getBalanceResponse(res: BalanceResponse) {
        super.getBalanceResponse(res)
        isLoading(false)
        adapter?.setData(res.data)
    }

    override fun showError(title: String?, message: String?) {
        isLoading(false)
        super<BaseActivity>.showError(title, message)
    }

}