package id.padangsambian.retribusiapppetugas.view.company

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.padangsambian.retribusiapppetugas.R
import id.widianapw.android_utils.extensions.onClick
import kotlinx.android.synthetic.main.fragment_company_transaction_option_b_s.*

class CompanyTransactionOptionBS(val lunasAction: () -> Unit, val cicilAction: () -> Unit) :
    BottomSheetDialogFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_BottomSheetDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_company_transaction_option_b_s, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_pay_lunas?.onClick {
            this.dismiss()
            lunasAction()
        }

        btn_pay_cicil?.onClick {
            this.dismiss()
            cicilAction()
        }
    }
}