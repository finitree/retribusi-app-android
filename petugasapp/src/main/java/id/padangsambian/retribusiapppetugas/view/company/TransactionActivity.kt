package id.padangsambian.retribusiapppetugas.view.company

import android.os.Bundle
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.padangsambian.shared.getColor
import kotlinx.android.synthetic.main.activity_transaction.*

class TransactionActivity : BaseActivity() {
    companion object {
        const val ID = "id"
        const val SUBSCRIPTION_TYPE = "subscription_type"
    }

    private var initialFilter: FilterReq? = null

    private var pagerAdapter: TransactionPagerAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction)
        val id = intent.getIntExtra(ID, 0)
        val subscriptionType = intent.getStringExtra(SUBSCRIPTION_TYPE)
        pagerAdapter = TransactionPagerAdapter(supportFragmentManager, id, subscriptionType)
        tl_transaction?.setNavigationOnClickListener {
            onBackPressed()
        }
        tl_transaction?.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.action_filter -> {
                    openBSFilterTransaction()
                    true
                }
                else -> false
            }
        }

        vp_transaction?.adapter = pagerAdapter
        tab_transaction?.setupWithViewPager(vp_transaction)
    }

    private fun openBSFilterTransaction() {
        val bs = TransactionFilterBSFragment(initialFilter, {
            tl_transaction?.menu?.findItem(R.id.action_filter)?.icon?.setTint(
                R.color.colorPrimary.getColor(
                    this
                )
            )
            initialFilter = it
            pagerAdapter?.fetchFilterTransaction(it)
        }, {
            tl_transaction?.menu?.findItem(R.id.action_filter)?.icon?.setTint(
                R.color.black.getColor(
                    this
                )
            )
            initialFilter = null
            pagerAdapter?.fetchFilterTransaction(null)
        })
        bs.show(supportFragmentManager, "")
    }

}