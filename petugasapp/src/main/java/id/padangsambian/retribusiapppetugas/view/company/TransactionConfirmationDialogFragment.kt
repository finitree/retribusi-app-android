package id.padangsambian.retribusiapppetugas.view.company

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.shared.model.UnpaidTransactionItem
import id.padangsambian.shared.toCurrency
import id.widianapw.android_utils.extensions.onClick

class TransactionConfirmationDialogFragment(
    val unpaidTransactions: List<UnpaidTransactionItem>?=null,
    val moneyPay: Int?=null,
    val moneyChanges: Int?=null,
    val onSave: () -> Unit
) : DialogFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaction_confirmation_dialog, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = layoutInflater.inflate(R.layout.fragment_transaction_confirmation_dialog, null)
        val tvPaymentTotal = view.findViewById<TextView>(R.id.tv_payment_total)
        val tvMoneyPay = view.findViewById<TextView>(R.id.tv_payment_pay)
        val tvMoneyChanges = view.findViewById<TextView>(R.id.tv_payment_changes)

        val rvUnpaid = view.findViewById<RecyclerView>(R.id.rv_unpaid_transaction)
        val btnClose = view.findViewById<ImageButton>(R.id.btn_close)
        val btnSave = view.findViewById<MaterialButton>(R.id.btn_save)

        val builder = context?.let {
            MaterialAlertDialogBuilder(it).run {
                setView(view)
                create()

            }
        }

        builder?.let {
//            tvNominalLabel?.text =
//                "Nominal Bayar (${company?.subscription?.subscription_type?.category})"
//
//            val payText =
//                "${company?.next_transaction?.subscription_qty} x ${company?.next_transaction?.subscription_amount}"
//            tvPayment?.text = payText
            val adapter = TransactionDetailUnpaidAdapter()
            rvUnpaid?.adapter = adapter
            adapter.setData(unpaidTransactions)

            val paymentTotal = unpaidTransactions?.sumOf { it.amount ?: 0 }
            tvPaymentTotal?.text = paymentTotal?.toCurrency()
            tvMoneyPay?.text =moneyPay?.toCurrency()
            tvMoneyChanges?.text =moneyChanges?.toCurrency()

            btnSave?.onClick {
                dismiss()
                onSave()
            }

            btnClose?.onClick {
                dismiss()
            }
        }
        return builder!!
    }


}