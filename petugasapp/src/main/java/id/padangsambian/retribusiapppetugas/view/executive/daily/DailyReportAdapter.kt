package id.padangsambian.retribusiapppetugas.view.executive.daily

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.model.DailyReportItem
import id.padangsambian.shared.toCurrency
import id.widianapw.android_utils.common.Const
import id.widianapw.android_utils.extensions.toDateString
import kotlinx.android.synthetic.main.layout_rv_daily_report.view.*

class DailyReportAdapter : RecyclerView.Adapter<DailyReportAdapter.ViewHolder>() {
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    val list = mutableListOf<DailyReportItem>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_rv_daily_report, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.itemView.apply {
            tv_rv_company_daily_report?.text = item.company?.name
            tv_rv_total_transaction_daily_report?.text = item.total_amount?.toCurrency()
            tv_staff_name?.text = "Petugas: ${item.staff?.user?.name}"
            tv_rv_date?.text = item.created_at?.toDateString(Const.READ_DATE_FORMAT, fromFormat = "yyyy-MM-dd HH:mm:ss")
        }
    }

    override fun getItemCount(): Int = list.count()

    fun setData(data: List<DailyReportItem>?) {
        list.apply {
            clear()
            data?.let { addAll(it) }
        }
        notifyDataSetChanged()
    }
}