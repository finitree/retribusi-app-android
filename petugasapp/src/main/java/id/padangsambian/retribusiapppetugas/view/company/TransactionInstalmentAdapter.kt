package id.padangsambian.retribusiapppetugas.view.company

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.shared.model.UnpaidTransactionItem
import id.padangsambian.shared.toCurrency
import kotlinx.android.synthetic.main.layout_rv_instalment_transaction.view.*

class TransactionInstalmentAdapter(val onPriceChange: (Int) -> Unit) :
    RecyclerView.Adapter<TransactionInstalmentAdapter.ViewHolder>() {
    private val list = mutableListOf<UnpaidTransactionItem>()
    private val checkedListId = mutableListOf<Int>()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_rv_instalment_transaction, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.itemView.apply {
            cb_date?.text = item.unpaidAtFormatted
            tv_price?.text = item.amount?.toCurrency()
            cb_date?.isChecked = checkedListId.contains(item.id)
            cb_date?.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    item.id?.let { checkedListId.add(it) }
                } else {
                    checkedListId.remove(item.id)
                }
                calc()
            }
        }
    }

    private fun calc() {
        var total = 0
        list.forEach {
            if (checkedListId.contains(it.id)) {
                it.amount?.let { it1 ->
                    total += it1
                }
            }
        }
        onPriceChange(total)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setData(data: List<UnpaidTransactionItem>) {
        this.list.apply {
            clear()
            addAll(data)
        }
        notifyDataSetChanged()
    }

    fun getUnpaidIds(): List<Int> {
        return checkedListId
    }

    fun getUnpaidSelectedItems(): List<UnpaidTransactionItem> {
        return list.filter { checkedListId.contains(it.id) }
    }
}