package id.padangsambian.retribusiapppetugas.view.executive

import android.app.Dialog
import android.os.Bundle
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.presenter.Auth
import id.padangsambian.retribusiapppetugas.view.auth.LoginActivity
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.padangsambian.retribusiapppetugas.view.executive.daily.DailyReportActivity
import id.padangsambian.retribusiapppetugas.view.executive.monthly.MonthlyReportActivity
import id.padangsambian.retribusiapppetugas.view.executive.staff.StaffReportActivity
import id.padangsambian.shared.input.BaseDialogFragment
import id.widianapw.android_utils.extensions.goToActivity
import id.widianapw.android_utils.extensions.onClick
import kotlinx.android.synthetic.main.activity_executive_main.*

class ExecutiveMainActivity : BaseActivity() {
//    private val reportPresenter = ReportPresenter(this)
//    private val dailyReportAdapter = DailyReportAdapter()
//    val DATE_FORMAT_API = "yyyy-MM-dd"
//    var selectedDate: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_executive_main)
        initView()
//        loadData()
    }

    private fun initView() {
        tv_ex_name?.text = Auth.user()?.name
        tv_daily_report?.onClick {
            goToActivity(DailyReportActivity::class.java)
        }
        tv_monthly_report?.onClick {
            goToActivity(MonthlyReportActivity::class.java)
        }
        tv_staff_report?.onClick {
            goToActivity(StaffReportActivity::class.java)
        }

        btn_logout_executive?.onClick {
            showConfirmationDialog(
                title = "Apakah anda yakin ingin keluar dari akun ini?",
                listener = object : BaseDialogFragment.Listener {
                    override fun primaryClick(dialog: Dialog) {
                        super.primaryClick(dialog)
                        preference.isLoggedIn = false
                        finishAffinity()
                        goToActivity(LoginActivity::class.java)
                    }
                }
            )
        }

    }

//    private fun loadData(date: String? = null) {
//        tv_date_daily_report?.text = date?.toDateString(
//            Const.READ_DATE_FORMAT,
//            fromFormat = DATE_FORMAT_API
//        )
//            ?: Utils.getCurrentDateTime(DATE_FORMAT_API)
//                .toDateString(Const.READ_DATE_FORMAT, fromFormat = DATE_FORMAT_API)
//        doRequest {
//            reportPresenter.getDailyReport(date)
//        }
//    }

//    private fun initView() {
//        ib_filter_daily_report?.onClick {
//            val bs = DailyReportFilterBSFragment(selectedDate, {
//                selectedDate = it
//                loadData(it)
//            }, {
//                selectedDate = null
//                loadData(null)
//            })
//            bs.show(supportFragmentManager, "")
//
//        }
//        sr_daily_report?.setOnRefreshListener {
//            loadData(selectedDate)
//        }
//
//        rv_daily_report?.adapter = dailyReportAdapter
//
//        btn_logout_executive?.onClick {
//            showConfirmationDialog(
//                title = "Apakah anda yakin ingin keluar dari akun ini?",
//                listener = object : BaseDialogFragment.Listener {
//                    override fun primaryClick(dialog: Dialog) {
//                        super.primaryClick(dialog)
//                        preference.isLoggedIn = false
//                        finishAffinity()
//                        goToActivity(LoginActivity::class.java)
//                    }
//                }
//            )
//        }
//
//    }

//    override fun getDailyReportResponse(res: DailyReportResponse) {
//        super.getDailyReportResponse(res)
//        sr_daily_report?.isRefreshing = false
//        tv_total_transaction_value?.text = res.data?.total_transaction?.toCurrency()
//        tv_qty_transaction_value?.text = res.data?.total_qty?.toString()
//        dailyReportAdapter.setData(res.data?.transactions)
//    }
//
//    override fun showError(title: String?, message: String?) {
//        sr_daily_report?.isRefreshing = false
//        super<BaseActivity>.showError(title, message)
//    }
}