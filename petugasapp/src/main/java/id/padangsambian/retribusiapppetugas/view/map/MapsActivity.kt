package id.padangsambian.retribusiapppetugas.view.map

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import id.padangsambian.retribusiapp.utilities.constant.Permissions
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.model.CompanyListResponse
import id.padangsambian.retribusiapppetugas.presenter.company.CompanyPresenter
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.padangsambian.retribusiapppetugas.view.company.CompanyDetailActivity
import id.padangsambian.retribusiapppetugas.view.company.CompanyFilterBS
import id.padangsambian.shared.Utils
import id.padangsambian.shared.getColor
import id.padangsambian.shared.input.BaseDialogFragment
import id.padangsambian.shared.model.Company
import id.padangsambian.shared.model.SuccessResponse
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.setVisibility
import id.widianapw.android_utils.extensions.toDateString
import kotlinx.android.synthetic.main.activity_maps.*


class MapsActivity : BaseActivity(), CompanyPresenter.Delegate, OnMapReadyCallback,
    GoogleMap.OnMarkerClickListener {
    companion object {
        const val STATE = "state"
        const val ID = "id"
    }

    enum class State {
        DETAIL, LIST
    }

    private var map: GoogleMap? = null
    private var state: State? = null
    private var initialId: Int? = null

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private val presenter = CompanyPresenter(this)
    private val markers = mutableListOf<Marker>()
    private val companies = mutableListOf<Company>()
    private var filteredType: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        state = try {
            intent.getSerializableExtra(STATE) as State
        } catch (e: Exception) {
            State.LIST
        }
        initialId = intent.getIntExtra(ID, 0)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(p0: GoogleMap) {
        map = p0
        initView()
        loadData(filteredType)
        if (state == State.LIST) {
            getDeviceLocation()
        }
    }

    private fun loadData(type: String? = null) {
        doRequest {
            presenter.getCompany(type)
        }
    }

    private fun initView() {
        btn_back_map?.onClick {
            onBackPressed()
        }

        cv_filter_map?.onClick {
            openFilterBS()
        }
        iv_filter_map?.onClick {
            openFilterBS()
        }

        map?.setOnMarkerClickListener(this)
        map?.setOnMapClickListener {
            if (layout_expand_map?.isExpanded == true) {
                layout_expand_map?.collapse()
            }
        }
    }

    private fun openFilterBS() {
        val bs = CompanyFilterBS(filteredType) {
            filteredType = it
            if (it == null) {
                iv_filter_map?.setColorFilter(R.color.quantum_grey600.getColor(this))
            } else {
                iv_filter_map?.setColorFilter(R.color.colorPrimary.getColor(this))
            }
            loadData(it)
        }
        bs.show(supportFragmentManager, "")
    }

    private fun getDeviceLocation() {
        if (checkPermission(Permissions.locations)) {
            // Construct a FusedLocationProviderClient.
            map?.isMyLocationEnabled = true
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
            fusedLocationProviderClient.lastLocation.addOnCompleteListener {
                val currentLocation = it.result
                val position = LatLng(currentLocation.latitude, currentLocation.longitude)
                moveCamera(position)
            }
        }
    }


    private fun moveCamera(latLng: LatLng, zoom: Float = 18f) {
        map?.animateCamera(zoom.let { CameraUpdateFactory.newLatLngZoom(latLng, it) })
    }

    override fun getCompanyResponse(res: CompanyListResponse) {
        super.getCompanyResponse(res)
        companies.apply {
            clear()
            res.data?.let { addAll(it) }
        }
        markers.apply {
            this.map { it.remove() }
            clear()

            if (state == State.LIST)
                res.data?.forEach {
                    val latLng = LatLng(it.latitude ?: 0.0, it.longitude ?: 0.0)
                    val icon = if (it.company_close?.is_close == true) {
                        R.drawable.ic_marker_closed
                    } else {
                        if (it.getPaymentStatus() is Company.PaymentStatus.Paid) R.drawable.ic_marker_paid else R.drawable.ic_marker_unpaid
                    }
                    val marker = map?.addMarker(
                        MarkerOptions().position(latLng)
                            .icon(BitmapDescriptorFactory.fromResource(icon))
                    )
                    marker?.tag = it.id
                    marker?.let { it1 -> markers.add(it1) }
                }
            else {
                companies.find { it.id == initialId }?.let {
                    val latLng = LatLng(it.latitude ?: 0.0, it.longitude ?: 0.0)
                    moveCamera(latLng)
                    layout_expand_map?.isExpanded = true
                    it.id?.let { it1 -> setViewFromId(it1) }

                    val icon =
                        if (it.getPaymentStatus() is Company.PaymentStatus.Paid) R.drawable.ic_marker_paid else R.drawable.ic_marker_unpaid
                    val marker = map?.addMarker(
                        MarkerOptions().position(latLng)
                            .icon(BitmapDescriptorFactory.fromResource(icon))
                    )
                    marker?.tag = it.id
                    marker?.let { it1 -> markers.add(it1) }
                }
            }
        }

    }

    override fun onMarkerClick(p0: Marker): Boolean {
        val id = p0.tag as Int
        setViewFromId(id)
        return true
    }

    private fun setViewFromId(id: Int) {
        val company = companies.find { it.id == id }

        val pos = LatLng(company?.latitude ?: 0.0, company?.longitude ?: 0.0)
        moveCamera(pos)

        company?.apply {

            tv_company_name_map?.text = name
            tv_company_address_map?.text = address

            tv_company_type_map?.text = company_type?.type
            tv_company_tempekan_map?.text = banjar?.name ?: "-"
            tv_company_payment_type_map?.text = subscription?.subscription_type?.category ?: "-"
            tv_company_payment_map?.text = getPaymentStatus().status

            val open = if (company_close?.is_close == true) "Tutup" else "Buka"
            val openDesc = if (company_close?.is_cuti == true) {
                company_close?.schedule?.let {
                    val READ_DATE = "dd MMMM yyyy"
                    val dateStart = it.date_start?.toDateString(
                        fromFormat = "yyyy-MM-dd",
                        toFormat = READ_DATE
                    )
                    val dateEnd = it.date_end?.toDateString(
                        fromFormat = "yyyy-MM-dd",
                        toFormat = READ_DATE
                    )
                    if (dateStart == dateEnd) {
                        dateStart
                    } else {
                        "$dateStart - $dateEnd"
                    }
                }
            } else {
                if (company_close?.is_close != true) {
                    "${company_close?.current_schedule?.opening_hours} - ${company_close?.current_schedule?.closing_hours}"
                } else ""
            }
            tv_close_company_bs?.text = "${open} $openDesc"

            btn_company_detail_map?.onClick {
                with(Intent(this@MapsActivity, CompanyDetailActivity::class.java)) {
                    putExtra(CompanyDetailActivity.ID, id)
                    startActivity(this)
                }
            }

            btn_map?.onClick {
                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?daddr=$latitude,$longitude")
                )
                startActivity(intent)
            }

            btn_call_map?.onClick {
                val intent = Intent(Intent.ACTION_DIAL);
                intent.data = Uri.parse("tel:" + user?.phone);
                if (intent.resolveActivity(packageManager) != null) {
                    startActivity(intent);
                }
            }


            btn_store_map?.setVisibility((getPaymentStatus() is Company.PaymentStatus.Unpaid) && (company_close?.is_close != true))
            btn_store_map?.onClick {
                doRetributionPayment(this, {
                    isLoading(true)
                    doRequest {
                        presenter.postTransaction(it)
                    }
                }, {
                    isLoading(true)
                    doRequest {
                        presenter.postInstalmentTransaction(it)
                    }
                })
            }


            layout_button_action_map?.setVisibility(state == State.LIST)
        }
        layout_expand_map?.isExpanded = true
    }

    private fun isLoading(izLoading: Boolean) {
        if (izLoading) Utils.showProgress(this)
        else Utils.hideProgress()
    }

    override fun showError(title: String?, message: String?) {
        isLoading(false)
        super<BaseActivity>.showError(title, message)
    }

    override fun postTransactionResponse(res: SuccessResponse) {
        super.postTransactionResponse(res)
        isLoading(false)
        loadData(filteredType)
        showSuccessDialog(
            "Transaksi berhasil dilakukan",
            listener = object : BaseDialogFragment.Listener {})
    }
}