package id.padangsambian.retribusiapppetugas.view.company

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.orhanobut.logger.Logger
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.shared.model.Company
import id.padangsambian.shared.toCurrency
import id.widianapw.android_utils.extensions.isNotNoll
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.onTextChanged
import id.widianapw.android_utils.extensions.value
import kotlinx.android.synthetic.main.fragment_transaction_instalment_dialog.*


data class TransactionInstalmentRequest(
    var unpaid_ids: List<Int>? = null,
    var money_pay: Int? = null,
    var money_changes: Int? = null,
)

class TransactionInstalmentBS(
    val company: Company,
    val onPay: (TransactionInstalmentRequest) -> Unit
) :
    BottomSheetDialogFragment() {
    private var totalPrice = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_BottomSheetDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaction_instalment_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = TransactionInstalmentAdapter { price ->
            Logger.d("price: $price")
            totalPrice = price
            tv_total_instalment?.text = "Total Pembayaran: ${price.toCurrency()}"
            validateButtonPay()
        }
        til_money_pay?.editText?.onTextChanged {
            validateButtonPay()
        }
        rv_instalment_transaction?.adapter = adapter
        company.unpaid_transaction?.let { adapter.setData(it) }
        btn_pay_instalment?.onClick {
            val req = TransactionInstalmentRequest().apply {
                unpaid_ids = adapter.getUnpaidIds()
                money_pay = til_money_pay?.value()?.toIntOrNull()
                money_changes = ((til_money_pay?.value()?.toIntOrNull() ?: 0) - (totalPrice
                    ?: 0)).coerceAtLeast(0)
            }
            val dialog = TransactionConfirmationDialogFragment(
                adapter.getUnpaidSelectedItems(),
                req.money_pay,
                req.money_changes
            ) {
                this.dismiss()
                this.onPay(req)
            }
            dialog.show(childFragmentManager, "")
        }
    }

    private fun validateButtonPay() {
        val changes =
            ((til_money_pay?.value()?.toIntOrNull() ?: 0) - (totalPrice ?: 0)).coerceAtLeast(0)
        tv_total_money_changes?.text = "Total kembalian ${changes.toCurrency()}"

        btn_pay_instalment?.isEnabled =
            (til_money_pay.value()?.toIntOrNull().isNotNoll()) && ((til_money_pay.value()
                ?.toIntOrNull() ?: 0) >= totalPrice)
    }
}