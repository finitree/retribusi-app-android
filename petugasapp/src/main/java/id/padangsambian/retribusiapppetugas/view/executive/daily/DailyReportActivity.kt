package id.padangsambian.retribusiapppetugas.view.executive.daily

import android.os.Bundle
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.model.DailyReportResponse
import id.padangsambian.retribusiapppetugas.presenter.report.ReportPresenter
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.padangsambian.shared.openDatePicker
import id.widianapw.android_utils.common.Const
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.onTextChanged
import id.widianapw.android_utils.extensions.toDateString
import id.widianapw.android_utils.extensions.visible
import kotlinx.android.synthetic.main.activity_daily_report.*

class DailyReportActivity : BaseActivity(), ReportPresenter.Delegate {
    private val reportPresenter = ReportPresenter(this)
    private val adapter = DailyReportAdapter()
    val DATE_FORMAT_API = "yyyy-MM-dd"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daily_report)
        setupToolbar("Laporan Harian") {
            onBackPressed()
        }
        initView()
    }

    private fun initView() {
        rv_daily_report?.adapter = adapter
        et_date_daily_report?.apply {
            onClick {
                this.openDatePicker(supportFragmentManager, isValidateBackward = true)
            }
            onTextChanged {
                tv_date_daily_report?.visible()
                tv_date_daily_report?.text = it
                loadData(it)
            }
        }

        sr_daily_report?.setOnRefreshListener {
            if (et_date_daily_report?.text.isNullOrEmpty()) {
                sr_daily_report?.isRefreshing = false
            } else {
                et_date_daily_report?.text?.toString()?.let { loadData(it) }
            }
        }
    }

    private fun loadData(date: String) {
        val formatted = date.toDateString(
            fromFormat = Const.READ_DATE_FORMAT,
            toFormat = DATE_FORMAT_API
        )
        doRequest {
            reportPresenter.getDailyReport(formatted)
        }
    }

    override fun getDailyReportResponse(res: DailyReportResponse) {
        super.getDailyReportResponse(res)
        sr_daily_report?.isRefreshing = false
        adapter.setData(res.data?.transactions)
    }

    override fun showError(title: String?, message: String?) {
        sr_daily_report?.isRefreshing = false
        super<BaseActivity>.showError(title, message)
    }

}