package id.padangsambian.retribusiapppetugas.view.owner

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.model.CompanyListResponse
import id.padangsambian.retribusiapppetugas.model.OwnerCompanyData
import id.padangsambian.retribusiapppetugas.model.OwnerCompanyItem
import id.padangsambian.retribusiapppetugas.model.PostBulkTransactionRequest
import id.padangsambian.retribusiapppetugas.presenter.company.CompanyPresenter
import id.padangsambian.retribusiapppetugas.presenter.transaction.TransactionPresenter
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.padangsambian.shared.Utils
import id.padangsambian.shared.input.BaseDialogFragment
import id.padangsambian.shared.model.SuccessResponse
import id.widianapw.android_utils.extensions.onTextChanged
import id.widianapw.android_utils.logger.WLog
import kotlinx.android.synthetic.main.activity_owner_company.*

class OwnerCompanyActivity : BaseActivity(), CompanyPresenter.Delegate,
    TransactionPresenter.Delegate {
    val transactionPresenter = TransactionPresenter(this)
    val companyPresenter = CompanyPresenter(this)
    var ownerCompanyList = mutableListOf<OwnerCompanyItem>()
    val adapter = OwnerCompanyAdapter(this, {
        showConfirmationDialog(
            title = "Pembayaran",
            description = "Apakah yakin ingin melakukan pembayaran ini?",
            listener = object : BaseDialogFragment.Listener {
                override fun primaryClick(dialog: Dialog) {
                    super.primaryClick(dialog)
                    WLog.obj(it)
                    val req = PostBulkTransactionRequest().apply {
                        companies =
                            it.company?.filter { ft -> ft.id != null }?.map { mp -> mp.id!! }
                    }
                    Utils.showProgress(this@OwnerCompanyActivity)
                    doRequest {
                        transactionPresenter.postBulkTransaction(req)
                    }
                }
            })
    }, onCallOwner = {
        it.owner?.phone?.let { it1 -> intentPhoneCall(it1) }
    })

    private fun intentPhoneCall(phone: String) {
        val intent = Intent(Intent.ACTION_DIAL).apply {
            data = Uri.parse("tel:$phone")
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_owner_company)
        setupToolbar("Pemilik Usaha") {
            onBackPressed()
        }
        initView()
        loadData()
    }

    private fun initView() {
        rv_owner_company?.adapter = adapter
        sr_owner_company?.setOnRefreshListener {
            loadData()
        }

        til_search_owner_company?.editText?.onTextChanged { value ->
            val list = if (value.isEmpty()) ownerCompanyList.toList() else ownerCompanyList.filter {
                it.owner?.name?.contains(value) == true
            }.toList()
            adapter.setData(list)
        }
    }

    private fun loadData() {
        doRequest {
            companyPresenter.getCompany()
        }
    }

    override fun getCompanyResponse(res: CompanyListResponse) {
        super.getCompanyResponse(res)
        sr_owner_company?.isRefreshing = false

        val companyList = res.data
        val ownerList = res.data?.map { it.user }?.distinctBy { it?.id }
        val ownerCompanyData = OwnerCompanyData()
        val list = mutableListOf<OwnerCompanyItem>()
        ownerList?.forEach {
            val ownerCompanyItem = OwnerCompanyItem()
            ownerCompanyItem.owner = it
            ownerCompanyItem.company = companyList?.filter { company -> company.user?.id == it?.id }
            list.add(ownerCompanyItem)
        }
        ownerCompanyData.data = list
        ownerCompanyList = list
        adapter.setData(ownerCompanyData.data)
    }

    override fun postBulkTransactionResponse(res: SuccessResponse) {
        super.postBulkTransactionResponse(res)
        Utils.hideProgress()
        showSuccessDialog(
            "Transaksi berhasil dilakukan",
            listener = object : BaseDialogFragment.Listener {})
        loadData()
    }

    override fun showError(title: String?, message: String?) {
        Utils.hideProgress()
        super<BaseActivity>.showError(title, message)
    }
}