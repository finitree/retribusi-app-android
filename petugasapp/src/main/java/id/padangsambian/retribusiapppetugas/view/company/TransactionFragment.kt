package id.padangsambian.retribusiapppetugas.view.company

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.model.TransactionResponse
import id.padangsambian.retribusiapppetugas.presenter.transaction.TransactionPresenter
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.widianapw.android_utils.extensions.setVisibility
import kotlinx.android.synthetic.main.fragment_transaction.*

private const val TYPE = "type"
private const val ID = "id"
private const val SUBSCRIPTION_TYPE = "subscription_type"

class TransactionFragment : Fragment(), TransactionPresenter.Delegate {
    private var type: String? = null
    private var subscriptionType: String? = null
    private var companyId: Int? = null
    private val presenter = TransactionPresenter(this)
    private var adapter: TransactionAdapter? = null
    private var date_from: String? = null
    private var date_to: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            type = it.getString(TYPE)
            companyId = it.getInt(ID)
            subscriptionType = it.getString(SUBSCRIPTION_TYPE)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaction, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = TransactionAdapter(type, subscriptionType)
        rv_transaction?.adapter = adapter
        sr_transaction?.setOnRefreshListener {
            fetchData(date_from, date_to)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(type: String, companyID: Int?, subscriptionType: String?) =
            TransactionFragment().apply {
                arguments = Bundle().apply {
                    putString(TYPE, type)
                    putInt(ID, companyID ?: 0)
                    putString(SUBSCRIPTION_TYPE, subscriptionType)
                }
            }
    }

    override fun onResume() {
        super.onResume()
        fetchData(date_from, date_to)
    }

    fun fetchData(date_from: String? = null, date_to: String? = null) {
        doRequest {
            type?.let {
                companyId?.let { it1 ->
                    presenter.getTransaction(
                        it,
                        it1,
                        date_from,
                        date_to
                    )
                }
            }
        }
    }

    override fun getTransactionResponse(response: TransactionResponse) {
        super.getTransactionResponse(response)
        sr_transaction?.isRefreshing = false
        val isEmpty = response.data.isNullOrEmpty()
        rv_transaction?.setVisibility(!isEmpty)
        tv_empty_transaction?.setVisibility(isEmpty)
        response.data?.let { adapter?.setData(it) }
    }

    override fun showError(title: String?, message: String?) {
        (activity as? BaseActivity?)?.showError(title, message)
    }


}