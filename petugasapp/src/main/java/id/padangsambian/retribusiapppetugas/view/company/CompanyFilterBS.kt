package id.padangsambian.retribusiapppetugas.view.company

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.padangsambian.retribusiapp.utilities.getPaymentTypes
import id.padangsambian.retribusiapp.utilities.setSelections
import id.padangsambian.retribusiapppetugas.R
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.validate
import id.widianapw.android_utils.validator.ValidatorType
import kotlinx.android.synthetic.main.fragment_company_filter_b_s.*

class CompanyFilterBS(val filteredType: String? = null, val onSave: (String?) -> Unit) :
    BottomSheetDialogFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.AppTheme_BottomSheetDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_company_filter_b_s, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (filteredType != null) {
            val item = getPaymentTypes().find { it.name?.lowercase() == filteredType.lowercase() }
            if (item != null) {
                select_payment_type_filter?.setItem(item)
            }
        }

        select_payment_type_filter?.setSelections(childFragmentManager, getPaymentTypes())
        btn_save_filter_company?.onClick {
            if (til_payment_type_filter?.validate(ValidatorType.Required) == true) {
                onSave(select_payment_type_filter?.selectItem?.name?.lowercase())
                dismiss()
            }
        }

        btn_back_filter_company?.onClick {
            dismiss()
        }

        btn_reset_filter_company?.onClick {
            onSave(null)
            dismiss()
        }
    }
}