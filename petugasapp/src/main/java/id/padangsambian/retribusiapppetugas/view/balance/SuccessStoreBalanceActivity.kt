package id.padangsambian.retribusiapppetugas.view.balance

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.padangsambian.retribusiapppetugas.MainActivity
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.widianapw.android_utils.extensions.goToActivity
import id.widianapw.android_utils.extensions.onClick
import kotlinx.android.synthetic.main.activity_success_store_balance.*

class SuccessStoreBalanceActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success_store_balance)

        btn_to_home?.onClick {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        finishAffinity()
        goToActivity(MainActivity::class.java)
    }
}