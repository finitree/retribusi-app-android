package id.padangsambian.retribusiapppetugas.view.company

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.padangsambian.retribusiapppetugas.R
import id.padangsambian.shared.model.UnpaidTransactionItem
import id.padangsambian.shared.toCurrency
import kotlinx.android.synthetic.main.layout_rv_unpaid_transaction.view.*

class TransactionDetailUnpaidAdapter :
    RecyclerView.Adapter<TransactionDetailUnpaidAdapter.ViewHolder>() {
    val list = mutableListOf<UnpaidTransactionItem>()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_rv_unpaid_transaction, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.itemView.apply {
            tv_unpaid_date?.text = item.unpaidAtFormatted
            tv_unpaid_amount?.text = item.amount?.toCurrency()
        }
    }

    override fun getItemCount(): Int = list.count()

    fun setData(data: List<UnpaidTransactionItem>?){
        this.list.apply {
            clear()
            data?.let { addAll(it) }
        }
        notifyDataSetChanged()
    }
}