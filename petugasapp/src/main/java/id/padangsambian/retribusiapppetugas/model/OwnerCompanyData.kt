package id.padangsambian.retribusiapppetugas.model

import id.padangsambian.shared.model.Company
import id.padangsambian.shared.model.user.User

data class OwnerCompanyData(
    var data: List<OwnerCompanyItem>? = null
)

data class OwnerCompanyItem(
    var owner: User? = null,
    var company: List<Company>? = null
)
