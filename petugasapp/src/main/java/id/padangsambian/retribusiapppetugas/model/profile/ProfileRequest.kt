package id.padangsambian.retribusiapppetugas.model.profile

/**
 * Created by Widiana Putra on 27/12/2021
 * Copyright (c) 2021 - Made With Love
 */
data class ProfileRequest(
    var phone: String? = null,
    var password: String? = null,
    var email: String? = null,
    var name: String? = null
)
