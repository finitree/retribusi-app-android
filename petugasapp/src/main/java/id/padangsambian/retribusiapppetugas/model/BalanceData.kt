package id.padangsambian.retribusiapppetugas.model

import id.widianapw.android_utils.common.Const
import id.widianapw.android_utils.extensions.toDateString

/**
 * Created by Widiana Putra on 29/12/2021
 * Copyright (c) 2021 - Made With Love
 */

class BalanceResponse {
    var data: List<BalanceItem>? = null
}

class BalanceItem {
    var id: Int? = null
    var type: String? = null
    var amount: Int? = null
    var notes: String? = null
    var created_at: String? = null

    fun getFormattedDate(): String? {
        return this.created_at?.toDateString(Const.READ_DATE_FORMAT, isConvertToLocal = false)
    }
}

class BalanceRequest {
    var setor_amount: Int? = null
    var staff_amount: Int? = null
    var notes: String? = null
}