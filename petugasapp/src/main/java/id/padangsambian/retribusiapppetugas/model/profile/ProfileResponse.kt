package id.padangsambian.retribusiapppetugas.model.profile

import id.padangsambian.shared.model.user.User
import io.realm.RealmObject

/**
 * Created by Widiana Putra on 27/12/2021
 * Copyright (c) 2021 - Made With Love
 */

class ProfileResponse {
    var data: User? = null
}