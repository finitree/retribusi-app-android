package id.padangsambian.retribusiapppetugas.model

import id.padangsambian.shared.model.Company
import id.padangsambian.shared.model.user.Staff

class DailyReportResponse {
    var data: DailyReportData? = null
}

class DailyReportData {
    var total_transaction: Int? = null
    var total_qty: Int? = null
    var transactions: List<DailyReportItem>? = null
}

class DailyReportItem {
    var total_amount: Int? = null
    var qty: Int? = null
    var amount: Int? = null
    var company: Company? = null
    var staff: Staff? = null
    var created_at: String? = null
}

class StaffReportResponse {
    var data: List<Staff>? = null
}