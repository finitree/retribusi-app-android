package id.padangsambian.retribusiapppetugas.model

import id.padangsambian.shared.model.Company

/**
 * Created by Widiana Putra on 27/12/2021
 * Copyright (c) 2021 - Made With Love
 */
class CompanyListResponse {
    var data: List<Company>? = null
}


class CompanyResponse {
    var data: Company? = null
}

class PostTransactionCompanyRequest {
    var company_id: Int? = null
}

class PostBulkTransactionRequest{
    var companies: List<Int>?=null
}
