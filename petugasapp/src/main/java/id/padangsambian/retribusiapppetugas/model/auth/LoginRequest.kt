package id.padangsambian.retribusiapppetugas.model.auth

data class LoginRequest(
    var phone: String? = null,
    var password: String? = null
)

data class ResetPasswordRequest(
    var email: String? = null
)
