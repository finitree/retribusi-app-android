package id.padangsambian.retribusiapppetugas

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapppetugas.model.CompanyListResponse
import id.padangsambian.retribusiapppetugas.model.profile.ProfileResponse
import id.padangsambian.retribusiapppetugas.presenter.company.CompanyPresenter
import id.padangsambian.retribusiapppetugas.presenter.profile.ProfilePresenter
import id.padangsambian.retribusiapppetugas.view.auth.LoginActivity
import id.padangsambian.retribusiapppetugas.view.balance.BalanceActivity
import id.padangsambian.retribusiapppetugas.view.balance.StoreBalanceActivity
import id.padangsambian.retribusiapppetugas.view.common.BaseActivity
import id.padangsambian.retribusiapppetugas.view.company.CompanyAdapter
import id.padangsambian.retribusiapppetugas.view.company.CompanyDetailActivity
import id.padangsambian.retribusiapppetugas.view.company.CompanyListActivity
import id.padangsambian.retribusiapppetugas.view.map.MapsActivity
import id.padangsambian.retribusiapppetugas.view.owner.OwnerCompanyActivity
import id.padangsambian.retribusiapppetugas.view.profile.ProfileActivity
import id.padangsambian.shared.Utils
import id.padangsambian.shared.input.BaseDialogFragment
import id.padangsambian.shared.model.SuccessResponse
import id.padangsambian.shared.toCurrency
import id.widianapw.android_utils.extensions.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_maps.*

class MainActivity : BaseActivity(), ProfilePresenter.Delegate, CompanyPresenter.Delegate {

    private val profilePresenter = ProfilePresenter(this)
    private val companyPresenter = CompanyPresenter(this)
    private var companyAdapter: CompanyAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
        loadData(true)
    }

    private fun initView() {
        companyAdapter = CompanyAdapter({
            with(Intent(this, CompanyDetailActivity::class.java)) {
                putExtra(CompanyDetailActivity.ID, it.id)
                startActivity(this)
            }
        }, {
            doRetributionPayment(it, {
                isLoading(true)
                doRequest {
                    companyPresenter.postTransaction(it)
                }
            }, {
                isLoading(true)
                doRequest {
                    companyPresenter.postInstalmentTransaction(it)
                }
            })
        })

        rv_company?.adapter = companyAdapter

        tv_store_balance?.onClick {
            goToActivity(StoreBalanceActivity::class.java)
        }

        tv_see_owner?.onClick {
            goToActivity(OwnerCompanyActivity::class.java)
        }

        btn_balance_main?.clickAndGoto(this, BalanceActivity::class.java)
        btn_see_all_company?.clickAndGoto(this, CompanyListActivity::class.java)
        tv_see_map?.onClick {
            with(Intent(this, MapsActivity::class.java)) {
                putExtra(MapsActivity.STATE, MapsActivity.State.LIST)
                startActivity(this)
            }
        }

        btn_logout?.onClick {
            showConfirmationDialog(
                title = "Apakah anda yakin ingin keluar dari akun ini?",
                listener = object : BaseDialogFragment.Listener {
                    override fun primaryClick(dialog: Dialog) {
                        super.primaryClick(dialog)
                        preference.isLoggedIn = false
                        finishAffinity()
                        goToActivity(LoginActivity::class.java)
                    }
                }
            )
        }

        btn_profile_home?.onClick {
            goToActivity(ProfileActivity::class.java)
        }

        sr_main?.setOnRefreshListener {
            doRequest {
                profilePresenter.getProfile()
                companyPresenter.getCompany()
            }
        }
    }

    private fun loadData(isFirst: Boolean = false) {
        isLoading(true, isFirst)
        doRequest {
            companyPresenter.getCompany()
        }
    }

    override fun onResume() {
        super.onResume()
        //load saldo only
        doRequest {
            companyPresenter.getCompany()
            profilePresenter.getProfile()
        }
    }

    private fun isLoading(izLoading: Boolean, isFirst: Boolean = false) {
        if (izLoading) {
            if (isFirst) loader_main?.visible()
            else Utils.showProgress(this)
        } else {
            loader_main?.animateGone()
            Utils.hideProgress()
            sr_main?.isRefreshing = false
        }
    }

    override fun getProfileResponse(res: ProfileResponse) {
        super.getProfileResponse(res)
        val balance = res.data?.staff?.balance?.toCurrency()
        tv_staff_balance?.text = balance
    }

    override fun getCompanyResponse(res: CompanyListResponse) {
        super.getCompanyResponse(res)
        isLoading(false)
        val list = res.data?.take(3)
        companyAdapter?.setData(list)
    }

    override fun postTransactionResponse(res: SuccessResponse) {
        super.postTransactionResponse(res)
        isLoading(false)
        layout_expand_map?.toggle()
        showSuccessDialog(
            "Transaksi berhasil dilakukan",
            listener = object : BaseDialogFragment.Listener {})
        doRequest {
            profilePresenter.getProfile()
            companyPresenter.getCompany()
        }
    }

    override fun showError(title: String?, message: String?) {
        isLoading(false)
        super<BaseActivity>.showError(title, message)
    }
}