package id.padangsambian.retribusiapppetugas.utilities

import android.app.Application
import android.content.Context
import com.cloudinary.android.MediaManager
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import io.realm.Realm
import io.realm.RealmConfiguration
import java.util.*

/**
 * Created by Widiana Putra on 01/12/21
 * Copyright (c) 2021 - Made with love
 */
class GlobalClass : Application() {
    lateinit var context: Context

    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        Realm.init(context)
        initRealm()

//        init cloudinary
        val config = HashMap<String, Any?>()
        config["cloud_name"] = "retribusi"
        config["secure"] = true
        MediaManager.init(this, config)

        //init logger
        Logger.addLogAdapter(AndroidLogAdapter())

    }

    private fun initRealm() {
        val realmConfiguration = RealmConfiguration.Builder()
            .schemaVersion(1)
            .deleteRealmIfMigrationNeeded()
            .build()
        Realm.setDefaultConfiguration(realmConfiguration)
    }

    companion object {
        private var instance: GlobalClass? = null
        fun applicationContext(): Context = instance!!.context
    }
}