package id.padangsambian.retribusiapp.utilities.constant

import android.Manifest

object Constants {
    const val baseUrl = "retribusi.finitree.com/api/staff"
}

object Permissions {
    val image = arrayOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    val locations = arrayOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
}