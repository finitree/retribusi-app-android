package id.padangsambian.retribusiapp.utilities

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by Widiana Putra on 01/12/21
 * Copyright (c) 2021 - Made with love
 */
class Preferences(context: Context) {
    private var sharedPreferences: SharedPreferences =
        context.getSharedPreferences("SharedPreferences", Context.MODE_PRIVATE)
    private var editor: SharedPreferences.Editor = sharedPreferences.edit()

    private var ACCESS_TOKEN = "access_token"
    private var IS_LOGGED_IN = "is_logged_in"

    var accessToken: String?
        get() = sharedPreferences.getString(ACCESS_TOKEN, "") ?: ""
        set(value) = editor.putString(ACCESS_TOKEN, value).apply()

    var isLoggedIn: Boolean
        get() = sharedPreferences.getBoolean(IS_LOGGED_IN, false)
        set(value) = editor.putBoolean(IS_LOGGED_IN, value).apply()
}