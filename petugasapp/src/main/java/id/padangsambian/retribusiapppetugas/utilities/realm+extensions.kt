package id.padangsambian.retribusiapp.utilities

import io.realm.ImportFlag
import io.realm.Realm
import io.realm.RealmObject
import io.realm.RealmResults

/**
 * Created by Widiana Putra on 01/12/21
 * Copyright (c) 2021 - Made with love
 */

fun <T> List<T>.replaceAllToRealm() {
    val realm = Realm.getDefaultInstance()
    if (!this.isNullOrEmpty()) {
        val item = this[0] as RealmObject
        realm.where(item.javaClass).findAll().deleteAllRealm()
    }

    for (item in this) {
        realm.beginTransaction()
        realm.copyToRealmOrUpdate(item as RealmObject)
        realm.commitTransaction()
    }
}

fun <T> List<T>.saveAllToRealm() {
    val realm = Realm.getDefaultInstance()
    for (item in this) {
        realm.beginTransaction()
        realm.copyToRealmOrUpdate(item as RealmObject)
        realm.commitTransaction()
    }
}

fun <T> RealmResults<T>.deleteAllRealm() {
    val realm = Realm.getDefaultInstance()
    realm.executeTransaction {
        this.deleteAllFromRealm()
    }
}

fun RealmObject.saveToRealm() {
    val realm = Realm.getDefaultInstance()
    realm.beginTransaction()
    realm.copyToRealmOrUpdate(this)
    realm.commitTransaction()
}

fun RealmObject.saveOnlySingleDataToRealm() {
    val realm = Realm.getDefaultInstance()
    realm.where(this.javaClass).findAll().deleteAllRealm()
    realm.beginTransaction()
    realm.copyToRealmOrUpdate(this)
    realm.commitTransaction()
}

fun RealmObject.saveOnlySingleDataToRealmWithoutPK() {
    val realm = Realm.getDefaultInstance()
    realm.where(this.javaClass).findAll().deleteAllRealm()
    realm.beginTransaction()
    realm.copyToRealm(this)
    realm.commitTransaction()
}

fun RealmObject.saveToRealmWithoutPrimary() {
    val realm = Realm.getDefaultInstance()
    realm.beginTransaction()
    realm.copyToRealm(this)
    realm.commitTransaction()
}

fun RealmObject.deleteRealm() {
    val realm = Realm.getDefaultInstance()
    realm.executeTransaction {
        this.deleteFromRealm()
    }
}

