package id.padangsambian.retribusiapppetugas.presenter.report

import id.padangsambian.retribusiapppetugas.model.DailyReportResponse
import id.padangsambian.retribusiapppetugas.model.StaffReportResponse
import id.padangsambian.retribusiapppetugas.presenter.base.BaseDelegate
import id.padangsambian.retribusiapppetugas.utilities.network.client
import id.padangsambian.retribusiapppetugas.utilities.network.requestApi
import io.ktor.client.request.*

class ReportPresenter(val delegate: Delegate) {
    interface Delegate : BaseDelegate {
        fun getDailyReportResponse(res: DailyReportResponse) {}
        fun getMonthReportResponse(res: DailyReportResponse) {}
        fun getStaffResponse(res: StaffReportResponse) {}
        fun getStaffDetailResponse(res: DailyReportResponse) {}
    }

    suspend fun getDailyReport(date: String? = null) {
        requestApi(delegate) {
            val res = client.get<DailyReportResponse>("report/daily") {
                if (date != null) {
                    parameter("date", date)
                }
            }
            delegate.getDailyReportResponse(res)
        }
    }

    suspend fun getMonthlyReport(month: String) {
        requestApi(delegate) {
            val res = client.get<DailyReportResponse>("report/monthly") {
                parameter("month", month)
            }
            delegate.getMonthReportResponse(res)
        }
    }

    suspend fun getStaffReport() {
        requestApi(delegate) {
            val res = client.get<StaffReportResponse>("report/staff")
            delegate.getStaffResponse(res)
        }
    }

    suspend fun getStaffDetailReport(id: Int, date: String? = null) {
        requestApi(delegate) {
            val res = client.get<DailyReportResponse>("report/staff/${id}") {
                if (!date.isNullOrEmpty()) {
                    parameter("date", date)
                }
            }
            delegate.getStaffDetailResponse(res)
        }

    }


}