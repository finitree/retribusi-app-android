package id.padangsambian.retribusiapppetugas.presenter.balance

import id.padangsambian.retribusiapppetugas.model.BalanceRequest
import id.padangsambian.retribusiapppetugas.model.BalanceResponse
import id.padangsambian.retribusiapppetugas.presenter.base.BaseDelegate
import id.padangsambian.retribusiapppetugas.utilities.network.client
import id.padangsambian.retribusiapppetugas.utilities.network.requestApi
import id.padangsambian.shared.model.SuccessResponse
import io.ktor.client.request.*

/**
 * Created by Widiana Putra on 29/12/2021
 * Copyright (c) 2021 - Made With Love
 */
class BalancePresenter(val delegate: Delegate) {
    interface Delegate : BaseDelegate {
        fun getBalanceResponse(res: BalanceResponse) {}
        fun storeBalanceResponse(res: SuccessResponse) {}
    }

    suspend fun getBalance() {
        requestApi(delegate) {
            val res = client.get<BalanceResponse>(path = "balance")
            delegate.getBalanceResponse(res)
        }
    }

    suspend fun storeBalance(req: BalanceRequest) {
        requestApi(delegate) {
            val res = client.post<SuccessResponse>(path = "balance") {
                body = req
            }
            delegate.storeBalanceResponse(res)
        }
    }
}