package id.padangsambian.retribusiapppetugas.presenter.profile

import id.padangsambian.retribusiapp.utilities.saveOnlySingleDataToRealm
import id.padangsambian.retribusiapp.utilities.saveOnlySingleDataToRealmWithoutPK
import id.padangsambian.retribusiapppetugas.model.profile.ProfileRequest
import id.padangsambian.retribusiapppetugas.model.profile.ProfileResponse
import id.padangsambian.retribusiapppetugas.presenter.base.BaseDelegate
import id.padangsambian.retribusiapppetugas.utilities.network.client
import id.padangsambian.retribusiapppetugas.utilities.network.requestApi
import io.ktor.client.request.*

/**
 * Created by Widiana Putra on 27/12/2021
 * Copyright (c) 2021 - Made With Love
 */
class ProfilePresenter(val delegate: Delegate) {
    interface Delegate: BaseDelegate{
        fun getProfileResponse(res: ProfileResponse){}
        fun updateProfileResponse(res: ProfileResponse){}
    }

    suspend fun getProfile(){
        requestApi(delegate){
            val res = client.get<ProfileResponse>(path = "profile")
            res.data?.saveOnlySingleDataToRealm()
            delegate.getProfileResponse(res)
        }
    }

    suspend fun updateProfile(req: ProfileRequest){
        requestApi(delegate){
            val res = client.patch<ProfileResponse>(path = "profile"){
                body = req
            }
            res.data?.saveOnlySingleDataToRealm()
            delegate.updateProfileResponse(res)
        }
    }


}