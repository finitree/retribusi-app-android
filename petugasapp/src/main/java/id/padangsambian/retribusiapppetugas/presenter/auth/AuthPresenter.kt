package id.padangsambian.retribusiapppetugas.presenter.auth

import id.padangsambian.retribusiapp.utilities.saveOnlySingleDataToRealm
import id.padangsambian.retribusiapppetugas.model.auth.LoginRequest
import id.padangsambian.retribusiapppetugas.model.auth.LoginResponse
import id.padangsambian.retribusiapppetugas.model.auth.ResetPasswordRequest
import id.padangsambian.retribusiapppetugas.presenter.base.BaseDelegate
import id.padangsambian.retribusiapppetugas.utilities.network.client
import id.padangsambian.retribusiapppetugas.utilities.network.requestApi
import id.padangsambian.shared.model.SuccessResponse
import io.ktor.client.request.*

class AuthPresenter(val delegate: Delegate) {
    interface Delegate : BaseDelegate {
        fun loginResponse(res: LoginResponse) {}
        fun sendResetPasswordResponse(res: SuccessResponse) {}
    }

    suspend fun login(request: LoginRequest) {
        requestApi(delegate) {
            val response = client.post<LoginResponse>(path = "login") {
                body = request
            }
            response.data?.user?.saveOnlySingleDataToRealm()
            delegate.loginResponse(response)
        }
    }

    suspend fun sendResetPassword(req: ResetPasswordRequest) {
        requestApi(delegate) {
            val response =
                client.post<SuccessResponse>(path = "password/send-email-forgot-password") {
                    body = req
                }
            delegate.sendResetPasswordResponse(response)
        }
    }
}