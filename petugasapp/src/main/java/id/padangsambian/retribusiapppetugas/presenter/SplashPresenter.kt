package id.padangsambian.retribusiapp.presenter

import InitialResponse
import id.padangsambian.retribusiapp.utilities.saveOnlySingleDataToRealm
import id.padangsambian.retribusiapppetugas.presenter.base.BaseDelegate
import id.padangsambian.retribusiapppetugas.utilities.network.client
import id.padangsambian.retribusiapppetugas.utilities.network.requestApi
import io.ktor.client.request.*

/**
 * Created by Widiana Putra on 06/12/21
 * Copyright (c) 2021 - Made with love
 */
class SplashPresenter(val delegate: Delegate) {
    interface Delegate : BaseDelegate {
        fun initialResponse(response: InitialResponse)
    }

    suspend fun getInitial() {
        requestApi(delegate) {
            val response = client.get<InitialResponse>(path = "initial")
            response.data?.let {
                it.apply {
                    setting?.saveOnlySingleDataToRealm()
                }
                delegate.initialResponse(response = response)
            }
        }
    }
}