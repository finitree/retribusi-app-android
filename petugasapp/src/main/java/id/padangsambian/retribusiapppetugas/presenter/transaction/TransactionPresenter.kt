package id.padangsambian.retribusiapppetugas.presenter.transaction

import id.padangsambian.retribusiapppetugas.model.PostBulkTransactionRequest
import id.padangsambian.retribusiapppetugas.model.TransactionResponse
import id.padangsambian.retribusiapppetugas.presenter.base.BaseDelegate
import id.padangsambian.retribusiapppetugas.utilities.network.client
import id.padangsambian.retribusiapppetugas.utilities.network.requestApi
import id.padangsambian.shared.model.SuccessResponse
import io.ktor.client.request.*

/**
 * Created by Widiana Putra on 30/12/2021
 * Copyright (c) 2021 - Made With Love
 */
class TransactionPresenter(val delegate: Delegate) {
    interface Delegate : BaseDelegate {
        fun getTransactionResponse(res: TransactionResponse) {}
        fun postBulkTransactionResponse(res: SuccessResponse) {}
    }

    suspend fun getTransaction(
        type: String,
        id: Int,
        date_from: String? = null,
        date_to: String? = null
    ) {
        requestApi(delegate) {
            val res = client.get<TransactionResponse>(path = "transaction") {
                parameter("type", type)
                parameter("company_id", id)
                if (date_from?.isNotEmpty() == true) {
                    parameter("date_from", date_from)
                }

                if (date_to?.isNotEmpty() == true) {
                    parameter("date_to", date_to)
                }

            }
            delegate.getTransactionResponse(res)
        }
    }

    suspend fun postBulkTransaction(
        req: PostBulkTransactionRequest
    ) {
        requestApi(delegate) {
            val res = client.post<SuccessResponse>(path = "transaction/bulk") {
                body = req
            }
            delegate.postBulkTransactionResponse(res)
        }
    }


}