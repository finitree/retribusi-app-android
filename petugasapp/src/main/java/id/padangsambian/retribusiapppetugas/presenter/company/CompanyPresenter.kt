package id.padangsambian.retribusiapppetugas.presenter.company

import id.padangsambian.retribusiapppetugas.model.CompanyListResponse
import id.padangsambian.retribusiapppetugas.model.CompanyResponse
import id.padangsambian.retribusiapppetugas.presenter.base.BaseDelegate
import id.padangsambian.retribusiapppetugas.utilities.network.client
import id.padangsambian.retribusiapppetugas.utilities.network.requestApi
import id.padangsambian.retribusiapppetugas.view.company.TransactionCompanyRequest
import id.padangsambian.retribusiapppetugas.view.company.TransactionInstalmentRequest
import id.padangsambian.shared.model.SuccessResponse
import io.ktor.client.request.*

/**
 * Created by Widiana Putra on 27/12/2021
 * Copyright (c) 2021 - Made With Love
 */
class CompanyPresenter(val delegate: Delegate) {
    interface Delegate : BaseDelegate {
        fun getCompanyResponse(res: CompanyListResponse) {}
        fun getCompanyDetailResponse(res: CompanyResponse) {}
        fun postTransactionResponse(res: SuccessResponse) {}
    }

    suspend fun getCompany(type: String? = null) {
        var path = "company"
        if (type != null) {
            path = "company?type=${type}"
        }
        requestApi(delegate) {
            val res = client.get<CompanyListResponse>(path = path)
            delegate.getCompanyResponse(res)
        }

    }

    suspend fun getCompanyDetail(id: Int) {
        requestApi(delegate) {
            val res = client.get<CompanyResponse>(path = "company/$id")
            delegate.getCompanyDetailResponse(res)
        }
    }

    suspend fun postTransaction(req: TransactionCompanyRequest) {
        requestApi(delegate) {

            val res = client.post<SuccessResponse>(path = "transaction") {
                body = req
            }
            delegate.postTransactionResponse(res)
        }
    }

    suspend fun postInstalmentTransaction(req: TransactionInstalmentRequest) {
        requestApi(delegate) {
            val res = client.post<SuccessResponse>("transaction/instalment") {
                body = req
            }
            delegate.postTransactionResponse(res)
        }
    }
}