package id.padangsambian.shared.model

import id.padangsambian.shared.model.user.User
import id.widianapw.android_utils.common.Const
import id.widianapw.android_utils.extensions.toDateString
import io.realm.RealmList
import io.realm.RealmObject

/**
 * Created by Widiana Putra on 07/12/21
 * Copyright (c) 2021 - Made with love
 */
open class Company : RealmObject() {
    var id: Int? = null
    var name: String? = null
    var address: String? = null
    var photos: String? = null
    var documents: String? = null
    var transaction_status: String? = null
    var user: User? = null
    var status: String? = null
    var subscription: Subscription? = null
    var subscription_amount: Int? = null
    var banjar: Banjar? = null

    //    var tempekan: Tempekan? = null
    var company_type: CompanyType? = null
    var next_transaction: NextTransaction? = null
    var latitude: Double? = null
    var longitude: Double? = null
    var company_close: CompanyClose? = null
    var company_operational_schedule: RealmList<CompanyOperationalItem>? = null
    var unpaid_transaction: RealmList<UnpaidTransactionItem>? = null
    fun getCompanyStatus(): Status {
        return when (this.status) {
            "verified" -> Status.Verified("Terverifikasi")
            "blocked" -> Status.Blocked("Terblokir")
            else -> Status.WaitingReview("Dalam Review")
        }
    }

    sealed class Status(val status: String) {
        class WaitingReview(status: String) : Status(status)
        class Verified(status: String) : Status(status)
        class Blocked(status: String) : Status(status)
    }

    sealed class PaymentStatus(val status: String) {
        class Paid(status: String) : PaymentStatus(status)
        class Unpaid(status: String) : PaymentStatus(status)
    }

    fun getPaymentStatus(): PaymentStatus {
        return when (this.transaction_status) {
            "paid" -> PaymentStatus.Paid("Telah Terbayar")
            else -> PaymentStatus.Unpaid("Belum Bayar")
        }
    }


}

open class UnpaidTransactionItem : RealmObject() {
    var id: Int? = null
    var amount: Int? = null
    var unpaid_at: String? = null
    val unpaidAtFormatted: String?
        get() = this.unpaid_at?.toDateString(Const.READ_DATE_FORMAT, isConvertToLocal = false)
}

open class NextTransaction : RealmObject() {
    var date: String? = null
    var subscription_qty: Int? = null
    var subscription_amount: Int? = null
}

open class CompanyClose : RealmObject() {
    var is_close: Boolean? = null
    var is_cuti: Boolean? = null
    var schedule: CompanyCloseItem? = null
    var current_schedule: CompanyOperationalItem? = null
}

open class CompanyCloseItem : RealmObject() {
    var id: Int? = null
    var company_id: Int? = null
    var date_start: String? = null
    var date_end: String? = null
    var description: String? = null
}

open class CompanyOperationalItem : RealmObject() {
    var id: Int? = null
    var day: Int? = null
    var opening_hours: String? = null
    var closing_hours: String? = null
    var is_open: Int? = null
    fun isOpenBool(): Boolean {
        return this.is_open == 1
    }
}
