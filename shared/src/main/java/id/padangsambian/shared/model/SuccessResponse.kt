package id.padangsambian.shared.model

/**
 * Created by Widiana Putra on 28/12/2021
 * Copyright (c) 2021 - Made With Love
 */
class SuccessResponse {
    var data: Any? = null
}