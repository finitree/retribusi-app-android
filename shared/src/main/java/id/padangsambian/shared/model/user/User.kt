package id.padangsambian.shared.model.user

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * Created by Widiana Putra on 02/12/21
 * Copyright (c) 2021 - Made with love
 */
@RealmClass
open class User : RealmObject() {
    @PrimaryKey
    var id: Int? = null
    var email: String? = null
    var name: String? = null
    var phone: String? = null
    var role: String? = null
    var staff: Staff? = null
    var photo: String? = null
    var ktp: String?=null
}
