package id.padangsambian.shared.model

import id.padangsambian.shared.input.SelectItem
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * Created by Widiana Putra on 06/12/21
 * Copyright (c) 2021 - Made with love
 */
@RealmClass
open class CompanyType : RealmObject() {
    @PrimaryKey
    var id: Int? = null

    var type: String? = null

    var is_document_required: Int? = null

    val isDocumentRequiredBool: Boolean
        get() = this.is_document_required == 1

    fun toSelectItem(): SelectItem {
        return SelectItem(this.id, this.type)
    }
}

@RealmClass
open class CompanyScale : RealmObject() {
    @PrimaryKey
    var id: Int? = null

    var scale: String? = null

    fun toSelectItem(): SelectItem {
        return SelectItem(this.id, this.scale)
    }
}

@RealmClass
open class Tempekan : RealmObject() {
    @PrimaryKey
    var id: Int? = null

    var name: String? = null

    var banjar: Banjar? = null

    fun toSelectItem(): SelectItem {
        return SelectItem(this.id, this.name)
    }
}

@RealmClass
open class Banjar : RealmObject() {
    @PrimaryKey
    var id: Int? = null

    var name: String? = null
    var address: String? = null

    fun toSelectItem(): SelectItem {
        return SelectItem(this.id, this.name)
    }
}

@RealmClass
open class SubscriptionType : RealmObject() {
    @PrimaryKey
    var id: Int? = null
    var category: String? = null

    fun toSelectItem(): SelectItem {
        return SelectItem(this.id, this.category)
    }
}
@RealmClass
open class SettingData : RealmObject() {
    @PrimaryKey
    var id: Int? = null

    var is_instalment_available: Int? = null

    val is_instalment_available_bool: Boolean
        get() = this.is_instalment_available == 1
}
