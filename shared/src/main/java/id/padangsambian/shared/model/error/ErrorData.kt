package id.padangsambian.shared.model.error


/**
 * Created by Widiana Putra on 10/11/21
 * Copyright (c)
 */
data class ErrorData(val error: ErrorItem)
data class ErrorItem(val code: Int, val title: String, val errors: List<ErrorList>)
data class ErrorList(val title: String, val message: String)
