package id.padangsambian.shared.model.user

import id.padangsambian.shared.model.Company
import io.realm.RealmList
import io.realm.RealmObject

open class Staff : RealmObject() {
    var id: Int? = null
    var user_id: Int? = null
    var balance: Int? = null
    var user: User? = null
    var companies: RealmList<Company>? = null
}