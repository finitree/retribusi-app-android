package id.padangsambian.shared.model

import io.realm.RealmObject

/**
 * Created by Widiana Putra on 07/12/21
 * Copyright (c) 2021 - Made with love
 */
open class Subscription : RealmObject(){
    var id: Int? = null
    var subscription_type: SubscriptionType? = null
    var company_type: CompanyType? = null
}