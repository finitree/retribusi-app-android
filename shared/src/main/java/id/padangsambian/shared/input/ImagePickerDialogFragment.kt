package id.padangsambian.shared.input

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.developers.imagezipper.ImageZipper
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import id.padangsambian.shared.R
import id.widianapw.android_utils.extensions.onClick
import kotlinx.android.synthetic.main.fragment_image_picker_dialog.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

/**
 * CREATED BY WIDIANA PUTRA 3/15/2021
 * PT. TIMEDOOR INDONESIA
 */
data class ImageRatioData(
    var x: Int,
    var y: Int
)

interface ImagePickerListener {
    fun onCropImage(dialog: BottomSheetDialogFragment, uri: Uri){
        dialog.dismiss()
    }
}

class ImagePickerDialogFragment(
    private val ratio: ImageRatioData,
    private val listener: ImagePickerListener
) : BottomSheetDialogFragment() {
    //region props
    var cameraUri: Uri? = null

    companion object{
        const val CAMERA_REQUEST_CODE = 123
        const val MAX_SINGLE_IMAGE_SIZE = 1000
    }


    //region lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_BottomSheetDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_image_picker_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        camera_picker?.onClick {
            openCamera()
        }

        btn_close_image_picker?.onClick {
            dismiss()
        }

        gallery_picker?.onClick {
            openGallery()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                CAMERA_REQUEST_CODE -> {
                    cameraUri?.let {
                        cropImage(it)
                    }
                }
                CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                    val result = CropImage.getActivityResult(data)
                    val uriImage = result.uri

                        val compressedFile = File(uriImage.path!!).compressSingleImage()
                        val uri = Uri.fromFile(compressedFile)
                        if (resultCode == Activity.RESULT_OK) {
                            listener.onCropImage(this, uri)
                        }

                }
            }

        }
    }

    //endregion

    //region local function
    private fun openCamera() {
        //take picture from camera and then save the file
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        //create file
        val file = getOutputMediaFile(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE)

        //get the uri from file
        cameraUri = Uri.fromFile(file)
        with(Intent(MediaStore.ACTION_IMAGE_CAPTURE)) {
            putExtra(MediaStore.EXTRA_OUTPUT, cameraUri)
            startActivityForResult(this, CAMERA_REQUEST_CODE)
        }
    }

    private fun cropImage(uri: Uri) {
        //crop image with camera uri (on open camera function)
        context?.let {
            CropImage.activity(uri).setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(ratio.x, ratio.y).start(it, this)
        }
    }

    private fun openGallery() {
        //open library with disable camera
        context?.let {
            CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).disableCamera()
                .setAspectRatio(ratio.x, ratio.y).start(
                    it, this
                )
        }
    }

    fun File.compressSingleImage(): File {
        var imageFile = this
        val quality = 20

        imageFile = ImageZipper(context)
            .setQuality(50)
            .compressToFile(imageFile)

        while (getImageSize(imageFile) >= MAX_SINGLE_IMAGE_SIZE) {
            try {
                imageFile = ImageZipper(context)
                    .setQuality(quality)
                    .compressToFile(imageFile)
            } catch (e: Exception) {
                break
            }
        }
        return imageFile
    }

    private fun getImageSize(image: File): Long {
        return image.length() / 1024
    }

    fun getOutputMediaFile(type: Int): File? {
        val mediaStorageDir = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),
            "Camera"
        )
        // Create the storage directory if it does not exist
        mediaStorageDir.apply {
            if (!exists()) {
                if (!mkdirs()) {
                    return null
                }
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        return when (type) {
            MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE -> {
                File("${mediaStorageDir.path}${File.separator}IMG_$timeStamp.jpg")
            }
            MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO -> {
                File("${mediaStorageDir.path}${File.separator}VID_$timeStamp.mp4")
            }
            else -> null
        }
    }
    //endregion
}