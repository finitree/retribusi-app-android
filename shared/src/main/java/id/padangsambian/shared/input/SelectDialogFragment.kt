package id.padangsambian.shared.input

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.divider.MaterialDividerItemDecoration
import id.padangsambian.shared.R
import id.widianapw.android_utils.extensions.onClick
import kotlinx.android.synthetic.main.layout_rv_select.view.*

data class SelectItem(
    var id: Int? = null,
    var name: String? = null,
    var isSelected: Boolean? = false
)

class SelectDialogFragment(
    val list: List<SelectItem>,
    val initialData: SelectItem? = null,
    val listener: Listener
) :
    DialogFragment() {

    interface Listener {
        fun save(dialog: Dialog, item: SelectItem?) {
            dialog.dismiss()
        }

        fun cancel(dialog: Dialog) {
            dialog.dismiss()
        }
    }

    val adapter = SelectAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_select_dialog, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = layoutInflater.inflate(R.layout.fragment_select_dialog, null)
        val saveBtn = view.findViewById<MaterialButton>(R.id.btn_save_select)
        val cancelBtn = view.findViewById<MaterialButton>(R.id.btn_cancel_select)
        val rv = view.findViewById<RecyclerView>(R.id.rv_select)
        val builder = context?.let {
            MaterialAlertDialogBuilder(it).run {
                setView(view)
                create()
            }
        }
        rv?.adapter = adapter

        list.find {
            it.id == initialData?.id
        }?.isSelected = true


        adapter.setData(list)
        context?.let { ctx ->
            val divider =
                MaterialDividerItemDecoration(
                    ctx,
                    LinearLayout.VERTICAL
                )
            rv?.addItemDecoration(divider)
        }
        builder?.setOnShowListener {
            saveBtn?.onClick {
                listener.save(builder, adapter.getSelectedItem())
            }
            cancelBtn?.onClick {
                listener.cancel(builder)
            }
        }
        return builder!!
    }
}

class SelectAdapter() :
    RecyclerView.Adapter<SelectAdapter.ViewHolder>() {

    init {
        hasStableIds()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    var list = mutableListOf<SelectItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.layout_rv_select, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.itemView.apply {
            tv_title_select?.text = item.name
            radio_select?.isChecked = item.isSelected == true
            onClick {
                if (radio_select?.isChecked != true) {
                    notifyCheckedChange(position)
                }
            }
        }
    }

    private fun notifyCheckedChange(position: Int) {
        list.forEachIndexed { index, selectItem ->
            val isSelected = index == position
            selectItem.isSelected = isSelected
        }

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    fun setData(list: List<SelectItem>) {
        this.list = list.toMutableList()
        notifyDataSetChanged()
    }

    fun getSelectedItem(): SelectItem? {
        return list.find { it.isSelected == true }
    }
}


