package id.padangsambian.shared.input

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import id.padangsambian.shared.R
import id.widianapw.android_utils.extensions.gone
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.setVisibility

class BaseDialogFragment(val type: Type?) : DialogFragment() {
    var isDisplayed = false;

    sealed class Type {
        object ServerError : Type()
        class NoInternet(var listener: Listener? = null) : Type()
        object TimedOut : Type()
        object OutOfMemory : Type()
        object WrongCredential : Type()
        class LocationPermission(val listener: Listener) : Type()
        class PrinterNotConnected(val listener: Listener) : Type()
        class SessionEnd() : Type()

        object BluetoothNotConnected : Type()

        class Alert(
            var image: Drawable? = null,
            var title: String? = null,
            var description: String? = null,
            val listener: Listener? = null
        ) : Type()

        class Confirmation(
            var image: Drawable? = null,
            var title: String? = null,
            var description: String? = null,
            var primaryButtonText: String? = null,
            var secondaryButtonText: String? = null,
            val listener: Listener? = null
        ) : Type()
    }

    interface Listener {
        fun primaryClick(dialog: Dialog) {
            dialog.dismiss()
        }

        fun secondaryClick(dialog: Dialog) {
            dialog.dismiss()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_base_dialog, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = layoutInflater.inflate(R.layout.fragment_base_dialog, null)
        val title = view.findViewById<TextView>(R.id.tv_title_base_dialog)
        val subtitle = view.findViewById<TextView>(R.id.tv_subtitle_base_dialog)
        val image = view.findViewById<ImageView>(R.id.iv_base_dialog)
        val primaryBtn = view.findViewById<MaterialButton>(R.id.btn_primary_base_dialog)
        val secondaryBtn = view.findViewById<MaterialButton>(R.id.btn_secondary_base_dialog)
        val builder = context?.let {
            MaterialAlertDialogBuilder(it).run {
                setView(view)
                create()

            }
        }

        builder?.let {
            when (type) {
                is Type.Alert -> {
                    if (type.image == null) image.gone()
                    else type.image?.let { image?.setImageDrawable(it) }

                    title?.apply {
                        text = type.title
                        setVisibility(type.title != null)
                    }
                    subtitle?.apply {
                        text = type.description
                        setVisibility(type.description != null)
                    }

                    secondaryBtn?.gone()
                    primaryBtn?.apply {
                        text = context.getString(R.string.tutup)
                        onClick {
                            if (type.listener != null) type.listener.primaryClick(builder)
                            else builder.dismiss()
                        }
                    }
                }

                is Type.Confirmation -> {
                    if (type.image == null) image.gone()
                    else type.image?.let { image?.setImageDrawable(it) }

                    title?.apply {
                        text = type.title
                        setVisibility(type.title != null)
                    }
                    subtitle?.apply {
                        text = type.description
                        setVisibility(type.description != null)
                    }

                    primaryBtn?.apply {
                        text = type.primaryButtonText ?: "Yakin"
                        onClick {
                            type.listener?.primaryClick(builder)
                        }
                    }
                    secondaryBtn?.apply {
                        text = type.secondaryButtonText ?: "Batal"
                        onClick {
                            type.listener?.secondaryClick(builder)
                        }
                    }
                }

                else -> {
                }
            }


        }
        return builder!!
    }

    override fun show(manager: FragmentManager, tag: String?) {
        try {
            isDisplayed = true
            super.show(manager, tag)
        } catch (e: Exception) {

        }

    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        isDisplayed = false
    }

    override fun dismiss() {
        super.dismiss()
        isDisplayed = false
    }
}