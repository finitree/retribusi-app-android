package id.padangsambian.shared.input

import android.content.Context
import android.text.InputType
import android.text.TextUtils
import android.util.AttributeSet
import com.google.android.material.textfield.TextInputEditText
import id.padangsambian.shared.R

/**
 * Created by Widiana Putra on 28/11/21
 * Copyright (c) 2021 - Made with love
 */
class Select(context: Context, attrs: AttributeSet?) :
    TextInputEditText(context, attrs) {
    var selectItem: SelectItem? = null

    init {
        isFocusableInTouchMode = false
        isFocusable = false
        isLongClickable = false
        inputType = InputType.TYPE_CLASS_TEXT
        maxLines = 1
        ellipsize = TextUtils.TruncateAt.END
        setCompoundDrawablesWithIntrinsicBounds(
            0,
            0,
            R.drawable.ic_baseline_keyboard_arrow_down_24,
            0
        )
    }

    fun setItem(item: SelectItem) {
        selectItem = item
        this.setText(selectItem?.name)
    }
}