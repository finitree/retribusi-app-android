package id.padangsambian.shared

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.Window
import android.widget.EditText
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointBackward
import com.google.android.material.datepicker.DateValidatorPointForward
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.textfield.TextInputLayout
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import id.widianapw.android_utils.common.Const
import id.widianapw.android_utils.extensions.toDate
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Widiana Putra on 02/12/21
 * Copyright (c) 2021 - Made with love
 */

object SubscriptionType {
    const val HARIAN = "harian"
    const val BULANAN = "bulanan"
}

object Utils {
    private lateinit var dialog: Dialog
    fun showProgress(context: Context) {
        dialog = Dialog(context)
        val view = LayoutInflater.from(context).inflate(R.layout.layout_progress_dialog, null)
        dialog.run {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawableResource(R.color.transparent)
//            setCancelable(false)
            setCanceledOnTouchOutside(false)
            setContentView(view)
            show()
        }
    }

    fun hideProgress() = if (this::dialog.isInitialized && dialog.isShowing) dialog.dismiss() else {
    }

    fun getCurrentDateTime(format: String?=null): String {
        val curDate = Calendar.getInstance().time
        val formatter =
            SimpleDateFormat(format ?: "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'", Locale.getDefault())
        return formatter.format(curDate)
    }


}

fun String?.abbreviateString(maxLength: Int): String {
    return if (this == null) {
        ""
    } else {
        return if (this.length <= maxLength)
            this
        else
            "${this.substring(0, maxLength - 2)}.."
    }
}

fun Float?.toIndonesianCurrency(): String? {
    return if (this == null) {
        null
    } else {
        val formatter = DecimalFormat("###,###,###")
        val value = this.toDouble()
        return "Rp. ${formatter.format(value)}"
    }
}

fun Number.toCurrency(): String {
    val formatter = DecimalFormat("###,###,###")
    val value = this.toDouble()
    return "Rp. ${formatter.format(value)}"
}

fun Int.getColor(context: Context): Int {
    return ContextCompat.getColor(context, this)
}

fun TextInputLayout?.setValue(value: String?) {
    this?.editText?.setText(value)
}

class Day {
    var id: Int? = null
    var name: String? = null
}

var days = mutableListOf<Day>(
    Day().apply {
        id = 1
        name = "Senin"
    },
    Day().apply {
        id = 2
        name = "Selasa"
    },
    Day().apply {
        id = 3
        name = "Rabu"
    },
    Day().apply {
        id = 4
        name = "Kamis"
    },
    Day().apply {
        id = 5
        name = "Jumat"
    },
    Day().apply {
        id = 6
        name = "Sabtu"
    },
    Day().apply {
        id = 7
        name = "Minggu"
    },
)

fun getCurrentDate(format: String? = null): String {
    val toFormat = format ?: Const.READ_DATE_FORMAT
    val sdf = SimpleDateFormat(toFormat)
    sdf.timeZone = TimeZone.getDefault()
    return sdf.format(Date())
}

private fun getTimeStringFormat(time: String): String {
    var temp = time
    if (time.length == 1) temp = "0$time"
    return temp
}

const val DATETIME_API_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"
fun String.toYesterdayDateInMillis(
    fromFormat: String? = null,
    isConvertToLocal: Boolean? = false
): Long {
    val isConvert = isConvertToLocal ?: true
    val format = fromFormat ?: DATETIME_API_FORMAT
    val dateFormat = SimpleDateFormat(format)
    if (isConvertToLocal == true) dateFormat.timeZone = TimeZone.getDefault()
    else dateFormat.timeZone = TimeZone.getTimeZone("UTC")
    val date = dateFormat.parse(this)
    return (date.time - 1000L * 60L * 60L * 24L)
}

fun String.toDateMillis(fromFormat: String? = null, isConvertToLocal: Boolean? = true): Long {
    val isConvert = isConvertToLocal ?: true
    val format = fromFormat ?: "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"
    val dateFormat = SimpleDateFormat(format)
    if (isConvert) dateFormat.timeZone = TimeZone.getDefault() else dateFormat.timeZone =
        TimeZone.getTimeZone("UTC")
    val date = dateFormat.parse(this)
    return date.time
}


fun EditText?.openDatePicker(
    fm: FragmentManager,
    isValidateBackward: Boolean = false,
    isValidateForward: Boolean = false,
    validateFrom: String? = null
) {
    val validator = when {
        isValidateForward -> CalendarConstraints.Builder().setValidator(
            DateValidatorPointForward.now()
        )
        isValidateBackward -> CalendarConstraints.Builder()
            .setValidator(DateValidatorPointBackward.now())
        validateFrom != null ->
            CalendarConstraints.Builder()
                .setValidator(DateValidatorPointForward.from(validateFrom.toYesterdayDateInMillis()))
        else -> null
    }
    val selectedDateMillis =
        if (this?.text.toString().isEmpty()) Calendar.getInstance().timeInMillis
        else {
            this?.text.toString().toDateMillis(Const.READ_DATE_FORMAT, isConvertToLocal = false)
        }

    val datePicker =
        MaterialDatePicker.Builder.datePicker()
            .setCalendarConstraints(validator?.build())
            .setTitleText("Pilih Tanggal")
            .setSelection(
                selectedDateMillis
            )
            .build()
    datePicker.addOnPositiveButtonClickListener {
        val formattedDate = it.toDate(Const.READ_DATE_FORMAT)
        this?.setText(formattedDate)
    }
    datePicker.show(fm, "")
}

fun EditText?.openTimePicker(fm: FragmentManager) {
    //variable to set selected time on time picker dialog
    val initialValue = this?.text.toString()
    val selectedTime = if (initialValue.isEmpty()) "00:00" else initialValue
    val hour = selectedTime.split(":")[0].toIntOrNull()
    val minute = selectedTime.split(":")[1].toIntOrNull()
    //time picker dialog builder
    val timePicker =
        MaterialTimePicker.Builder()
            .setInputMode(MaterialTimePicker.INPUT_MODE_KEYBOARD)
            .setTimeFormat(
                TimeFormat.CLOCK_24H
            ).setHour(hour ?: 0).setMinute(minute ?: 0).build()
    timePicker.addOnPositiveButtonClickListener {
        val value = "${getTimeStringFormat(timePicker.hour.toString())}:${
            getTimeStringFormat(timePicker.minute.toString())
        }"
        this?.setText(value)
    }
    timePicker.show(fm, "")
}
