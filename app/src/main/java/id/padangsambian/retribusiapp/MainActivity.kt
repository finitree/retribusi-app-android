package id.padangsambian.retribusiapp

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import id.padangsambian.retribusiapp.model.response.CompanyResponse
import id.padangsambian.retribusiapp.model.response.TransactionResponse
import id.padangsambian.retribusiapp.model.response.TransactionType
import id.padangsambian.retribusiapp.presenter.Auth
import id.padangsambian.retribusiapp.presenter.CompanyPresenter
import id.padangsambian.retribusiapp.presenter.TransactionPresenter
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapp.view.auth.LoginActivity
import id.padangsambian.retribusiapp.view.common.BaseActivity
import id.padangsambian.retribusiapp.view.company.CompanyAdapter
import id.padangsambian.retribusiapp.view.company.CompanyAddActivity
import id.padangsambian.retribusiapp.view.company.CompanyDetailActivity
import id.padangsambian.retribusiapp.view.profile.ProfileActivity
import id.padangsambian.retribusiapp.view.transaction.TransactionActivity
import id.padangsambian.retribusiapp.view.transaction.TransactionAdapter
import id.padangsambian.shared.input.BaseDialogFragment
import id.widianapw.android_utils.extensions.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), CompanyPresenter.Delegate, TransactionPresenter.Delegate {
    companion object {
        const val IS_FROM_HOME = "isFromHome"
    }

    var isFromHome: Boolean? = null
    private val companyPresenter = CompanyPresenter(this)
    private var companyAdapter: CompanyAdapter? = null
    private val transactionPresenter = TransactionPresenter(this)
    private var transactionAdapter: TransactionAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        isFromHome = intent.getBooleanExtra(IS_FROM_HOME, false)
        if (isFromHome == true) {
            showAlertDialog(
                R.drawable.ic_success.getDrawable(this),
                "Pendaftaran Berhasil",
                "Selamat akun Anda berhasil dibuat. Tunggu admin untuk melakukan verifikasi usaha Anda.",
            )
        }

        initView()
        fetchData(true)
    }

    private fun fetchData(isFirst: Boolean = false) {
        isLoading(true, isFirst)
        tv_name_main?.text = Auth.user()?.name
        doRequest {
            companyPresenter.getCompanies()
            transactionPresenter.getTransaction(TransactionType.PAID)
        }
    }

    private fun isLoading(izLoading: Boolean, isFirst: Boolean = false) {
        if (izLoading) {
            if (isFirst) loader_main?.visible()
        } else {
            sr_main?.isRefreshing = false
            loader_main?.animateGone()
        }
    }

    private fun initView() {
        tv_name_main?.text = Auth.user()?.name

        transactionAdapter = TransactionAdapter(TransactionType.PAID)
        rv_transaction_main?.adapter = transactionAdapter

        companyAdapter = CompanyAdapter {
            val intent = Intent(this, CompanyDetailActivity::class.java)
            intent.run {
                putExtra(CompanyDetailActivity.ID, it.id)
                putExtra(CompanyDetailActivity.STATE, CompanyDetailActivity.State.READ)
            }
            startActivity(intent)
        }
        rv_company_main?.adapter = companyAdapter

        sr_main?.setOnRefreshListener {
            fetchData()
        }

        btn_profile_home?.onClick {
//            goToActivity(MapsActivity::class.java)
            goToActivity(ProfileActivity::class.java)
        }

        btn_logout?.onClick {
            showConfirmationDialog(
                title = "Apakah anda yakin ingin keluar dari akun ini?",
                listener = object : BaseDialogFragment.Listener {
                    override fun primaryClick(dialog: Dialog) {
                        super.primaryClick(dialog)
                        preferences.isLoggedIn = false
                        finishAffinity()
                        goToActivity(LoginActivity::class.java)
                    }
                }
            )
        }

        btn_add_company?.onClick {
            goToActivity(CompanyAddActivity::class.java)
        }

        btn_view_transaction?.onClick {
            goToActivity(TransactionActivity::class.java)
        }
    }

    override fun getCompaniesResponse(response: CompanyResponse) {
        isLoading(false)
        super.getCompaniesResponse(response)
        response.data?.let { companyAdapter?.setData(it) }
    }

    override fun getTransactionResponse(response: TransactionResponse) {
        super.getTransactionResponse(response)
        val isEmptyList = response.data?.isNullOrEmpty() == true
        tv_empty_transaction_main?.setVisibility(isEmptyList)
        rv_transaction_main?.setVisibility(!isEmptyList)
        response.data?.take(2)?.let { transactionAdapter?.setData(it) }

    }

    override fun showError(title: String?, message: String?) {
        super<BaseActivity>.showError(title, message)
        isLoading(false)
    }
}