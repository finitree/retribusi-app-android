package id.padangsambian.retribusiapp.view.company

import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.padangsambian.retribusiapp.R
import id.padangsambian.retribusiapp.model.response.CompanyCloseResponse
import id.padangsambian.retribusiapp.presenter.CompanyPresenter
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapp.view.common.BaseActivity
import id.padangsambian.shared.Utils
import id.padangsambian.shared.input.BaseDialogFragment
import id.padangsambian.shared.model.CompanyCloseItem
import id.padangsambian.shared.model.SuccessResponse
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.setVisibility
import kotlinx.android.synthetic.main.activity_company_close.*

class CompanyCloseActivity : BaseActivity(), CompanyPresenter.Delegate {
    companion object {
        const val COMPANY_ID = "company_id"
    }

    private val companyPresenter = CompanyPresenter(this)
    private var companyId: Int? = null
    private var adapter: CompanyCloseAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        companyId = intent.getIntExtra(COMPANY_ID, 0)
        setContentView(R.layout.activity_company_close)
        setupToolbar("Jadwal Libur") {
            onBackPressed()
        }

        cv_add_cuti?.onClick {
            openCompanyCloseBS()
        }
        adapter = CompanyCloseAdapter({
            openCompanyCloseBS(it)
        }, {
            showConfirmationDialog(
                title = "Hapus Libur",
                description = "Apakah anda yakin ingin menghapus libur ini?",
                listener = object : BaseDialogFragment.Listener {
                    override fun primaryClick(dialog: Dialog) {
                        super.primaryClick(dialog)
                        isLoading(true)
                        doRequest {
                            it.id?.let { it1 ->
                                companyPresenter.deleteCompanyClose(
                                    companyId!!,
                                    it1
                                )
                            }
                        }
                    }
                })
        })
        rv_company_close?.adapter = adapter

        sr_company_close?.setOnRefreshListener {
            fetchData()
        }
        fetchData()
    }

    override fun deleteCompanyCloseResponse(response: SuccessResponse) {
        super.deleteCompanyCloseResponse(response)
        isLoading(false)
        fetchData()
    }

    fun isLoading(izLoading: Boolean) {
        if (izLoading) {
            Utils.showProgress(this)
        } else {
            Utils.hideProgress()
        }
    }

    private fun openCompanyCloseBS(item: CompanyCloseItem? = null) {
        val bs = companyId?.let {
            CompanyCutiBS(it, item) {
                fetchData()
            }
        }
        bs?.show(supportFragmentManager, "")
    }

    private fun fetchData() {
        doRequest {
            companyId?.let { companyPresenter.getCompanyClose(it) }
        }
    }

    override fun getCompanyCloseResponse(response: CompanyCloseResponse) {
        super.getCompanyCloseResponse(response)
        sr_company_close?.isRefreshing = false
        tv_empty_company_close?.setVisibility(response.data?.count() == 0)
        response.data?.let { adapter?.setData(it) }
    }
}