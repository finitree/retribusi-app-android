package id.padangsambian.retribusiapp.view.maps

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import id.padangsambian.retribusiapp.R
import id.padangsambian.retribusiapp.utilities.constant.Permissions
import id.padangsambian.retribusiapp.view.common.BaseActivity
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.setVisibility
import kotlinx.android.synthetic.main.activity_maps.*

class MapsActivity() : BaseActivity(), OnMapReadyCallback {
    enum class State {
        EDIT, READ
    }

    companion object {
        const val PICKER_RESULT = 999
        const val INIT_LAT = "init_lat"
        const val INIT_LNG = "init_lng"
        const val STATE = "state"
    }

    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    var currentMarker: Marker? = null
    var currentAddress: String? = null

    private var initLat: Double? = null
    private var initLng: Double? = null
    private var state: State = State.EDIT
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        //get intent
        intent?.apply {
            initLat = getDoubleExtra(INIT_LAT, 0.0)
            initLng = getDoubleExtra(INIT_LNG, 0.0)
            state = try {
                getSerializableExtra(STATE) as State
            } catch (e: Exception) {
                State.EDIT
            }
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        tv_title_map?.setVisibility(state == State.EDIT)
        initListener()

    }

    private fun initListener() {
        btn_back_map?.onClick {
            finish()
        }

        btn_choose_map?.setVisibility(state == State.EDIT)
        btn_choose_map?.onClick {
            val returnedIntent = Intent().apply {
                putExtra("latitude", currentMarker?.position?.latitude)
                putExtra("longitude", currentMarker?.position?.longitude)
                putExtra("address", tv_address_map?.text?.toString())
            }
            setResult(Activity.RESULT_OK, returnedIntent)
            finish()
        }
    }


    @SuppressLint("MissingPermission")
    private fun getDeviceLocation(isInit: Boolean = false) {
        if (checkPermission(Permissions.locations)) {
            // Construct a FusedLocationProviderClient.
            mMap.isMyLocationEnabled = true
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
            fusedLocationProviderClient.lastLocation.addOnSuccessListener { currentLocation ->
                if (currentLocation != null) {
                    val position = if (currentLocation != null) LatLng(
                        currentLocation.latitude,
                        currentLocation.longitude
                    ) else LatLng(-8.65442381368823, 115.186497735764)
                    LatLng(currentLocation.latitude, currentLocation.longitude)
                    if (isInit) {
                        if (initLat == 0.0 || initLat == null)
                            setMarkerPos(position)
                        else {
                            val pos =
                                initLat?.let { it1 -> initLng?.let { it2 -> LatLng(it1, it2) } }
                            if (pos != null) {
                                setMarkerPos(pos)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun setMarkerPos(position: LatLng) {
        if (currentMarker != null) currentMarker?.remove()

        val isDragable = state == State.EDIT
        currentMarker = mMap.addMarker(MarkerOptions().position(position).draggable(isDragable))
        setAddress(position)
    }

    private fun setAddress(position: LatLng) {
        try {
            moveCamera(position)
            val geocoder = Geocoder(this)
            val addresses = geocoder.getFromLocation(position.latitude, position.longitude, 1)
            val address = addresses?.get(0)?.getAddressLine(0)
            tv_address_map?.text = address
        } catch (e: Exception) {
        }
    }

    private fun moveCamera(latLng: LatLng, zoom: Float = 18f) {
        mMap.animateCamera(zoom.let { CameraUpdateFactory.newLatLngZoom(latLng, it) })
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        getDeviceLocation(true)
        mMap.setOnMyLocationButtonClickListener {
            getDeviceLocation()
            true
        }

        mMap.setOnMarkerDragListener(
            object : GoogleMap.OnMarkerDragListener {
                override fun onMarkerDrag(p0: Marker) {}
                override fun onMarkerDragStart(p0: Marker) {}
                override fun onMarkerDragEnd(p0: Marker) {
                    setMarkerPos(p0.position)
                }
            }
        )
    }

}