package id.padangsambian.retribusiapp.view.company

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.padangsambian.retribusiapp.R
import id.padangsambian.shared.days
import id.padangsambian.shared.model.CompanyOperationalItem
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.setVisibility
import kotlinx.android.synthetic.main.layout_rv_company_schedule_edit.view.*

class CompanyOperationalAdapter(val onEdit: (CompanyOperationalItem) -> Unit) :
    RecyclerView.Adapter<CompanyOperationalAdapter.ViewHolder>() {
    private val list = mutableListOf<CompanyOperationalItem>()
    private var isEdit = false

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_rv_company_schedule_edit, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.itemView.apply {
            btn_rv_edit_company_schedule?.setVisibility(isEdit)
            val dayName = days.find { it.id == item.day }?.name
            tv_rv_company_schedule?.text =
                "${dayName}, ${item.opening_hours} - ${item.closing_hours}"
            chip_rv_company_is_open?.text = if (item.isOpenBool()) "Buka" else "Tutup"
            if (item.isOpenBool())
                chip_rv_company_is_open?.setChipBackgroundColorResource(R.color.color_lightest_primary)
            else
                chip_rv_company_is_open?.setChipBackgroundColorResource(R.color.color_light_error)

            btn_rv_edit_company_schedule?.onClick {
                onEdit(item)
            }
        }
    }

    override fun getItemCount(): Int = list.count()

    fun setData(data: List<CompanyOperationalItem>) {
        this.list.apply {
            clear()
            addAll(data)
        }
        notifyDataSetChanged()
    }

    fun setEditMode(izEdit: Boolean) {
        isEdit = izEdit
        notifyDataSetChanged()
    }

    fun updateItem(operational: CompanyOperationalItem) {
        val days = this.list.map { it.day }
        val index = days.indexOf(operational.day)
        this.list[index].apply {
            is_open = operational.is_open
            opening_hours = operational.opening_hours
            closing_hours = operational.closing_hours
        }
        notifyItemChanged(index)
    }

    fun getData(): List<CompanyOperationalItem> {
        return this.list
    }
}