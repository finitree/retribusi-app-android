package id.padangsambian.retribusiapp.view.transaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import id.padangsambian.retribusiapp.R
import id.padangsambian.retribusiapp.model.response.TransactionResponse
import id.padangsambian.retribusiapp.presenter.TransactionPresenter
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapp.view.common.BaseActivity
import id.widianapw.android_utils.extensions.setVisibility
import kotlinx.android.synthetic.main.fragment_transaction.*

private const val TYPE = "type"
private const val COMPANY_ID = "company_id"
private const val SUBSCRIPTION_TYPE = "subscription_type"

class TransactionFragment : Fragment(), TransactionPresenter.Delegate {
    private var type: String? = null
    private var companyId: Int? = null
    private var subscriptionType: String? = null
    private val presenter = TransactionPresenter(this)
    private var adapter: TransactionAdapter? = null
    private var date_from: String? = null
    private var date_to: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            type = it.getString(TYPE)
            companyId = it.getInt(COMPANY_ID, 0)
            subscriptionType = it.getString(SUBSCRIPTION_TYPE)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaction, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = TransactionAdapter(type, subscriptionType)
        rv_transaction?.adapter = adapter
        sr_transaction?.setOnRefreshListener {
            fetchData(date_from, date_to)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(type: String, companyId: Int? = null, subscriptionType: String? = null) =
            TransactionFragment().apply {
                arguments = Bundle().apply {
                    putString(TYPE, type)
                    putInt(COMPANY_ID, companyId ?: 0)
                    putString(SUBSCRIPTION_TYPE, subscriptionType)
                }
            }
    }

    override fun onResume() {
        super.onResume()
        fetchData(date_from, date_to)
    }

    fun fetchData(date_from: String? = null, date_to: String? = null) {
        this.date_from = date_from
        this.date_to = date_to
        doRequest {
            type?.let { presenter.getTransaction(it, companyId, date_from, date_to) }
        }
    }

    override fun getTransactionResponse(response: TransactionResponse) {
        super.getTransactionResponse(response)
        sr_transaction?.isRefreshing = false
        val isEmpty = response.data.isNullOrEmpty()
        rv_transaction?.setVisibility(!isEmpty)
        tv_empty_transaction?.setVisibility(isEmpty)
        response.data?.let { adapter?.setData(it) }
    }

    override fun showError(title: String?, message: String?) {
        (activity as? BaseActivity?)?.showError(title, message)
    }
}