package id.padangsambian.retribusiapp.view.splash

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import id.padangsambian.retribusiapp.MainActivity
import id.padangsambian.retribusiapp.R
import id.padangsambian.retribusiapp.model.response.InitialResponse
import id.padangsambian.retribusiapp.presenter.SplashPresenter
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapp.view.auth.LoginActivity
import id.padangsambian.retribusiapp.view.common.BaseActivity
import id.widianapw.android_utils.extensions.goToActivity

@SuppressLint("CustomSplashScreen")
class SplashActivity : BaseActivity(), SplashPresenter.Delegate {
    val presenter = SplashPresenter(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        doRequest {
            presenter.getInitial()
        }
    }

    override fun initialResponse(response: InitialResponse) {
        Handler().postDelayed({
            finish()
            if (preferences.isLoggedIn) goToActivity(MainActivity::class.java)
            else goToActivity(LoginActivity::class.java)
        }, 2000)
    }
}