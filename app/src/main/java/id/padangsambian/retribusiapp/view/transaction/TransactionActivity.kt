package id.padangsambian.retribusiapp.view.transaction

import android.os.Bundle
import id.padangsambian.retribusiapp.R
import id.padangsambian.retribusiapp.view.common.BaseActivity
import id.padangsambian.shared.getColor
import id.widianapw.android_utils.extensions.isNotNoll
import kotlinx.android.synthetic.main.activity_transaction.*

class TransactionActivity : BaseActivity() {
    companion object {
        const val COMPANY_ID = "company_id"
        const val SUBSCRIPTION_TYPE = "subscription_type"
    }

    private var initialFilter: FilterReq? = null

    private var pagerAdapter: TransactionPagerAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction)
        val companyID = intent.getIntExtra(COMPANY_ID, 0)
        tl_transaction?.setNavigationOnClickListener {
            onBackPressed()
        }
        tl_transaction?.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.menu_filter -> {
                    openBSFilterTransaction()
                    true
                }
                else -> false
            }
        }
        val companyToSend = if (companyID.isNotNoll()) companyID else null
        val subscriptionType = intent.getStringExtra(SUBSCRIPTION_TYPE)
        pagerAdapter =
            TransactionPagerAdapter(supportFragmentManager, companyToSend, subscriptionType)
        vp_transaction?.adapter = pagerAdapter
        tab_transaction?.setupWithViewPager(vp_transaction)
    }

    private fun openBSFilterTransaction() {
        val bs = TransactionFilterBSFragment(initialFilter,{
            tl_transaction?.menu?.findItem(R.id.menu_filter)?.icon?.setTint(
                R.color.colorPrimary.getColor(
                    this
                )
            )
            initialFilter = it
            pagerAdapter?.fetchFilterTransaction(it, vp_transaction.currentItem)
        }, {
            tl_transaction?.menu?.findItem(R.id.menu_filter)?.icon?.setTint(
                R.color.black.getColor(
                    this
                )
            )
            initialFilter = null
            pagerAdapter?.fetchFilterTransaction(null, vp_transaction.currentItem)
        })
        bs.show(supportFragmentManager, "")
    }
}