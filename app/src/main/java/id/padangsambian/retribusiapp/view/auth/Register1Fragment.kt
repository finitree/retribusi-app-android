package id.padangsambian.retribusiapp.view.auth

import android.app.Dialog
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import id.padangsambian.retribusiapp.R
import id.padangsambian.retribusiapp.view.common.BaseActivity
import id.padangsambian.shared.input.BaseDialogFragment
import id.padangsambian.shared.input.ImageRatioData
import id.widianapw.android_utils.extensions.*
import id.widianapw.android_utils.validator.ValidatorType
import kotlinx.android.synthetic.main.fragment_register1.*

class Register1Fragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register1, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListener()
        setupInitialView()
    }

    private fun setupInitialView() {
        (activity as? RegisterActivity?)?.registerRequest?.run {
            til_name_register?.editText?.setText(name)
            til_email_register?.editText?.setText(email)
            til_phone_register?.editText?.setText(phone)
            til_password_register?.editText?.setText(password)
            til_password_confirmation_register?.editText?.setText(password_confirmation)
            photo?.let {
                iv_register_1?.loadImage(it)
            }
        }
    }

    private fun initListener() {
        til_password_confirmation_register?.editText?.onTextChanged {
            til_password_confirmation_register?.apply {
                error = null
                isErrorEnabled = false
            }
        }

        btn_register_1?.onClick {
            if (isAllValid()) {
                (activity as? RegisterActivity?)?.apply {
                    registerRequest.apply {
                        name = til_name_register?.value()
                        email = til_email_register?.value()
                        phone = til_phone_register?.value()
                        password = til_password_register?.value()
                        password_confirmation = til_password_confirmation_register?.value()

                    }
                }
                (activity as? RegisterActivity?)?.nextPage()
            }
        }

        btn_add_iv_register?.onClick {
            (activity as? BaseActivity?)?.openImagePicker(ImageRatioData(1, 1)) {
                pb_image_register1?.visible()
                (activity as? BaseActivity?)?.uploadCloudinary(
                    it.path!!,
                    object : BaseActivity.CloudinaryListener {
                        override fun onSuccess(url: String) {
                            super.onSuccess(url)
                            pb_image_register1?.gone()
                            iv_register_1?.loadImage(url)
                            (activity as? RegisterActivity?)?.apply {
                                registerRequest.photo = url
                            }
                        }
                    })
            }
        }

        btn_add_image_ktp?.onClick {
            openImageRes()
        }

        btn_edit_image_ktp?.onClick {
            openImageRes()
        }

        btn_delete_image_ktp?.onClick {
            (activity as? BaseActivity?)?.showConfirmationDialog(
                image = context?.let { it1 -> R.drawable.ic_alert.getDrawable(it1) },
                title = "Apakah anda yakin ingin meghapus foto ini?",
                listener = object : BaseDialogFragment.Listener {
                    override fun primaryClick(dialog: Dialog) {
                        super.primaryClick(dialog)
                        (activity as? RegisterActivity?)?.registerRequest?.ktp = null
                        selected_section_image?.gone()
                        unselected_section_image?.visible()
                    }
                }
            )
        }

    }

    private fun openImageRes() {
        (activity as? BaseActivity?)?.openImagePicker(
            ImageRatioData(2, 1)
        ) {
            pb_image_upload?.visible()
            uploadImage(it)
        }
    }

    private fun uploadImage(it: Uri) {
        it.path?.let { path ->
            (activity as? BaseActivity?)?.uploadCloudinary(
                path,
                object : BaseActivity.CloudinaryListener {
                    override fun onSuccess(url: String) {
                        super.onSuccess(url)
                        selected_section_image?.visible()
                        unselected_section_image?.gone()
                        iv_add_ktp?.loadImage(url)
                        (activity as? RegisterActivity?)?.registerRequest?.ktp = url
                        pb_image_upload?.gone()
                    }

                    override fun onError() {
                        super.onError()
                        pb_image_upload?.gone()
                    }

                })
        }

    }


    private fun isAllValid(): Boolean {
        val isValid = mutableListOf<Boolean>()
        isValid.add(
            til_name_register?.validate(
                ValidatorType.Required
            ) == true
        )

        if (til_email_register?.value()?.isNotBlank() == true) {
            isValid.add(
                til_email_register?.validate(
                    ValidatorType.Email
                ) == true
            )
        }

        isValid.add(
            til_phone_register?.validate(
                ValidatorType.Required,
                ValidatorType.MinimumLength(9),
                ValidatorType.MaximumLength(13)
            ) == true
        )

        isValid.add(
            til_password_register?.validate(
                ValidatorType.Required,
                ValidatorType.MinimumLength(8)
            ) == true
        )

        val isConfirmationValid =
            til_password_confirmation_register?.value() == til_password_register?.value()

        isValid.add(
            isConfirmationValid
        )
        if (!isConfirmationValid) {
            til_password_confirmation_register?.apply {
                error = "Harus sama dengan kata sandi"
                isErrorEnabled = true
            }
        }

        val isImageValid = (activity as? RegisterActivity?)?.registerRequest?.ktp != null
        isValid.add(isImageValid)
        tv_error_image_ktp?.setVisibility(!isImageValid)

        return !isValid.contains(false)
    }

}