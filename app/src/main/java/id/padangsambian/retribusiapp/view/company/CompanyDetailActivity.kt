package id.padangsambian.retribusiapp.view.company

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import id.padangsambian.retribusiapp.R
import id.padangsambian.retribusiapp.model.CompanyRequest
import id.padangsambian.retribusiapp.model.getDefaultCompanyOperational
import id.padangsambian.retribusiapp.model.response.CompanyDetailResponse
import id.padangsambian.retribusiapp.presenter.CompanyPresenter
import id.padangsambian.retribusiapp.presenter.InitialHelper
import id.padangsambian.retribusiapp.utilities.constant.Permissions
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapp.utilities.getFileName
import id.padangsambian.retribusiapp.utilities.setSelections
import id.padangsambian.retribusiapp.view.common.BaseActivity
import id.padangsambian.retribusiapp.view.maps.MapsActivity
import id.padangsambian.retribusiapp.view.transaction.TransactionActivity
import id.padangsambian.shared.Utils
import id.padangsambian.shared.getColor
import id.padangsambian.shared.input.BaseDialogFragment
import id.padangsambian.shared.input.ImageRatioData
import id.padangsambian.shared.model.Company
import id.padangsambian.shared.model.CompanyOperationalItem
import id.padangsambian.shared.toCurrency
import id.widianapw.android_utils.common.Const
import id.widianapw.android_utils.extensions.*
import id.widianapw.android_utils.validator.ValidatorType
import kotlinx.android.synthetic.main.activity_company_detail.*
import kotlinx.android.synthetic.main.fragment_register2.*

class CompanyDetailActivity : BaseActivity(), CompanyPresenter.Delegate {

    companion object {
        const val STATE = "state"
        const val ID = "id"
    }

    enum class State {
        READ,
        EDIT,
        CREATE
    }

    private var state: State? = null
    private var id: Int? = null
    private val presenter = CompanyPresenter(this)
    private var companyData: Company? = null
    private var companyRequest = CompanyRequest()
    private var companyOperationalData = getDefaultCompanyOperational()
    private var companyOperationalAdapterEdit: CompanyOperationalAdapter? = null
    private var companyOperationalAdapterRead = CompanyOperationalAdapter({})
    private var pdfLauncher =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            val path =
                uri?.let {
                    getDriveFilePath(uri = it)
                }
            val filename =
                uri?.let { contentResolver?.getFileName(it) }
            path?.let {
                pb_file_picker?.visible()
                uploadPdfCloudinary(
                    it,
                    listener = object : CloudinaryListener {
                        override fun onSuccess(url: String) {
                            super.onSuccess(url)
                            companyRequest.documents = url
                            et_file_picker?.setText(filename)
                            pb_file_picker?.gone()
                        }

                        override fun onError() {
                            super.onError()
                            pb_file_picker?.gone()
                        }
                    })
            }

        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company_detail)
        intent?.run {
            state = getSerializableExtra(STATE) as State
            id = getIntExtra(ID, 0)
        }
        initListener()
        updateAction(state)
    }

    private fun updateAction(state: State?) {
        this.state = state
        when (state) {
            State.READ -> setReadView()
            State.EDIT -> setEditView()
            else -> {
            }
        }

    }

    private fun setEditView() {
        layout_company_edit?.visible()
        layout_company_read?.gone()
        updateView(companyData)
    }

    private fun setReadView() {
        layout_company_edit?.gone()
        layout_company_read?.visible()
        fetchData(true)
    }

    private fun fetchData(isFirst: Boolean = false) {
        isLoading(true, isFirst)
        doRequest {
            id?.let { presenter.getCompany(it) }
        }
    }

    private fun isLoading(izLoading: Boolean, isFirst: Boolean = false) {
        if (izLoading) {
            if (isFirst) loader_company_detail?.visible()
            else Utils.showProgress(this)
        } else {
            loader_company_detail?.animateGone()
            Utils.hideProgress()
        }
    }

    private fun openOperationalBS(operational: CompanyOperationalItem) {
        val bs = CompanyOperationalBS(operational) {
            companyOperationalAdapterEdit?.updateItem(it)
        }
        bs.show(supportFragmentManager, "")
    }

    private fun initListener() {
        setupToolbar("Data Usaha") {
            onBackPressed()
        }

        rv_company_operational_read?.adapter = companyOperationalAdapterRead

        //schedule
        companyOperationalAdapterEdit = CompanyOperationalAdapter { openOperationalBS(it) }
        rv_company_operational_schedule?.adapter = companyOperationalAdapterEdit


        btn_company_transaction_history?.onClick {
            with(Intent(this, TransactionActivity::class.java)) {
                putExtra(TransactionActivity.COMPANY_ID, this@CompanyDetailActivity.id)
                putExtra(
                    TransactionActivity.SUBSCRIPTION_TYPE,
                    companyData?.subscription?.subscription_type?.category?.lowercase()
                )
                startActivity(this)
            }
        }

        btn_edit_company?.onClick {
            updateAction(State.EDIT)
        }

        select_company_type?.setSelections(
            supportFragmentManager,
            InitialHelper.getCompanyTypeSelect()
        )

//        select_company_scale?.setSelections(
//            supportFragmentManager,
//            InitialHelper.getCompanyScaleSelect()
//        )

        select_subsription_type?.setSelections(
            supportFragmentManager,
            InitialHelper.getSubscriptionTypesScale()
        )

        btn_add_image_company?.onClick {
            openImageRes()
        }

        btn_edit_image_company?.onClick {
            openImageRes()
        }

        btn_set_close_company?.onClick {
            with(Intent(this, CompanyCloseActivity::class.java)) {
                putExtra(CompanyCloseActivity.COMPANY_ID, this@CompanyDetailActivity.id)
                startActivity(this)
            }
        }

        btn_delete_image_company?.onClick {
            showConfirmationDialog(
                image = R.drawable.ic_alert.getDrawable(this),
                title = "Apakah anda yakin ingin meghapus foto ini?",
                listener = object : BaseDialogFragment.Listener {
                    override fun primaryClick(dialog: Dialog) {
                        super.primaryClick(dialog)
                        companyRequest.photos = null
                        selected_section_image?.gone()
                        unselected_section_image?.visible()
                    }
                }
            )
        }

        et_file_picker?.onClick {
            if (checkPermission(Permissions.image))
                pdfLauncher.launch("application/pdf")
        }

        et_location_picker?.onClick {
            val intent = Intent(this, MapsActivity::class.java).apply {
                putExtra(MapsActivity.INIT_LAT, companyData?.latitude)
                putExtra(MapsActivity.INIT_LNG, companyData?.longitude)
            }
            startActivityForResult(
                intent,
                MapsActivity.PICKER_RESULT
            )
        }

        btn_register_2?.onClick {
            if (isAllValid()) {
                isLoading(true)
                companyRequest.apply {
                    name = til_name_company?.value()
                    photos = photos ?: companyData?.photos
                    documents = documents ?: companyData?.documents
                    address = til_address_company?.value()
                    company_type_id = select_company_type?.selectItem?.id
                    company_operational_schedule = companyOperationalAdapterEdit?.getData()
                }
                doRequest {
                    id?.let { it1 -> presenter.patchCompany(it1, companyRequest) }
                }
            }
        }

        btn_company_document_read?.onClick {
            companyData?.documents?.let {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(it))
                startActivity(browserIntent)
            }
        }

        btn_company_pin_read?.onClick {
            companyData?.latitude?.let {
                val intent = Intent(this, MapsActivity::class.java).apply {
                    putExtra(MapsActivity.INIT_LAT, companyData?.latitude)
                    putExtra(MapsActivity.INIT_LNG, companyData?.longitude)
                    putExtra(MapsActivity.STATE, MapsActivity.State.READ)
                }
                startActivity(intent)
            }
        }
    }


    private fun openImageRes() {
        openImagePicker(
            ImageRatioData(2, 1)
        ) {
            pb_image_upload?.visible()
            uploadImage(it)
        }
    }

    private fun uploadImage(it: Uri) {
        it.path?.let { path ->
            uploadCloudinary(
                path,
                object : CloudinaryListener {
                    override fun onSuccess(url: String) {
                        super.onSuccess(url)
                        companyRequest.photos = url
                        selected_section_image?.visible()
                        unselected_section_image?.gone()
                        iv_add_company?.loadImage(url)
                        pb_image_upload?.gone()
                    }

                    override fun onError() {
                        super.onError()
                        pb_image_upload?.gone()
                    }

                })
        }

    }

    override fun onBackPressed() {
        if (state == State.READ) {
            super.onBackPressed()
        } else
            updateAction(State.READ)
    }

    private fun updateView(response: Company?) {
        companyData = response
        companyOperationalData = companyData?.company_operational_schedule
        companyData?.run {
            chip_open_status_company?.text =
                if (company_close?.is_close == true) "Tutup" else "Buka"

//            Read
            val backgroundColor = when (getCompanyStatus()) {
                is Company.Status.Verified -> R.color.color_lightest_primary
                is Company.Status.WaitingReview -> R.color.color_light_yellow
                is Company.Status.Blocked -> R.color.color_light_error
            }

            val textColor = when (getCompanyStatus()) {
                is Company.Status.Verified -> R.color.color_primary
                is Company.Status.WaitingReview -> R.color.color_dark_yellow
                is Company.Status.Blocked -> R.color.color_primary_error
            }

            chip_company_status_read?.apply {
                setChipBackgroundColorResource(backgroundColor)
                setTextColor(textColor.getColor(this@CompanyDetailActivity))
                text = getCompanyStatus().status
            }


            tv_company_name_read?.text = name ?: "-"
            tv_company_address_read?.text = address ?: "-"
//            tv_company_scale_real?.text = subscription?.company_scale?.scale ?: "-"
            tv_company_tempekan_read?.text = banjar?.name ?: "-"
            tv_company_type_read?.text = company_type?.type ?: "-"
            tv_company_payment_type_read?.text = subscription?.subscription_type?.category ?: "-"
            tv_company_payment_amount_read?.text = subscription_amount?.toCurrency()

            val nextPayment =
                if (next_transaction == null) "-" else next_transaction?.date?.toDateString(
                    Const.READ_DATE_FORMAT,
                    isConvertToLocal = false
                )
            tv_company_next_payment_read?.text =
                "Tanggal pembayaran selanjutnya adalah $nextPayment"
            iv_company_read?.loadImage(photos)

            layout_cuti_company_detail?.setVisibility(company_close?.is_cuti == true)
            company_close?.schedule?.let {
                val READ_DATE = "dd MMMM yyyy"
                val dateStart = it.date_start?.toDateString(
                    fromFormat = "yyyy-MM-dd",
                    toFormat = READ_DATE
                )
                val dateEnd = it.date_end?.toDateString(
                    fromFormat = "yyyy-MM-dd",
                    toFormat = READ_DATE
                )
                if (dateStart == dateEnd) {
                    tv_company_close_detail?.text = dateStart
                } else {
                    tv_company_close_detail?.text = "$dateStart - $dateEnd"
                }

            }
            company_operational_schedule?.let {
                companyOperationalAdapterRead.setData(it)
                companyOperationalAdapterRead.setEditMode(false)
            }


//            Edit
            unselected_section_image?.gone()
            selected_section_image?.visible()
            iv_add_company?.loadImage(photos)

            til_name_company?.editText?.setText(name)
            til_address_company?.editText?.setText(address)
            til_address_company?.isEnabled = false

            company_type?.toSelectItem()?.let { select_company_type?.setItem(it) }
//            subscription?.company_scale?.toSelectItem()?.let { select_company_scale?.setItem(it) }
//            til_scale_company?.isEnabled = false

            subscription?.subscription_type?.toSelectItem()?.let {
                select_subsription_type?.setItem(
                    it
                )
            }
            til_subscription_type_company?.isEnabled = false

            til_document_company?.editText?.setText(documents)

            til_location_company?.isEnabled = false
            company_operational_schedule?.let {
                companyOperationalAdapterEdit?.setData(it)
                companyOperationalAdapterEdit?.setEditMode(true)
            }
            btn_register_2?.text = "Ubah"
        }


    }

    override fun getCompanyDetailResponse(response: CompanyDetailResponse) {
        super.getCompanyDetailResponse(response)
        isLoading(false)
        updateView(response.data)
    }

    override fun showError(title: String?, message: String?) {
        isLoading(false)
        super<BaseActivity>.showError(title, message)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                MapsActivity.PICKER_RESULT -> {
                    val latitude = data?.getDoubleExtra("latitude", 0.0)
                    val longitude = data?.getDoubleExtra("longitude", 0.0)
                    val address = data?.getStringExtra("address")
                    et_location_picker?.setText("$latitude, $longitude")
                    til_address_company?.editText?.setText(address)
                }
            }
        }
    }

    private fun isAllValid(): Boolean {
        val validity = mutableListOf<Boolean>()
        validity.add(
            til_name_company?.validate(
                ValidatorType.Required
            ) == true
        )

        validity.add(
            til_address_company?.validate(
                ValidatorType.Required
            ) == true
        )

        validity.add(
            til_type_company?.validate(
                ValidatorType.Required
            ) == true
        )

        return !validity.contains(false)
    }

    override fun getCompanyUpdateResponse(response: CompanyDetailResponse) {
        isLoading(false)
        super.getCompanyUpdateResponse(response)
        showAlertDialog(
            image = R.drawable.ic_success.getDrawable(this),
            title = "Perubahan berhasil disimpan!",
            listener = object : BaseDialogFragment.Listener {
                override fun primaryClick(dialog: Dialog) {
                    super.primaryClick(dialog)
                    onBackPressed()
                }
            }
        )
    }
}