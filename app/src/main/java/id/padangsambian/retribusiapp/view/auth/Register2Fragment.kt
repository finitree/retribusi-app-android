package id.padangsambian.retribusiapp.view.auth

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import id.padangsambian.retribusiapp.R
import id.padangsambian.retribusiapp.model.getDefaultCompanyOperational
import id.padangsambian.retribusiapp.presenter.InitialHelper
import id.padangsambian.retribusiapp.utilities.GlobalClass
import id.padangsambian.retribusiapp.utilities.Preferences
import id.padangsambian.retribusiapp.utilities.constant.Permissions
import id.padangsambian.retribusiapp.utilities.getFileName
import id.padangsambian.retribusiapp.utilities.setSelections
import id.padangsambian.retribusiapp.view.common.BaseActivity
import id.padangsambian.retribusiapp.view.company.CompanyOperationalAdapter
import id.padangsambian.retribusiapp.view.company.CompanyOperationalBS
import id.padangsambian.retribusiapp.view.maps.MapsActivity
import id.padangsambian.shared.input.BaseDialogFragment
import id.padangsambian.shared.input.ImageRatioData
import id.padangsambian.shared.model.CompanyOperationalItem
import id.widianapw.android_utils.extensions.*
import id.widianapw.android_utils.validator.ValidatorType
import kotlinx.android.synthetic.main.fragment_register2.*

class Register2Fragment : Fragment() {
    val preferences = Preferences(GlobalClass.applicationContext())
    private val companyOperationalData = getDefaultCompanyOperational()
    private var companyOperationalAdapter: CompanyOperationalAdapter? = null
    private var isDocumentRequired: Boolean = false

    private var pdfLauncher =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            val path =
                uri?.let {
                    (activity as? BaseActivity?)?.getDriveFilePath(uri = it)
                }
            val filename =
                uri?.let { (activity as? BaseActivity?)?.contentResolver?.getFileName(it) }
            path?.let {
                pb_file_picker?.visible()
                (activity as? BaseActivity?)?.uploadPdfCloudinary(
                    it,
                    listener = object : BaseActivity.CloudinaryListener {
                        override fun onSuccess(url: String) {
                            super.onSuccess(url)
                            (activity as? RegisterActivity?)?.registerRequest?.documents = url
                            et_file_picker?.setText(filename)
                            pb_file_picker?.gone()
                        }

                        override fun onError() {
                            super.onError()
                            pb_file_picker?.gone()
                        }
                    })
            }

        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register2, container, false)
    }

    private fun openOperationalBS(operational: CompanyOperationalItem) {
        val bs = CompanyOperationalBS(operational) {
            companyOperationalAdapter?.updateItem(it)
        }
        bs.show(childFragmentManager, "")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        companyOperationalAdapter = CompanyOperationalAdapter { openOperationalBS(it) }
        rv_company_operational_schedule?.adapter = companyOperationalAdapter
        companyOperationalData?.let { companyOperationalAdapter?.setData(it) }
        companyOperationalAdapter?.setEditMode(true)

        setupInitialValue()

        btn_register_2?.onClick {
            if (isAllValid()) {
                (activity as? BaseActivity?)?.showConfirmationDialog(
                    context?.let { it1 -> R.drawable.ic_alert.getDrawable(it1) },
                    "Apakah Anda Yakin Mengirim Data Akun dan Usaha Anda ?",
                    listener = object : BaseDialogFragment.Listener {
                        override fun primaryClick(dialog: Dialog) {
                            super.primaryClick(dialog)
                            (activity as? RegisterActivity?)?.register()
                        }
                    }
                )
            }
        }

        btn_add_image_company?.onClick {
            openImageRes()
        }

        btn_edit_image_company?.onClick {
            openImageRes()
        }

        btn_delete_image_company?.onClick {
            (activity as? BaseActivity?)?.showConfirmationDialog(
                image = context?.let { it1 -> R.drawable.ic_alert.getDrawable(it1) },
                title = "Apakah anda yakin ingin meghapus foto ini?",
                listener = object : BaseDialogFragment.Listener {
                    override fun primaryClick(dialog: Dialog) {
                        super.primaryClick(dialog)
                        (activity as? RegisterActivity?)?.registerRequest?.photos = null
                        selected_section_image?.gone()
                        unselected_section_image?.visible()
                    }
                }
            )
        }

        select_company_type?.setSelections(
            childFragmentManager,
            InitialHelper.getCompanyTypeSelect()
        )

        select_company_type?.onTextChanged {
            isDocumentRequired = InitialHelper.getCompanyTypes()
                ?.find { it.id == select_company_type?.selectItem?.id }?.isDocumentRequiredBool == true
            til_document_company?.apply {
                error = null
                isErrorEnabled = false
            }
            if (isDocumentRequired) {
                til_document_company?.helperText = ""
            } else {
                til_document_company?.helperText = "*Opsional"
            }
        }

//        select_company_scale?.setSelections(
//            childFragmentManager,
//            InitialHelper.getCompanyScaleSelect()
//        )

        select_subsription_type?.setSelections(
            childFragmentManager,
            InitialHelper.getSubscriptionTypesScale()
        )

        et_file_picker?.onClick {
            if ((activity as? BaseActivity?)?.checkPermission(Permissions.image) == true)
                pdfLauncher.launch("application/pdf")
        }

        et_location_picker?.onClick {
            startActivityForResult(
                Intent(context, MapsActivity::class.java),
                MapsActivity.PICKER_RESULT
            )
        }

    }

    private fun setupInitialValue() {
        (activity as? RegisterActivity?)?.registerRequest?.run {
            til_name_company?.editText?.setText(company_name)

            latitude?.let { _ ->
                et_location_picker?.setText("$latitude, $longitude")
            }

            til_address_company?.editText?.setText(address)

            photos?.let { img ->
                selected_section_image?.visible()
                unselected_section_image?.gone()
                iv_add_company?.loadImage(img)
            }
            documents?.let {
                et_file_picker?.setText(it)
            }
            address?.let {
                til_address_company?.editText?.setText(it)
            }
            company_operational_schedule?.let {
                companyOperationalAdapter?.setData(it)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                MapsActivity.PICKER_RESULT -> {
                    val latitude = data?.getDoubleExtra("latitude", 0.0)
                    val longitude = data?.getDoubleExtra("longitude", 0.0)
                    val address = data?.getStringExtra("address")
                    (activity as? RegisterActivity?)?.registerRequest?.apply {
                        this.latitude = latitude
                        this.longitude = longitude
                        this.address = address
                    }

                    et_location_picker?.setText("$latitude, $longitude")
                    til_address_company?.editText?.setText(address)
                }
            }
        }
    }

    private fun isAllValid(): Boolean {
        val isValid = mutableListOf<Boolean>()

        isValid.add(
            til_name_company?.validate(
                ValidatorType.Required
            ) == true
        )

        isValid.add(
            til_address_company?.validate(
                ValidatorType.Required
            ) == true
        )

        isValid.add(
            til_type_company?.validate(
                ValidatorType.Required
            ) == true
        )

//        isValid.add(
//            til_scale_company?.validate(
//                ValidatorType.Required
//            ) == true
//        )

        isValid.add(
            til_subscription_type_company?.validate(
                ValidatorType.Required
            ) == true
        )

        isValid.add(
            til_location_company?.validate(
                ValidatorType.Required
            ) == true
        )

        isValid.add(
            til_address_company?.validate(
                ValidatorType.Required
            ) == true
        )

        if (isDocumentRequired) {
            isValid.add(til_document_company?.validate(ValidatorType.Required) == true)
        }


        val isImageValid = (activity as? RegisterActivity?)?.registerRequest?.photos != null
        isValid.add(isImageValid)
        tv_error_image_company?.setVisibility(!isImageValid)

        (activity as? RegisterActivity?)?.registerRequest?.apply {
            company_name = til_name_company?.value()
            address = til_address_company?.value()
//            company_scale_id = select_company_scale?.selectItem?.id
            subscription_type_id = select_subsription_type?.selectItem?.id
            company_type_id = select_company_type?.selectItem?.id
            company_operational_schedule = companyOperationalAdapter?.getData()
        }
        return !isValid.contains(false)
    }

    private fun openImageRes() {
        (activity as? BaseActivity?)?.openImagePicker(
            ImageRatioData(2, 1)
        ) {
            pb_image_upload?.visible()
            uploadImage(it)
        }
    }

    private fun uploadImage(it: Uri) {
        it.path?.let { path ->
            (activity as? BaseActivity?)?.uploadCloudinary(
                path,
                object : BaseActivity.CloudinaryListener {
                    override fun onSuccess(url: String) {
                        super.onSuccess(url)
                        selected_section_image?.visible()
                        unselected_section_image?.gone()
                        iv_add_company?.loadImage(url)
                        (activity as? RegisterActivity?)?.registerRequest?.photos = url
                        pb_image_upload?.gone()
                    }

                    override fun onError() {
                        super.onError()
                        pb_image_upload?.gone()
                    }

                })
        }

    }


}