package id.padangsambian.retribusiapp.view.auth

import android.os.Bundle
import id.padangsambian.retribusiapp.R
import id.padangsambian.retribusiapp.model.ResetPasswordRequest
import id.padangsambian.retribusiapp.presenter.auth.AuthPresenter
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapp.view.common.BaseActivity
import id.padangsambian.shared.Utils
import id.padangsambian.shared.model.SuccessResponse
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.validate
import id.widianapw.android_utils.extensions.value
import id.widianapw.android_utils.validator.ValidatorType
import kotlinx.android.synthetic.main.activity_forgot_password.*

class ForgotPasswordActivity : BaseActivity(), AuthPresenter.Delegate {
    private val presenter = AuthPresenter(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        setupToolbar("Lupa Kata Sandi") {
            onBackPressed()
        }
        initView()
        initListener()

    }

    private fun initView() {

    }

    private fun initListener() {
        btn_reset_password?.onClick {
            if (isValid()) {
                isLoading(true)
                val req = ResetPasswordRequest().apply {
                    email = til_email?.value()
                }
                doRequest {
                    presenter.sendResetPassword(req)
                }
            }
        }
    }

    private fun isLoading(izLoading: Boolean) {
        if (izLoading) {
            Utils.showProgress(this)
        } else {
            Utils.hideProgress()
        }
    }

    override fun sendResetPasswordResponse(res: SuccessResponse) {
        super.sendResetPasswordResponse(res)
        isLoading(false)
        showSuccessDialog("Link reset kata sandi telah terkirim ke email anda, reset kata sandi lalu login ulang pada aplikasi.")
    }

    override fun showError(title: String?, message: String?) {
        isLoading(false)
        super<BaseActivity>.showError(title, message)
    }

    private fun isValid(): Boolean {
        return til_email?.validate(ValidatorType.Required, ValidatorType.Email) == true
    }

}