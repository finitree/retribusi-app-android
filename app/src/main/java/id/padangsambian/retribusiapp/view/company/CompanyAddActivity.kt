package id.padangsambian.retribusiapp.view.company

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import id.padangsambian.retribusiapp.R
import id.padangsambian.retribusiapp.model.CompanyAddRequest
import id.padangsambian.retribusiapp.model.getDefaultCompanyOperational
import id.padangsambian.retribusiapp.model.response.CompanyDetailResponse
import id.padangsambian.retribusiapp.presenter.CompanyPresenter
import id.padangsambian.retribusiapp.presenter.InitialHelper
import id.padangsambian.retribusiapp.utilities.constant.Permissions
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapp.utilities.getFileName
import id.padangsambian.retribusiapp.utilities.setSelections
import id.padangsambian.retribusiapp.view.common.BaseActivity
import id.padangsambian.retribusiapp.view.maps.MapsActivity
import id.padangsambian.shared.Utils
import id.padangsambian.shared.input.BaseDialogFragment
import id.padangsambian.shared.input.ImageRatioData
import id.padangsambian.shared.model.CompanyOperationalItem
import id.widianapw.android_utils.extensions.*
import id.widianapw.android_utils.validator.ValidatorType
import kotlinx.android.synthetic.main.fragment_register2.*

class CompanyAddActivity : BaseActivity(), CompanyPresenter.Delegate {
    private val presenter = CompanyPresenter(this)
    private val request = CompanyAddRequest()
    private var companyOperationalData = getDefaultCompanyOperational()
    private var companyOperationalAdapter: CompanyOperationalAdapter? = null
    private var isDocumentRequired: Boolean = false
    private var pdfLauncher =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            val path =
                uri?.let {
                    getDriveFilePath(uri = it)
                }
            val filename =
                uri?.let { contentResolver?.getFileName(it) }
            path?.let {
                pb_file_picker?.visible()
                uploadPdfCloudinary(
                    it,
                    listener = object : CloudinaryListener {
                        override fun onSuccess(url: String) {
                            super.onSuccess(url)
                            request.documents = url
                            et_file_picker?.setText(filename)
                            pb_file_picker?.gone()
                        }

                        override fun onError() {
                            super.onError()
                            pb_file_picker?.gone()
                        }
                    })
            }

        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company_add)
        setupToolbar("Tambah Usaha") {
            onBackPressed()
        }
        initListener()

    }

    private fun initListener() {
        btn_register_2?.text = "Tambah Usaha"
        btn_register_2?.onClick {
            if (isAllValid()) {
                showConfirmationDialog(
                    this.let { it1 -> R.drawable.ic_alert.getDrawable(it1) },
                    "Apakah Anda Yakin Mengirim Data Usaha Anda ?",
                    listener = object : BaseDialogFragment.Listener {
                        override fun primaryClick(dialog: Dialog) {
                            super.primaryClick(dialog)
                            createCompany()
                        }
                    }
                )
            }
        }
        //schedule
        companyOperationalAdapter = CompanyOperationalAdapter {
            openOperationalBS(it)
        }

        rv_company_operational_schedule?.adapter = companyOperationalAdapter
        setupInitSchedule()

        btn_add_image_company?.onClick {
            openImageRes()
        }

        btn_edit_image_company?.onClick {
            openImageRes()
        }

        btn_delete_image_company?.onClick {
            showConfirmationDialog(
                image = this.let { it1 -> R.drawable.ic_alert.getDrawable(it1) },
                title = "Apakah anda yakin ingin meghapus foto ini?",
                listener = object : BaseDialogFragment.Listener {
                    override fun primaryClick(dialog: Dialog) {
                        super.primaryClick(dialog)
                        request.photos = null
                        selected_section_image?.gone()
                        unselected_section_image?.visible()
                    }
                }
            )
        }

        select_company_type?.setSelections(
            supportFragmentManager,
            InitialHelper.getCompanyTypeSelect()
        )
        select_company_type?.onTextChanged {
            isDocumentRequired = InitialHelper.getCompanyTypes()
                ?.find { it.id == select_company_type?.selectItem?.id }?.isDocumentRequiredBool == true
            til_document_company?.apply {
                error = null
                isErrorEnabled = false
            }
            if (isDocumentRequired) {
                til_document_company?.helperText = ""
            } else {
                til_document_company?.helperText = "*Opsional"
            }
        }

//        select_company_scale?.setSelections(
//            supportFragmentManager,
//            InitialHelper.getCompanyScaleSelect()
//        )

        select_subsription_type?.setSelections(
            supportFragmentManager,
            InitialHelper.getSubscriptionTypesScale()
        )

        et_file_picker?.onClick {
            if (checkPermission(Permissions.image))
                pdfLauncher.launch("application/pdf")
        }

        et_location_picker?.onClick {
            val intent = Intent(this, MapsActivity::class.java)
            request.latitude?.let {
                intent.putExtra(MapsActivity.INIT_LNG, request.longitude)
                intent.putExtra(MapsActivity.INIT_LAT, request.latitude)

            }
            startActivityForResult(
                intent,
                MapsActivity.PICKER_RESULT
            )
        }
    }

    private fun setupInitSchedule() {
        companyOperationalAdapter?.setEditMode(true)
        companyOperationalData?.let { companyOperationalAdapter?.setData(it) }
    }

    private fun openOperationalBS(operational: CompanyOperationalItem) {
        val bs = CompanyOperationalBS(operational) {
            companyOperationalAdapter?.updateItem(it)
        }
        bs.show(supportFragmentManager, "")
    }

    private fun createCompany() {
        isLoading(true)
        doRequest {
            presenter.storeCompany(request)
        }
    }

    private fun isLoading(izLoading: Boolean) {
        if (izLoading) Utils.showProgress(this)
        else Utils.hideProgress()
    }

    private fun openImageRes() {
        openImagePicker(
            ImageRatioData(2, 1)
        ) {
            pb_image_upload?.visible()
            uploadImage(it)
        }
    }

    private fun uploadImage(it: Uri) {
        it.path?.let { path ->
            uploadCloudinary(
                path,
                object : BaseActivity.CloudinaryListener {
                    override fun onSuccess(url: String) {
                        super.onSuccess(url)
                        selected_section_image?.visible()
                        unselected_section_image?.gone()
                        iv_add_company?.loadImage(url)
                        request.photos = url
                        pb_image_upload?.gone()
                    }

                    override fun onError() {
                        super.onError()
                        pb_image_upload?.gone()
                    }

                })
        }

    }


    private fun isAllValid(): Boolean {
        val isValid = mutableListOf<Boolean>()

        isValid.add(
            til_name_company?.validate(
                ValidatorType.Required
            ) == true
        )

        isValid.add(
            til_address_company?.validate(
                ValidatorType.Required
            ) == true
        )

        isValid.add(
            til_type_company?.validate(
                ValidatorType.Required
            ) == true
        )

//        isValid.add(
//            til_scale_company?.validate(
//                ValidatorType.Required
//            ) == true
//        )

        isValid.add(
            til_subscription_type_company?.validate(
                ValidatorType.Required
            ) == true
        )

        isValid.add(
            til_location_company?.validate(
                ValidatorType.Required
            ) == true
        )

        isValid.add(
            til_address_company?.validate(
                ValidatorType.Required
            ) == true
        )

        if (isDocumentRequired) {
            isValid.add(til_document_company?.validate(ValidatorType.Required) == true)
        }

        val isImageValid = request.photos != null
        isValid.add(isImageValid)
        tv_error_image_company?.setVisibility(!isImageValid)

        request.apply {
            name = til_name_company?.value()
            address = til_address_company?.value()
//            company_scale_id = select_company_scale?.selectItem?.id
            subscription_type_id = select_subsription_type?.selectItem?.id
            company_type_id = select_company_type?.selectItem?.id
            company_operational_schedule = companyOperationalAdapter?.getData()
        }
        return !isValid.contains(false)
    }

    override fun getCompanyAddResponse(response: CompanyDetailResponse) {
        super.getCompanyAddResponse(response)
        isLoading(false)
        showAlertDialog(
            image = R.drawable.ic_success.getDrawable(this),
            "Pendaftaran Berhasil",
            "Selamat akun Anda berhasil dibuat. Tunggu admin untuk melakukan verifikasi usaha Anda.",
            listener = object : BaseDialogFragment.Listener {
                override fun primaryClick(dialog: Dialog) {
                    super.primaryClick(dialog)
                    finish()
                }
            }
        )
    }

    override fun showError(title: String?, message: String?) {
        isLoading(false)
        super<BaseActivity>.showError(title, message)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                MapsActivity.PICKER_RESULT -> {
                    val latitude = data?.getDoubleExtra("latitude", 0.0)
                    val longitude = data?.getDoubleExtra("longitude", 0.0)
                    val address = data?.getStringExtra("address")
                    request.apply {
                        this.latitude = latitude
                        this.longitude = longitude
                        this.address = address
                    }

                    et_location_picker?.setText("$latitude, $longitude")
                    til_address_company?.editText?.setText(address)
                }
            }
        }
    }
}
