package id.padangsambian.retribusiapp.view.company

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.padangsambian.retribusiapp.R
import id.padangsambian.shared.days
import id.padangsambian.shared.model.CompanyOperationalItem
import id.padangsambian.shared.openTimePicker
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.value
import kotlinx.android.synthetic.main.fragment_company_operational_b_s.*

class CompanyOperationalBS(
    val operational: CompanyOperationalItem,
    val onSave: (CompanyOperationalItem) -> Unit
) : BottomSheetDialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.AppTheme_BottomSheetDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_company_operational_b_s, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val dayName = days.find { it.id == operational.day }?.name
        tv_operatinal_day?.text = dayName

        til_opening_hours?.editText?.apply {
            setText(operational.opening_hours)
            onClick {
                openTimePicker(childFragmentManager)
            }
        }

        til_closing_hours?.editText?.apply {
            setText(operational.closing_hours)
            onClick {
                openTimePicker(childFragmentManager)
            }
        }

        switch_operational_open?.text = if (operational.isOpenBool()) "Buka" else "Tutup"
        switch_operational_open?.isChecked = operational.isOpenBool()
        switch_operational_open?.setOnCheckedChangeListener { buttonView, isChecked ->
            switch_operational_open?.text = if (isChecked) "Buka" else "Tutup"
        }

        btn_save_operational?.onClick {
            val toSend = operational
            toSend.opening_hours = til_opening_hours?.value()
            toSend.closing_hours = til_closing_hours?.value()
            toSend.is_open = if (switch_operational_open?.isChecked == true) 1 else 0
            onSave(toSend)
            dismiss()
        }

    }
}