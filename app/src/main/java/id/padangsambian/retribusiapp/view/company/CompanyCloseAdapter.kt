package id.padangsambian.retribusiapp.view.company

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.padangsambian.retribusiapp.R
import id.padangsambian.shared.model.CompanyCloseItem
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.toDateString
import kotlinx.android.synthetic.main.activity_company_detail.*
import kotlinx.android.synthetic.main.layout_rv_company_close.view.*
import java.lang.Exception

class CompanyCloseAdapter(
    val onEdit: (CompanyCloseItem) -> Unit,
    val onDelete: (CompanyCloseItem) -> Unit
) : RecyclerView.Adapter<CompanyCloseAdapter.ViewHolder>() {
    private val list = mutableListOf<CompanyCloseItem>()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_rv_company_close, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.itemView.apply {
            val READ_DATE = "dd MMMM yyyy"
            val dateStart = item.date_start?.toDateString(
                fromFormat = "yyyy-MM-dd",
                toFormat = READ_DATE
            )
            val dateEnd = item.date_end?.toDateString(
                fromFormat = "yyyy-MM-dd",
                toFormat = READ_DATE
            )
            if (dateStart == dateEnd) {
                tv_date_rv_company_close?.text = dateStart
            } else {
                tv_date_rv_company_close?.text = "$dateStart - $dateEnd"
            }


            btn_edit_rv_company_close?.onClick {
                onEdit(item)
            }

            btn_delete_rv_company_close?.onClick {
                onDelete(item)
            }

        }
    }

    override fun getItemCount(): Int = list.count()

    fun setData(data: List<CompanyCloseItem>) {
        this.list.apply {
            clear()
            addAll(data)
        }
        notifyDataSetChanged()
    }

    fun removeItem(id: Int) {
        try {
            val ids = this.list.map { it.id }
            val index = ids.indexOf(id)
            this.list.removeAt(index)
            notifyItemRemoved(index)
            notifyItemRangeChanged(index, itemCount)
        } catch (e: Exception) {

        }
    }
}