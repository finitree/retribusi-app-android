package id.padangsambian.retribusiapp.view.company

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.padangsambian.retribusiapp.R
import id.padangsambian.retribusiapp.model.CompanyCloseRequest
import id.padangsambian.retribusiapp.presenter.CompanyPresenter
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.shared.Utils
import id.padangsambian.shared.model.CompanyCloseItem
import id.padangsambian.shared.model.SuccessResponse
import id.padangsambian.shared.openDatePicker
import id.widianapw.android_utils.common.Const
import id.widianapw.android_utils.extensions.*
import id.widianapw.android_utils.validator.ValidatorType
import kotlinx.android.synthetic.main.fragment_company_cuti_b_s.*

class CompanyCutiBS(
    val companyId: Int,
    val initial: CompanyCloseItem? = null,
    val onSave: () -> Unit
) :
    BottomSheetDialogFragment(), CompanyPresenter.Delegate {
    private var isEdit = initial != null
    val DATE_FORMAT_API = "yyyy-MM-dd"

    private val presenter = CompanyPresenter(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.AppTheme_BottomSheetDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_company_cuti_b_s, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        til_date_start_cuti?.editText?.apply {
            initial?.date_start?.let {
                val formattedDate = it.toDateString(
                    fromFormat = DATE_FORMAT_API,
                    toFormat = Const.READ_DATE_FORMAT
                )
                setText(formattedDate)
            }
            onClick {
                openDatePicker(childFragmentManager, isValidateForward = true)
            }
        }

        til_date_end_cuti?.editText?.apply {
            initial?.date_end?.let {
                val formattedDate = it.toDateString(
                    fromFormat = DATE_FORMAT_API,
                    toFormat = Const.READ_DATE_FORMAT
                )
                setText(formattedDate)
            }
            onClick { openDatePicker(childFragmentManager, isValidateForward = true) }
        }

        btn_save_cuti?.onClick {
            if (isAllValid()) {
                isLoading(true)
                val startDate = til_date_start_cuti?.value()
                    ?.toDateString(fromFormat = Const.READ_DATE_FORMAT, toFormat = DATE_FORMAT_API)
                val endDate = til_date_end_cuti?.value()
                    ?.toDateString(fromFormat = Const.READ_DATE_FORMAT, toFormat = DATE_FORMAT_API)
                val req = CompanyCloseRequest().apply {
                    date_start = startDate
                    date_end = endDate
                }
                if (isEdit) {
                    doRequest {
                        initial?.company_id?.let { it1 ->
                            initial.id?.let { it2 ->
                                presenter.patchCompanyClose(
                                    it1,
                                    it2, req
                                )
                            }
                        }
                    }
                } else {
                    doRequest {
                        presenter.postCompanyClose(companyId, req)
                    }
                }
            }
        }
    }

    override fun postCompanyCloseResponse(response: SuccessResponse) {
        super.postCompanyCloseResponse(response)
        isLoading(false)
        onSave()
        dismiss()
    }

    override fun patchCompanyCloseResponse(respnse: SuccessResponse) {
        super.patchCompanyCloseResponse(respnse)
        isLoading(false)
        onSave()
        dismiss()
    }

    fun isLoading(izLoading: Boolean) {
        if (izLoading) {
            context?.let { Utils.showProgress(it) }
        } else {
            Utils.hideProgress()
        }
    }

    private fun isAllValid(): Boolean {
        val validities = mutableListOf<Boolean>()
        validities.add(
            til_date_start_cuti?.validate(
                ValidatorType.Required
            ) == true
        )
        validities.add(
            til_date_end_cuti?.validate(
                ValidatorType.Required
            ) == true
        )

        return !validities.contains(false)

    }
}