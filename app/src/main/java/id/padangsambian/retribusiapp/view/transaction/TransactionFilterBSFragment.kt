package id.padangsambian.retribusiapp.view.transaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.padangsambian.retribusiapp.R
import id.padangsambian.shared.openDatePicker
import id.widianapw.android_utils.common.Const
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.toDateString
import id.widianapw.android_utils.extensions.validate
import id.widianapw.android_utils.extensions.value
import id.widianapw.android_utils.validator.ValidatorType
import kotlinx.android.synthetic.main.fragment_company_cuti_b_s.*
import kotlinx.android.synthetic.main.fragment_transaction_filter_b_s.*

/**
 * A simple [Fragment] subclass.
 * Use the [TransactionFilterBSFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
data class FilterReq(
    val date_from: String?,
    val date_to: String?
)

class TransactionFilterBSFragment(
    val initialFilter: FilterReq? = null,
    val onSave: (FilterReq) -> Unit,
    val onReset: () -> Unit
) :
    BottomSheetDialogFragment() {
    val DATE_FORMAT_API = "yyyy-MM-dd"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.AppTheme_BottomSheetDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaction_filter_b_s, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        til_date_start_transaction?.editText?.apply {
//            initial?.date_start?.let {
//                val formattedDate = it.toDateString(
//                    fromFormat = DATE_FORMAT_API,
//                    toFormat = Const.READ_DATE_FORMAT
//                )
//                setText(formattedDate)
//            }
            initialFilter?.date_from?.let {
                val formattedDate = it.toDateString(
                    fromFormat = DATE_FORMAT_API,
                    toFormat = Const.READ_DATE_FORMAT
                )
                setText(formattedDate)
            }
            onClick {
                openDatePicker(childFragmentManager, isValidateBackward = true)
            }
        }

        til_date_end_transaction?.editText?.apply {
            initialFilter?.date_to?.let {
                val formattedDate = it.toDateString(
                    fromFormat = DATE_FORMAT_API,
                    toFormat = Const.READ_DATE_FORMAT
                )
                setText(formattedDate)
            }
            onClick {
                openDatePicker(childFragmentManager, isValidateBackward = true)
            }
        }


        btn_cancel_filter?.onClick {
            this.dismiss()
        }

        btn_reset_filter?.onClick {
            this.dismiss()
            onReset()
        }

        btn_save_filter?.onClick {
            if (isValid()) {
                this.dismiss()
                val startDate = til_date_start_transaction?.value()
                    ?.toDateString(fromFormat = Const.READ_DATE_FORMAT, toFormat = DATE_FORMAT_API)
                val endDate = til_date_end_transaction?.value()
                    ?.toDateString(fromFormat = Const.READ_DATE_FORMAT, toFormat = DATE_FORMAT_API)

                onSave(
                    FilterReq(
                        startDate,
                        endDate
                    )
                )
            }
        }
    }

    private fun isValid(): Boolean {
        val validities = mutableListOf<Boolean>()
        validities.add(
            til_date_start_transaction?.validate(
                ValidatorType.Required
            ) == true
        )

        validities.add(
            til_date_end_transaction?.validate(
                ValidatorType.Required
            ) == true
        )

        return !validities.contains(false)
    }
}