package id.padangsambian.retribusiapp.view.common

import android.app.Activity
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.drawable.Drawable
import android.net.Uri
import android.provider.OpenableColumns
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.cloudinary.android.MediaManager
import com.cloudinary.android.callback.ErrorInfo
import com.cloudinary.android.callback.UploadCallback
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.orhanobut.logger.Logger
import id.padangsambian.retribusiapp.R
import id.padangsambian.retribusiapp.presenter.base.BaseDelegate
import id.padangsambian.retribusiapp.utilities.Preferences
import id.padangsambian.retribusiapp.utilities.constant.Permissions
import id.padangsambian.shared.input.BaseDialogFragment
import id.padangsambian.shared.input.ImagePickerDialogFragment
import id.padangsambian.shared.input.ImagePickerListener
import id.padangsambian.shared.input.ImageRatioData
import id.widianapw.android_utils.extensions.getDrawable
import kotlinx.android.synthetic.main.fragment_appbar.*
import kotlinx.coroutines.Job
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.net.URLEncoder


/**
 * Created by Widiana Putra on 10/11/21
 * Copyright (c)
 */
open class BaseActivity : AppCompatActivity(), BaseDelegate {
    companion object {
        const val PDF_PICKER = 1
    }

    val jobs = mutableListOf<Job>()
    var baseDialog: BaseDialogFragment? = null
    val preferences by lazy { Preferences(this) }
    val getContent = registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
        // Handle the returned Uri
        Logger.d(uri?.path.toString() + "PATH")
        Logger.d(uri?.path)
        val path = uri?.let { getDriveFilePath(uri = it) }
        path?.let {
            uploadCloudinary(it, listener = object : CloudinaryListener {
                override fun onSuccess(url: String) {
                    super.onSuccess(url)
                    showAlertDialog(title = url)
                }
            })
        }
    }

    override fun showError(title: String?, message: String?) {
        showAlertDialog(title = title, description = message)
    }

    fun disposeJobs() {
        jobs.forEach {
            it.cancel()
        }
    }

    fun showConfirmationDialog(
        image: Drawable? = null,
        title: String? = null,
        description: String? = null,
        listener: BaseDialogFragment.Listener
    ) {
        val dialog = BaseDialogFragment(
            BaseDialogFragment.Type.Confirmation(
                image, title, description, listener = listener
            )
        )
        dialog.show(supportFragmentManager, "")
    }

    fun showAlertDialog(
        image: Drawable? = null,
        title: String? = null,
        description: String? = null,
        listener: BaseDialogFragment.Listener? = null
    ) {
        if (baseDialog?.isDisplayed != true) {
            baseDialog = BaseDialogFragment(
                BaseDialogFragment.Type.Alert(
                    image, title, description, listener = listener
                )
            )
            baseDialog?.show(supportFragmentManager, "")
        }
    }

    fun showSuccessSavedDialog(listener: BaseDialogFragment.Listener? = null) {
        val dialog = BaseDialogFragment(
            BaseDialogFragment.Type.Alert(
                image = R.drawable.ic_success.getDrawable(this),
                title = "Berhasil tersimpan!",
                listener = listener
            )
        )
        dialog.show(supportFragmentManager, "")
    }

    fun setupToolbar(title: String, backListener: (() -> Unit)? = null) {
        tl_main?.title = title
        if (backListener == null) {
            tl_main?.navigationIcon = null
        } else {
            tl_main?.setNavigationOnClickListener {
                backListener()
            }
        }
    }

    fun openImagePicker(ratio: ImageRatioData, listener: (Uri) -> Unit) {
        if (checkPermission(Permissions.image)) {
            val dialog = ImagePickerDialogFragment(ratio, listener = object : ImagePickerListener {
                override fun onCropImage(dialog: BottomSheetDialogFragment, uri: Uri) {
                    super.onCropImage(dialog, uri)
                    listener(uri)
                }
            })
            dialog.show(supportFragmentManager, "")
        }
    }

    interface CloudinaryListener {
        fun onSuccess(url: String) {}
        fun onError() {}
        fun onProgress(progress: Long) {
            Logger.d(progress)
        }
    }

    fun uploadCloudinary(path: String, listener: CloudinaryListener) {
        MediaManager.get().upload(path).unsigned("retribusi").callback(
            object : UploadCallback {
                override fun onStart(requestId: String?) {
                }

                override fun onProgress(requestId: String?, bytes: Long, totalBytes: Long) {
                    listener.onProgress(bytes / totalBytes)
                    Logger.d(bytes)
                    Logger.d(totalBytes)
                }

                override fun onSuccess(requestId: String?, resultData: MutableMap<Any?, Any?>?) {
                    val secureUrl = resultData?.get("secure_url").toString()
                    listener.onSuccess(secureUrl)
                }

                override fun onError(requestId: String?, error: ErrorInfo?) {
                    listener.onError()
                    showAlertDialog(title = error?.description)
                }

                override fun onReschedule(requestId: String?, error: ErrorInfo?) {
                }
            }
        ).dispatch(this)
    }

    fun uploadPdfCloudinary(path: String, listener: CloudinaryListener) {
//        Logger.d(CloudinaryPDF().getPdfUrl("ew17onnpmh8v3vgvonvj.pdf"))
//        val options: MutableMap<String, Any> = HashMap()
//        options["tags"] = "attachment"
//
        MediaManager.get().upload(path).unsigned("retribusi").callback(
            object : UploadCallback {
                override fun onStart(requestId: String?) {
                }

                override fun onProgress(requestId: String?, bytes: Long, totalBytes: Long) {
                    listener.onProgress(bytes / totalBytes)
                    Logger.d(bytes)
                    Logger.d(totalBytes)
                }

                override fun onSuccess(requestId: String?, resultData: MutableMap<Any?, Any?>?) {
                    val secureUrl = resultData?.get("secure_url").toString()
                    listener.onSuccess(secureUrl)
                }

                override fun onError(requestId: String?, error: ErrorInfo?) {
                    listener.onError()
                    showAlertDialog(title = error?.description)
                }

                override fun onReschedule(requestId: String?, error: ErrorInfo?) {
                }
            }
        ).dispatch(this)
    }


    fun getDriveFilePath(uri: Uri): String {
        val returnUri = uri
        val returnCursor: Cursor? = this.contentResolver.query(returnUri, null, null, null, null)     /*
       * Get the column indexes of the data in the Cursor,
       *     * move to the first row in the Cursor, get the data,
       *     * and display it.
       * */
        val nameIndex: Int = returnCursor!!.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        val sizeIndex: Int = returnCursor.getColumnIndex(OpenableColumns.SIZE)
        returnCursor.moveToFirst()
        val name: String = (returnCursor.getString(nameIndex))
        val size = java.lang.Long.toString(returnCursor.getLong(sizeIndex))
        val file = File(this.cacheDir, URLEncoder.encode(name, "utf-8"))
        try {
            val inputStream: InputStream? = this.contentResolver.openInputStream(uri)
            val outputStream = FileOutputStream(file)
            val read: Int = 0
            val maxBufferSize: Int = 1 * 1024 * 1024
            val bytesAvailable: Int = inputStream!!.available()       //int bufferSize = 1024;
            val bufferSize: Int = Math.min(bytesAvailable, maxBufferSize)
            val buffers = ByteArray(bufferSize)
            inputStream.use { inputStream: InputStream ->
                outputStream.use { fileOut ->
                    while (true) {
                        val length = inputStream.read(buffers)
                        if (length <= 0)
                            break
                        fileOut.write(buffers, 0, length)
                    }
                    fileOut.flush()
                    fileOut.close()
                }
            }
            inputStream.close()
        } catch (e: Exception) {

            Log.e("Exception", e.message.toString())
        }
        return file.path
    }

    fun openPdfPicker() {
        if (checkPermission(Permissions.image))
            getContent.launch("application/pdf")
        else askPermission(this, Permissions.image)
    }


    fun askPermission(activity: Activity, permission: Array<out String>) =
        ActivityCompat.requestPermissions(activity, permission, 69)

    fun checkPermission(permissions: Array<out String>): Boolean {
        val list = arrayListOf<String>()
        permissions.forEach {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    it
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                list.add(it)
            }
        }
        if (list.isNotEmpty()) {
            askPermission(this, list.toArray(arrayOf()))
        }
        return list.isEmpty()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    fun showSuccessDialog(title: String? = null, listener: BaseDialogFragment.Listener? = null) {
        val dialog = BaseDialogFragment(
            BaseDialogFragment.Type.Alert(
                image = R.drawable.ic_success.getDrawable(this),
                title = title ?: "Berhasil disimpan!",
                listener = listener
            )
        )
        dialog.show(supportFragmentManager, "")
    }

}
