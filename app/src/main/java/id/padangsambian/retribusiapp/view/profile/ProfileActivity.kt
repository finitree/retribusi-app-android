package id.padangsambian.retribusiapp.view.profile

import android.app.Dialog
import android.net.Uri
import android.os.Bundle
import id.padangsambian.retribusiapp.R
import id.padangsambian.retribusiapp.model.ProfileRequest
import id.padangsambian.retribusiapp.model.response.ProfileResponse
import id.padangsambian.retribusiapp.presenter.ProfilePresenter
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapp.view.common.BaseActivity
import id.padangsambian.shared.Utils
import id.padangsambian.shared.input.BaseDialogFragment
import id.padangsambian.shared.input.ImageRatioData
import id.widianapw.android_utils.extensions.*
import id.widianapw.android_utils.validator.ValidatorType
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : BaseActivity(), ProfilePresenter.Delegate {
    private val presenter = ProfilePresenter(this)
    var imageUrl: String? = null
    var ktpUrl: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        initListener()
        loadProfile()
    }

    private fun loadProfile() {
        isLoading(true)
        doRequest {
            presenter.getProfile()
        }
    }

    override fun getProfileResponse(response: ProfileResponse) {
        super.getProfileResponse(response)
        isLoading(false)
        response.data?.let {
            til_name_profile?.editText?.setText(it.name)
            til_email_profile?.editText?.setText(it.email)
            til_phone_profile?.editText?.setText(it.phone)
            it.photo?.let {
                iv_profile?.loadImage(it)
            }

            it.ktp?.let { url ->
                selected_section_image?.visible()
                unselected_section_image?.gone()
                iv_add_ktp?.loadImage(url)
                ktpUrl = url
            }
        }
    }

    override fun showError(title: String?, message: String?) {
        isLoading(false)
        super<BaseActivity>.showError(title, message)
    }

    private fun isLoading(izLoading: Boolean) {
        if (izLoading) Utils.showProgress(this)
        else Utils.hideProgress()
    }

    private fun openImageRes() {
        openImagePicker(
            ImageRatioData(2, 1)
        ) {
            pb_image_upload?.visible()
            uploadImage(it)
        }
    }

    private fun uploadImage(it: Uri) {
        it.path?.let { path ->
            uploadCloudinary(
                path,
                object : CloudinaryListener {
                    override fun onSuccess(url: String) {
                        super.onSuccess(url)
                        selected_section_image?.visible()
                        unselected_section_image?.gone()
                        iv_add_ktp?.loadImage(url)
                        ktpUrl = url
                        pb_image_upload?.gone()
                    }

                    override fun onError() {
                        super.onError()
                        pb_image_upload?.gone()
                    }

                })
        }

    }


    private fun initListener() {
        setupToolbar("Profile") {
            onBackPressed()
        }

        btn_add_image_ktp?.onClick {
            openImageRes()
        }

        btn_edit_image_ktp?.onClick {
            openImageRes()
        }

        switch_change_password?.setOnCheckedChangeListener { buttonView, isChecked ->
            expanded_password_profile?.isExpanded = isChecked
        }


        btn_delete_image_ktp?.onClick {
            showConfirmationDialog(
                image = this.let { it1 -> R.drawable.ic_alert.getDrawable(it1) },
                title = "Apakah anda yakin ingin meghapus foto ini?",
                listener = object : BaseDialogFragment.Listener {
                    override fun primaryClick(dialog: Dialog) {
                        super.primaryClick(dialog)
                        ktpUrl = null
                        selected_section_image?.gone()
                        unselected_section_image?.visible()
                    }
                }
            )
        }


        btn_save_profile?.onClick {
            updateProfile()
        }

        btn_edit_image_profile?.onClick {
            openImagePicker(ImageRatioData(1, 1)) {
                pb_iv_profile?.visible()
                uploadCloudinary(it.path!!, object : CloudinaryListener {
                    override fun onSuccess(url: String) {
                        super.onSuccess(url)
                        iv_profile?.loadImage(url)
                        imageUrl = url
                        pb_iv_profile?.gone()
                    }
                })
            }
        }
    }

    private fun updateProfile() {
        if (isAllValid()) {
            isLoading(true)
            val request = ProfileRequest().apply {
                name = til_name_profile?.value()
                if (til_email_profile?.value()?.isNotEmpty() == true)
                    email = til_email_profile?.value()
                phone = til_phone_profile?.value()
                imageUrl?.let { photo = it }
                ktpUrl?.let { ktp = it }
                if (switch_change_password?.isChecked == true) {
                    password = til_password_profile?.value()
                }

            }
            doRequest {
                presenter.updateProfile(request)
            }

        }
    }

    private fun isAllValid(): Boolean {
        val validities = mutableListOf<Boolean>()
        validities.add(
            til_name_profile?.validate(
                ValidatorType.Required
            ) == true
        )

        if (til_email_profile?.value()?.isNotEmpty() == true)
            validities.add(
                til_email_profile?.validate(
                    ValidatorType.Email
                ) == true
            )

        if (switch_change_password?.isChecked == true) {
            validities.add(
                til_password_profile?.validate(
                    ValidatorType.Required,
                    ValidatorType.MinimumLength(8)
                ) == true
            )
        }


        val isImageValid = ktpUrl != null
        validities.add(isImageValid)
        tv_error_image_ktp?.setVisibility(!isImageValid)

        validities.add(
            til_phone_profile?.validate(
                ValidatorType.Required,
                ValidatorType.MinimumLength(9),
                ValidatorType.MaximumLength(13)
            ) == true
        )

        return !validities.contains(false)

    }

    override fun updateProfileResponse(response: ProfileResponse) {
        super.updateProfileResponse(response)
        isLoading(false)
        showSuccessSavedDialog(object : BaseDialogFragment.Listener {})
    }
}