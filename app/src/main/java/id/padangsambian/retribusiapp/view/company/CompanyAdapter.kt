package id.padangsambian.retribusiapp.view.company

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.padangsambian.retribusiapp.R
import id.padangsambian.shared.getColor
import id.padangsambian.shared.model.Company
import id.widianapw.android_utils.common.Const
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.setVisibility
import id.widianapw.android_utils.extensions.toDateString
import kotlinx.android.synthetic.main.layout_rv_company.view.*

/**
 * Created by Widiana Putra on 07/12/21
 * Copyright (c) 2021 - Made with love
 */
class CompanyAdapter(val clickListener: (Company) -> Unit) :
    RecyclerView.Adapter<CompanyAdapter.ViewHolder>() {
    private val companyList = mutableListOf<Company>()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.layout_rv_company, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = companyList[position]
        holder.itemView.apply {
            btn_rv_see_detail?.onClick {
                clickListener(item)
            }
            tv_rv_company_name?.text = item.name ?: "-"
            tv_rv_company_tempekan?.text = item.banjar?.name ?: "-"
            tv_rv_company_next_payment?.text = if (item.next_transaction != null)
                item.next_transaction?.date?.toDateString(
                    Const.READ_DATE_FORMAT,
                    isConvertToLocal = false
                ) else "-"
            tv_rv_company_subscription?.text = item.subscription?.subscription_type?.category ?: "-"
            chip_rv_company_status?.text = item.getCompanyStatus().status

            cv_close_company?.setVisibility(item.company_close?.is_close == true)
            layout_cuti_company?.setVisibility(item.company_close?.is_cuti == true)

            item.company_close?.schedule?.let {
                val READ_DATE = "dd MMMM yyyy"
                val dateStart = it.date_start?.toDateString(
                    fromFormat = "yyyy-MM-dd",
                    toFormat = READ_DATE
                )
                val dateEnd = it.date_end?.toDateString(
                    fromFormat = "yyyy-MM-dd",
                    toFormat = READ_DATE
                )
                if (dateStart == dateEnd) {
                    tv_rv_company_close?.text = dateStart
                } else {
                    tv_rv_company_close?.text = "$dateStart - $dateEnd"
                }
            }

            val backgroundColor = when (item.getCompanyStatus()) {
                is Company.Status.Verified -> R.color.color_lightest_primary
                is Company.Status.WaitingReview -> R.color.color_light_yellow
                is Company.Status.Blocked -> R.color.color_light_error
            }

            val textColor = when (item.getCompanyStatus()) {
                is Company.Status.Verified -> R.color.color_primary
                is Company.Status.WaitingReview -> R.color.color_dark_yellow
                is Company.Status.Blocked -> R.color.color_primary_error
            }

            chip_rv_company_status?.setChipBackgroundColorResource(backgroundColor)
            chip_rv_company_status?.setTextColor(textColor.getColor(context))
        }
    }

    override fun getItemCount(): Int {
        return companyList.count()
    }

    fun setData(list: List<Company>) {
        companyList.apply {
            clear()
            addAll(list)
        }
        notifyDataSetChanged()
    }
}