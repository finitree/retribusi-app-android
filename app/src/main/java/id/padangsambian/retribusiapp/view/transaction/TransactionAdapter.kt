package id.padangsambian.retribusiapp.view.transaction

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.padangsambian.retribusiapp.R
import id.padangsambian.retribusiapp.model.response.TransactionItem
import id.padangsambian.retribusiapp.model.response.TransactionType
import id.padangsambian.shared.SubscriptionType
import id.padangsambian.shared.getColor
import id.padangsambian.shared.toCurrency
import id.widianapw.android_utils.common.Const
import id.widianapw.android_utils.extensions.setVisibility
import id.widianapw.android_utils.extensions.toDateString
import kotlinx.android.synthetic.main.layout_rv_transaction.view.*

/**
 * Created by Widiana Putra on 09/12/21
 * Copyright (c) 2021 - Made with love
 */
class TransactionAdapter(val type: String? = null, val subscriptionType: String? = null) :
    RecyclerView.Adapter<TransactionAdapter.ViewHolder>() {
    private val transactionList = mutableListOf<TransactionItem>()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_rv_transaction, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = transactionList[position]
        holder.itemView.apply {
            if (type == TransactionType.PAID) ic_transaction?.setColorFilter(
                R.color.color_primary.getColor(
                    context
                )
            )
            else ic_transaction?.setColorFilter(R.color.color_primary_error.getColor(context))

            tv_transaction_separator?.setVisibility(isShowSeparator(position))
            tv_transaction_separator?.text =
                if (subscriptionType == SubscriptionType.BULANAN) {
                    item.date?.toDateString("yyyy", isConvertToLocal = true)
                } else {
                    item.date?.toDateString("MMMM yyyy", isConvertToLocal = true)
                }

            layout_money?.setVisibility(false)
//            tv_money_pay?.text = "Uang Diberikan: ${item.money_pay?.toCurrency()}"
//            tv_money_changes?.text = "Uang Kembalian: ${item.money_changes?.toCurrency()}"

            tv_transaction_title?.text = item.company_name
            tv_transaction_amount?.text =
                item.amount?.toCurrency()
            tv_transaction_date?.text =
                item.date?.toDateString(toFormat = Const.READ_DATE_FORMAT, isConvertToLocal = false)
        }
    }

    fun isShowSeparator(position: Int): Boolean {
        val item = transactionList[position]
        return if (position == 0) {
            true
        } else {
            if (subscriptionType == SubscriptionType.BULANAN) {
                val prevYear = transactionList[position - 1].date?.toDateString(
                    "yyyy",
                    isConvertToLocal = false
                )
                val currentYear = item.date?.toDateString("yyyy", isConvertToLocal = false)
                currentYear != prevYear
            } else {
                val prevMonthYear = transactionList[position - 1].date?.toDateString(
                    "MMMM yyyy",
                    isConvertToLocal = false
                )
                val currentMonthYear =
                    item.date?.toDateString("MMMM yyyy", isConvertToLocal = false)
                currentMonthYear != prevMonthYear
            }
        }
    }


    override fun getItemCount(): Int {
        return transactionList.count()
    }

    fun setData(list: List<TransactionItem>) {
        transactionList.apply {
            clear()
            addAll(list)
        }
        notifyDataSetChanged()
    }
}