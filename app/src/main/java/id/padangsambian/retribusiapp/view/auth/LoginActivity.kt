package id.padangsambian.retribusiapp.view.auth

import android.os.Bundle
import com.orhanobut.logger.Logger
import id.padangsambian.retribusiapp.MainActivity
import id.padangsambian.retribusiapp.R
import id.padangsambian.retribusiapp.model.LoginRequest
import id.padangsambian.retribusiapp.model.response.LoginResponse
import id.padangsambian.retribusiapp.presenter.InitialHelper
import id.padangsambian.retribusiapp.presenter.auth.AuthPresenter
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapp.view.common.BaseActivity
import id.padangsambian.shared.Utils
import id.widianapw.android_utils.extensions.goToActivity
import id.widianapw.android_utils.extensions.onClick
import id.widianapw.android_utils.extensions.validate
import id.widianapw.android_utils.extensions.value
import id.widianapw.android_utils.validator.ValidatorType
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity(), AuthPresenter.Delegate {
    private val presenter = AuthPresenter(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initListener()
        Logger.d(InitialHelper.getCompanyTypes()?.map { it.id })

    }

    private fun initListener() {
        btn_login?.onClick {
            if (isAllValid()) {
                isLoading(true)
                val loginRequest = LoginRequest().apply {
                    phone = til_phone_login?.value()
                    password = til_password_login?.value()
                }
                doRequest {
                    presenter.login(loginRequest)
                }
            }
        }

        btn_register_login?.onClick {
            goToActivity(RegisterActivity::class.java)
        }

        btn_forgot_password?.onClick {
            goToActivity(ForgotPasswordActivity::class.java)
        }
    }

    private fun isAllValid(): Boolean {
        val validities = mutableListOf<Boolean>()
        validities.add(
            til_phone_login?.validate(
                ValidatorType.Required,
                ValidatorType.MinimumLength(9),
                ValidatorType.MaximumLength(13)
            ) == true
        )
        validities.add(
            til_password_login?.validate(
                ValidatorType.Required,
                ValidatorType.MinimumLength(8)
            ) == true
        )
        return !validities.contains(false)
    }

    override fun loginResponse(loginResponse: LoginResponse) {
        isLoading(false)
        finishAffinity()
        preferences.apply {
            accessToken = loginResponse.data?.access_token.toString()
            preferences.isLoggedIn = true
        }
        goToActivity(MainActivity::class.java)
    }

    override fun showError(title: String?, message: String?) {
        isLoading(false)
        super<BaseActivity>.showError(title, message)
    }

    fun isLoading(izLoading: Boolean) {
        if (izLoading) Utils.showProgress(this)
        else Utils.hideProgress()
    }
}