package id.padangsambian.retribusiapp.view.auth

import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import id.padangsambian.retribusiapp.MainActivity
import id.padangsambian.retribusiapp.R
import id.padangsambian.retribusiapp.model.RegisterRequest
import id.padangsambian.retribusiapp.model.response.LoginResponse
import id.padangsambian.retribusiapp.presenter.auth.AuthPresenter
import id.padangsambian.retribusiapp.utilities.doRequest
import id.padangsambian.retribusiapp.view.common.BaseActivity
import id.padangsambian.shared.Utils
import id.padangsambian.shared.input.BaseDialogFragment
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : BaseActivity(), AuthPresenter.Delegate {
    val registerRequest = RegisterRequest()
    val presenter = AuthPresenter(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        addFragment(Register1Fragment())
        tl_register?.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        when (supportFragmentManager.findFragmentById(R.id.fm_register_main)?.javaClass?.simpleName) {
            Register1Fragment().javaClass.simpleName -> {
                finish()
            }
            Register2Fragment().javaClass.simpleName -> {
                showConfirmationDialog(
                    title = "Data tidak akan tersimpan",
                    description = "Apakah yakin ingin kembali ke halaman sebelumnya?",
                    listener = object : BaseDialogFragment.Listener {
                        override fun primaryClick(dialog: Dialog) {
                            super.primaryClick(dialog)
                            prevPage()
                        }
                    })

            }
        }
    }

    fun nextPage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            pb_register?.setProgress(66, true)
        }
        tl_register?.title = "Buat Usaha"
        replaceFragment(Register2Fragment(), true)
    }

    fun prevPage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            pb_register?.setProgress(33, true)
        }
        tl_register?.title = "Registrasi"
        replaceFragment(Register1Fragment())
    }


    private fun addFragment(fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction
            .add(R.id.fm_register_main, fragment)
            .commit()
    }


    private fun replaceFragment(fragment: Fragment, isNext: Boolean = true) {
        if (isNext) {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction
                .setCustomAnimations(R.anim.slide_from_right, R.anim.fade_out)
                .replace(R.id.fm_register_main, fragment)
                .addToBackStack(null)
                .commit()
        } else {
            supportFragmentManager.popBackStack()
        }
    }

    fun register() {
        isLoading(true)
        doRequest {
            presenter.register(registerRequest)
        }
    }

    override fun registerResponse(loginResponse: LoginResponse) {
        super.registerResponse(loginResponse)
        isLoading(false)

        //user logged in
        finishAffinity()
        preferences.apply {
            isLoggedIn = true
            accessToken = loginResponse.data?.access_token.toString()
        }
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra(MainActivity.IS_FROM_HOME, true)
        startActivity(intent)
    }

    override fun showError(title: String?, message: String?) {
        isLoading(false)
        super<BaseActivity>.showError(title, message)
    }

    fun isLoading(izLoading: Boolean) {
        if (izLoading) Utils.showProgress(this)
        else Utils.hideProgress()
    }
}