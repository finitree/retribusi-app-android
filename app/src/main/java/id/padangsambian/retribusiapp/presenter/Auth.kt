package id.padangsambian.retribusiapp.presenter

import id.padangsambian.shared.model.user.User
import io.realm.Realm
import io.realm.kotlin.where

/**
 * Created by Widiana Putra on 11/12/21
 * Copyright (c) 2021 - Made with love
 */
object Auth {
    fun user(): User? {
        val realm = Realm.getDefaultInstance()
        return realm.where<User>().findFirst()
    }

    fun id(): Int? {
        return user()?.id
    }
}