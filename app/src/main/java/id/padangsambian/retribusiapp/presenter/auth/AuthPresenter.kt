package id.padangsambian.retribusiapp.presenter.auth

import id.padangsambian.retribusiapp.model.LoginRequest
import id.padangsambian.retribusiapp.model.RegisterRequest
import id.padangsambian.retribusiapp.model.ResetPasswordRequest
import id.padangsambian.retribusiapp.model.response.LoginResponse
import id.padangsambian.retribusiapp.presenter.base.BaseDelegate
import id.padangsambian.retribusiapp.utilities.network.client
import id.padangsambian.retribusiapp.utilities.network.requestApi
import id.padangsambian.retribusiapp.utilities.saveOnlySingleDataToRealm
import id.padangsambian.shared.model.SuccessResponse
import io.ktor.client.request.*

/**
 * Created by Widiana Putra on 02/12/21
 * Copyright (c) 2021 - Made with love
 */
class AuthPresenter(val delegate: Delegate) {
    interface Delegate : BaseDelegate {
        fun loginResponse(loginResponse: LoginResponse) {}
        fun registerResponse(loginResponse: LoginResponse) {}
        fun sendResetPasswordResponse(res: SuccessResponse) {}
    }

    suspend fun login(request: LoginRequest) {
        requestApi(delegate) {
            val response = client.post<LoginResponse>(path = "login") {
                body = request
            }
            response.data?.user?.saveOnlySingleDataToRealm()
            delegate.loginResponse(response)
        }
    }

    suspend fun register(request: RegisterRequest) {
        requestApi(delegate) {
            val response = client.post<LoginResponse>(path = "register") {
                body = request
            }
            response.data?.user?.saveOnlySingleDataToRealm()
            delegate.registerResponse(response)
        }
    }

    suspend fun sendResetPassword(req: ResetPasswordRequest) {
        requestApi(delegate) {
            val response =
                client.post<SuccessResponse>(path = "password/send-email-forgot-password") {
                    body = req
                }
            delegate.sendResetPasswordResponse(response)
        }
    }

}