package id.padangsambian.retribusiapp.presenter

import id.padangsambian.retribusiapp.model.response.InitialResponse
import id.padangsambian.retribusiapp.presenter.base.BaseDelegate
import id.padangsambian.retribusiapp.utilities.network.client
import id.padangsambian.retribusiapp.utilities.network.requestApi
import id.padangsambian.retribusiapp.utilities.replaceAllToRealm
import io.ktor.client.request.*

/**
 * Created by Widiana Putra on 06/12/21
 * Copyright (c) 2021 - Made with love
 */
class SplashPresenter(val delegate: Delegate) {
    interface Delegate : BaseDelegate {
        fun initialResponse(response: InitialResponse)
    }

    suspend fun getInitial() {
        requestApi(delegate) {
            val response = client.get<InitialResponse>(path = "initial")
            response.data?.let {
                it.apply {
                    company_types?.replaceAllToRealm()
                    subscription_types?.replaceAllToRealm()
                    tempekans?.replaceAllToRealm()
                }
                delegate.initialResponse(response = response)
            }
        }
    }
}