package id.padangsambian.retribusiapp.presenter

import id.padangsambian.retribusiapp.model.response.TransactionResponse
import id.padangsambian.retribusiapp.presenter.base.BaseDelegate
import id.padangsambian.retribusiapp.utilities.network.client
import id.padangsambian.retribusiapp.utilities.network.requestApi
import id.widianapw.android_utils.extensions.isNotNoll
import io.ktor.client.request.*

/**
 * Created by Widiana Putra on 09/12/21
 * Copyright (c) 2021 - Made with love
 */
class TransactionPresenter(val delegate: Delegate) {
    interface Delegate : BaseDelegate {
        fun getTransactionResponse(response: TransactionResponse) {}
    }

    suspend fun getTransaction(
        type: String,
        companyId: Int? = null,
        date_from: String? = null,
        date_to: String? = null
    ) {
        requestApi(delegate) {
            val response = client.get<TransactionResponse>(path = "transaction") {
                parameter("type", type)
                if (companyId.isNotNoll()) {
                    parameter("company_id", companyId)
                }
                if (date_from?.isNotEmpty() == true) {
                    parameter("date_from", date_from)
                }

                if (date_to?.isNotEmpty() == true) {
                    parameter("date_to", date_to)
                }
            }
            delegate.getTransactionResponse(response)
        }
    }
}