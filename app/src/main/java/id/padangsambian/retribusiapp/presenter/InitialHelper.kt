package id.padangsambian.retribusiapp.presenter

import id.padangsambian.shared.input.SelectItem
import id.padangsambian.shared.model.CompanyScale
import id.padangsambian.shared.model.CompanyType
import id.padangsambian.shared.model.SubscriptionType
import io.realm.Realm
import io.realm.kotlin.where

/**
 * Created by Widiana Putra on 06/12/21
 * Copyright (c) 2021 - Made with love
 */
object InitialHelper {
    fun getCompanyTypes(): List<CompanyType>? {
        val realm = Realm.getDefaultInstance()
        return realm.where<CompanyType>().findAll()
    }

    fun getCompanyTypeSelect(): List<SelectItem>? {
        return getCompanyTypes()?.map { it.toSelectItem() }
    }

    fun getCompanyScales(): List<CompanyScale>? {
        val realm = Realm.getDefaultInstance()
        return realm.where<CompanyScale>().findAll()
    }

    fun getCompanyScaleSelect(): List<SelectItem>? {
        return getCompanyScales()?.map { it.toSelectItem() }
    }

    fun getSubscriptionTypes(): List<SubscriptionType>? {
        val realm = Realm.getDefaultInstance()
        return realm.where<SubscriptionType>().findAll()
    }

    fun getSubscriptionTypesScale(): List<SelectItem>? {
        return getSubscriptionTypes()?.map { it.toSelectItem() }
    }
}