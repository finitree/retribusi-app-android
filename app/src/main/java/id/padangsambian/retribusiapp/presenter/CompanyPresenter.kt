package id.padangsambian.retribusiapp.presenter

import id.padangsambian.retribusiapp.model.CompanyAddRequest
import id.padangsambian.retribusiapp.model.CompanyCloseRequest
import id.padangsambian.retribusiapp.model.CompanyRequest
import id.padangsambian.retribusiapp.model.response.CompanyCloseResponse
import id.padangsambian.retribusiapp.model.response.CompanyDetailResponse
import id.padangsambian.retribusiapp.model.response.CompanyResponse
import id.padangsambian.retribusiapp.presenter.base.BaseDelegate
import id.padangsambian.retribusiapp.utilities.network.client
import id.padangsambian.retribusiapp.utilities.network.requestApi
import id.padangsambian.shared.model.SuccessResponse
import io.ktor.client.request.*

/**
 * Created by Widiana Putra on 07/12/21
 * Copyright (c) 2021 - Made with love
 */
class CompanyPresenter(val delegate: Delegate) {
    interface Delegate : BaseDelegate {
        fun getCompaniesResponse(response: CompanyResponse) {}
        fun getCompanyDetailResponse(response: CompanyDetailResponse) {}
        fun getCompanyUpdateResponse(response: CompanyDetailResponse) {}
        fun getCompanyAddResponse(response: CompanyDetailResponse) {}
        fun getCompanyCloseResponse(response: CompanyCloseResponse) {}
        fun postCompanyCloseResponse(response: SuccessResponse) {}
        fun patchCompanyCloseResponse(respnse: SuccessResponse) {}
        fun deleteCompanyCloseResponse(response: SuccessResponse){}
    }

    suspend fun getCompanies() {
        requestApi(delegate) {
            val response = client.get<CompanyResponse>(path = "company")
            delegate.getCompaniesResponse(response)
        }
    }

    suspend fun getCompany(id: Int) {
        requestApi(delegate) {
            val response = client.get<CompanyDetailResponse>(path = "company/$id")
            delegate.getCompanyDetailResponse(response)
        }
    }

    suspend fun patchCompany(id: Int, request: CompanyRequest) {
        requestApi(delegate) {
            val response = client.patch<CompanyDetailResponse>(path = "company/$id") {
                body = request
            }
            delegate.getCompanyUpdateResponse(response)
        }
    }

    suspend fun storeCompany(request: CompanyAddRequest) {
        requestApi(delegate) {
            val response = client.post<CompanyDetailResponse>(path = "company") {
                body = request
            }
            delegate.getCompanyAddResponse(response)
        }
    }

    suspend fun getCompanyClose(id: Int) {
        requestApi(delegate) {
            val response = client.get<CompanyCloseResponse>(path = "company/$id/close-schedule")
            delegate.getCompanyCloseResponse(response)
        }
    }

    suspend fun postCompanyClose(comId: Int, req: CompanyCloseRequest) {
        requestApi(delegate) {
            val response = client.post<SuccessResponse>(path = "company/${comId}/close-schedule") {
                body = req
            }
            delegate.postCompanyCloseResponse(response)
        }
    }

    suspend fun patchCompanyClose(comId: Int, id: Int, req: CompanyCloseRequest) {
        requestApi(delegate) {
            val response =
                client.patch<SuccessResponse>(path = "company/${comId}/close-schedule/${id}") {
                    body = req
                }
            delegate.patchCompanyCloseResponse(response)
        }
    }

    suspend fun deleteCompanyClose(comId: Int, id: Int) {
        requestApi(delegate) {
            val response =
                client.delete<SuccessResponse>(path = "company/${comId}/close-schedule/${id}")
            delegate.deleteCompanyCloseResponse(response)
        }
    }


}