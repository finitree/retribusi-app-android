package id.padangsambian.retribusiapp.presenter

import id.padangsambian.retribusiapp.model.ProfileRequest
import id.padangsambian.retribusiapp.model.response.ProfileResponse
import id.padangsambian.retribusiapp.presenter.base.BaseDelegate
import id.padangsambian.retribusiapp.utilities.network.client
import id.padangsambian.retribusiapp.utilities.network.requestApi
import id.padangsambian.retribusiapp.utilities.saveOnlySingleDataToRealm
import io.ktor.client.request.*

/**
 * Created by Widiana Putra on 09/12/21
 * Copyright (c) 2021 - Made with love
 */
class ProfilePresenter(val delegate: Delegate) {
    interface Delegate : BaseDelegate {
        fun getProfileResponse(response: ProfileResponse) {}
        fun updateProfileResponse(response: ProfileResponse) {}
    }

    suspend fun getProfile() {
        requestApi(delegate) {
            val response = client.get<ProfileResponse>(path = "profile")
            response.data?.saveOnlySingleDataToRealm()
            delegate.getProfileResponse(response)
        }
    }

    suspend fun updateProfile(request: ProfileRequest) {
        requestApi(delegate) {
            val response = client.patch<ProfileResponse>(path = "profile") {
                body = request
            }
            response.data?.saveOnlySingleDataToRealm()
            delegate.updateProfileResponse(response)
        }
    }
}