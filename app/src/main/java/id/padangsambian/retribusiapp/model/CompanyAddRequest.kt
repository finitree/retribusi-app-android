package id.padangsambian.retribusiapp.model

import id.padangsambian.shared.model.CompanyOperationalItem

/**
 * Created by Widiana Putra on 14/12/21
 * Copyright (c) 2021 - Made with love
 */
class CompanyAddRequest {
    var name: String? = null
    var address: String? = null
    var documents: String? = null
    var photos: String? = null
    var latitude: Double? = null
    var longitude: Double? = null

    var company_type_id: Int? = null
    var subscription_type_id: Int? = null

    var company_operational_schedule: List<CompanyOperationalItem>? = null
}

fun getDefaultCompanyOperational(): List<CompanyOperationalItem>? {
    val list = mutableListOf<CompanyOperationalItem>()
    list.add(
        CompanyOperationalItem().apply {
            day = 1
            opening_hours = "00:00"
            closing_hours = "23:59"
            is_open = 1
        }
    )
    list.add(
        CompanyOperationalItem().apply {
            day = 2
            opening_hours = "00:00"
            closing_hours = "23:59"
            is_open = 1
        }
    )
    list.add(
        CompanyOperationalItem().apply {
            day = 3
            opening_hours = "00:00"
            closing_hours = "23:59"
            is_open = 1
        }
    )
    list.add(
        CompanyOperationalItem().apply {
            day = 4
            opening_hours = "00:00"
            closing_hours = "23:59"
            is_open = 1
        }
    )
    list.add(
        CompanyOperationalItem().apply {
            day = 5
            opening_hours = "00:00"
            closing_hours = "23:59"
            is_open = 1
        }
    )
    list.add(
        CompanyOperationalItem().apply {
            day = 6
            opening_hours = "00:00"
            closing_hours = "23:59"
            is_open = 1
        }
    )
    list.add(
        CompanyOperationalItem().apply {
            day = 7
            opening_hours = "00:00"
            closing_hours = "23:59"
            is_open = 1
        }
    )
    return list
}