package id.padangsambian.retribusiapp.model

/**
 * Created by Widiana Putra on 02/12/21
 * Copyright (c) 2021 - Made with love
 */
data class LoginRequest(
    var phone: String?=null,
    var password: String?=null
)
data class ResetPasswordRequest(
    var email: String? = null
)
