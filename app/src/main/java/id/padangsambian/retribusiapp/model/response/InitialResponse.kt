package id.padangsambian.retribusiapp.model.response

import id.padangsambian.shared.model.CompanyScale
import id.padangsambian.shared.model.CompanyType
import id.padangsambian.shared.model.SubscriptionType
import id.padangsambian.shared.model.Tempekan

/**
 * Created by Widiana Putra on 06/12/21
 * Copyright (c) 2021 - Made with love
 */
class InitialResponse {
    var data: InitialData? = null
}

class InitialData {
    var company_types: List<CompanyType>? = null
    var tempekans: List<Tempekan>? = null
    var subscription_types: List<SubscriptionType>? = null
}

