package id.padangsambian.retribusiapp.model

import id.padangsambian.shared.model.CompanyOperationalItem

/**
 * Created by Widiana Putra on 09/12/21
 * Copyright (c) 2021 - Made with love
 */
class CompanyRequest {
    var company_type_id: Int? = null
    var name: String? = null
    var address: String? = null
    var documents: String? = null
    var photos: String? = null

    var company_operational_schedule: List<CompanyOperationalItem>? = null
}