package id.padangsambian.retribusiapp.model

/**
 * Created by Widiana Putra on 09/12/21
 * Copyright (c) 2021 - Made with love
 */
class ProfileRequest {
    var name: String? = null
    var email: String? = null
    var phone: String? = null
    var photo: String? = null
    var ktp: String?=null
    var password: String?=null
}