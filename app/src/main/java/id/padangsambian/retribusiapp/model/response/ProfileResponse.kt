package id.padangsambian.retribusiapp.model.response

import id.padangsambian.shared.model.user.User

/**
 * Created by Widiana Putra on 09/12/21
 * Copyright (c) 2021 - Made with love
 */
class ProfileResponse {
    var data: User? = null
}