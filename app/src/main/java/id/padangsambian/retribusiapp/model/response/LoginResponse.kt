package id.padangsambian.retribusiapp.model.response

import id.padangsambian.shared.model.user.User

/**
 * Created by Widiana Putra on 02/12/21
 * Copyright (c) 2021 - Made with love
 */
class LoginResponse {
    var data: LoginData? = null
}

class LoginData {
    var user: User? = null
    var access_token: String? = null
}