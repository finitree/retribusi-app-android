package id.padangsambian.retribusiapp.model

import id.padangsambian.shared.model.CompanyOperationalItem

/**
 * Created by Widiana Putra on 02/12/21
 * Copyright (c) 2021 - Made with love
 */
data class RegisterRequest(
    var name: String? = null,
    var email: String? = null,
    var password: String? = null,
    var password_confirmation: String? = null,
    var phone: String? = null,
    var photo: String? = null,
    var ktp: String? = null,

    var company_type_id: Int? = null,
    var subscription_type_id: Int? = null,

    var company_name: String? = null,
    var latitude: Double? = null,
    var longitude: Double? = null,
    var address: String? = null,
    var documents: String? = null,
    var photos: String? = null,

    var company_operational_schedule: List<CompanyOperationalItem>?=null
)
