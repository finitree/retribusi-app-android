package id.padangsambian.retribusiapp.model.response

/**
 * Created by Widiana Putra on 09/12/21
 * Copyright (c) 2021 - Made with love
 */
class TransactionResponse {
    var data: List<TransactionItem>? = null
}

object TransactionType {
    const val PAID = "paid"
    const val UNPAID = "unpaid"
}

class TransactionItem {
    var company_name: String? = null
    var amount: Int? = null
    var date: String? = null
    var money_pay: Int? = null
    var money_changes: Int? = null
}