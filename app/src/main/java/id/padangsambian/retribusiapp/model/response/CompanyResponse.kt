package id.padangsambian.retribusiapp.model.response

import id.padangsambian.shared.model.Company
import id.padangsambian.shared.model.CompanyCloseItem

/**
 * Created by Widiana Putra on 07/12/21
 * Copyright (c) 2021 - Made with love
 */
class CompanyResponse {
    var data: List<Company>? = null
}

class CompanyDetailResponse {
    var data: Company? = null
}

class CompanyCloseResponse {
    var data: List<CompanyCloseItem>? = null
}