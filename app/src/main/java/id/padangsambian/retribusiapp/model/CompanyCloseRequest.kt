package id.padangsambian.retribusiapp.model

class CompanyCloseRequest {
    var date_start: String? = null
    var date_end: String? = null
}