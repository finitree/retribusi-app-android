package id.padangsambian.retribusiapp.utilities.constant

import android.Manifest

/**
 * Created by Widiana Putra on 10/11/21
 * Copyright (c)
 */
object Constants {
    const val baseUrl = "retribusi.finitree.com/api/owner"
}

object Permissions {
    val image = arrayOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    )

    val locations = arrayOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
}