package id.padangsambian.retribusiapp.utilities;

import com.cloudinary.Transformation;
import com.cloudinary.android.MediaManager;

/**
 * Created by Widiana Putra on 13/12/21
 * Copyright (c) 2021 - Made with love
 */
public class CloudinaryPDF {
    public String getPdfUrl(String path) {
        return MediaManager.get().url().transformation(new Transformation().flags("attachment:pdf")).generate(path);
    }
}
