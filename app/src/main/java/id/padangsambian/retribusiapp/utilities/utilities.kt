package id.padangsambian.retribusiapp.utilities

import android.app.Dialog
import android.content.ContentResolver
import android.net.Uri
import android.provider.OpenableColumns
import androidx.fragment.app.FragmentManager
import id.padangsambian.shared.input.Select
import id.padangsambian.shared.input.SelectDialogFragment
import id.padangsambian.shared.input.SelectItem
import id.widianapw.android_utils.extensions.onClick
import kotlinx.android.synthetic.main.fragment_register2.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.net.URLEncoder

/**
 * Created by Widiana Putra on 10/11/21
 * Copyright (c) PT. TIMEDOOR INDONESIA
 */

 fun doRequest(function: suspend () -> Unit): Job {
    return GlobalScope.launch(Dispatchers.Main) {
        function()
    }
}

fun ContentResolver.getFileName(fileUri: Uri): String {

    var name = ""

    val returnCursor = this.query(fileUri, null, null, null, null)

    if (returnCursor != null) {

        val nameIndex = returnCursor.getColumnIndex(

            OpenableColumns.DISPLAY_NAME
        )

        returnCursor.moveToFirst()

        name = returnCursor.getString(nameIndex)

        returnCursor.close()

    }

    return URLEncoder.encode(name, "utf-8")

}

fun Select?.setSelections(fm: FragmentManager,list: List<SelectItem>?){
    this?.onClick {
        val dialog = list?.let { it1 ->
            SelectDialogFragment(
                it1,
                this.selectItem,
                listener = object : SelectDialogFragment.Listener {
                    override fun save(dialog: Dialog, item: SelectItem?) {
                        super.save(dialog, item)
                        item?.let { it1 -> this@setSelections.setItem(it1) }
                    }
                })
        }
        dialog?.show(fm, "")
    }
}